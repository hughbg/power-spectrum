import os, sys
import numpy as np
import matplotlib.pyplot as plt

def log_transform(im):
    '''returns log(image) scaled to the interval [0,1]'''
    try:
        (min, max) = (im[im > 0].min(), im.max())
        if (max > min) and (max > 0):
            return (np.log10(im.clip(min, max)) - np.log10(min)) / (np.log10(max) - np.log10(min))*np.log10(max)
    except:
        pass
    return im

def bbox(grid):
  rows = np.any(grid, axis=1)
  cols = np.any(grid, axis=0)
    
  rmin, rmax = np.where(rows)[0][[0, -1]]
  cmin, cmax = np.where(cols)[0][[0, -1]]

  return rmin, rmax, cmin, cmax

fname = sys.argv[1]
for line in open(fname):
  l = line.split()


if l[3] == "False": positive_negative_separate = False
else: positive_negative_separate = True
horizon =l[4]
horizon_low = float(horizon.split(":")[0])
horizon_high = float(horizon.split(":")[1])
if l[5] == "fast": fast = True
else: fast = False

#if fast:
#  os.system("./ps_fast "+sys.argv[1])
#else: os.system("./power_spectrum "+sys.argv[1])

f = open(fname)
header = f.readline().split()
data = np.loadtxt(f)	
f.close()

if int(header[0]) != data.shape[0] or int(header[1]) != data.shape[1]:
  print "Error with sizes, not the same as indicated"
  exit(1)

if fast:
  a, uv_max, b, c = bbox(data)
  data = data[:uv_max, :500]
  header[3] = uv_max/10
  header[5] = 500

print "Plotting"

data = data.transpose()
data = data[::-1]

frac = 0.1
lim1 = int(data.shape[0]*frac)
lim2 = int(data.shape[0]*(1-frac))
top = np.percentile(data, 99)
print top
rms1 = np.sqrt(np.mean(np.square(data[:lim1, :])))
rms2 = np.sqrt(np.mean(np.square(data[lim2:, :])))
rms =(rms1+rms2)/2
print rms
print top/rms
 
data[lim1:lim2,  :] = 0

if fast:
  plt.xlabel("UV length", fontsize=15)
  plt.ylabel("Delay spectrum FFT index", fontsize=15)
else:
  plt.xlabel("$k_{\perp} [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15)
  plt.ylabel("$k_{\parallel} [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15)
plt.imshow(log_transform(data), extent=[ float(header[2]), float(header[3]), float(header[4]), float(header[5]) ], aspect="auto")

#plt.show()

