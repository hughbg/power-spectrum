/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/

/*
Compare two files, one with internal "CAL" format from RTS. Not used and out of date.
*/


#include "flags.h"
#include "visibilities.h"

#define WHICH_DUMP 0

bool check(int v1, int v2, const char *what) {
  if ( v1 != v2 ) {
    cout << what << " aren't all the  same. " << v1 << " " << v2 << endl;
    return true;
  }
  return false;
}

bool check(float v1, float v2, const char *what) {
	const float tolerance = 0.5e-6;
	float diff;
	if ( v1 != 0 ) diff = abs(v1-v2)/v1;
	else if ( v2 != 0 ) diff = abs(v1-v2)/v2;
	else diff == 0;
	if ( diff > tolerance ) {
		cout << what << " aren't the same. " << v1 << " " << v2 << endl;
		return true;
	}
	return false;
}

int main(int argc, char *argv[]) {
  Visibilities **uvfits;
  Flags flags(WHICH_DUMP);
  int num;
  bool problem;
  
  cout << "Loading UV " << argv[1] << endl;
  load_uvdata(argv[1], uvfits, num);
  cout << "Loading CAL " << argv[2] << endl;
  Visibilities cal(argv[2]);
  
  check(uvfits[WHICH_DUMP]->num_channels, cal.num_channels, "num_channels");
  check(uvfits[WHICH_DUMP]->num_baselines, cal.num_baselines, "num_channels");
  check(uvfits[WHICH_DUMP]->frequencies[0], cal.frequencies[0], "base_freq");

  cout << "Checking\n";

  for (int bl=0; bl<uvfits[WHICH_DUMP]->num_baselines; ++bl) {
		  for (int ch=0; ch<uvfits[WHICH_DUMP]->num_channels; ++ch) {

			if ( check(uvfits[WHICH_DUMP]->visibilities[ch][bl].in_use, cal.visibilities[ch][bl].in_use, "in_use") ) {
		          uvfits[WHICH_DUMP]->visibilities[ch][bl].print();
			  cal.visibilities[ch][bl].print();
			}

                        if ( uvfits[WHICH_DUMP]->visibilities[ch][bl].in_use && cal.visibilities[ch][bl].in_use ) {
			  
				  if ( ! (uvfits[WHICH_DUMP]->visibilities[ch][bl].st1 == uvfits[WHICH_DUMP]->visibilities[0][bl].st1 && 
						  uvfits[WHICH_DUMP]->visibilities[ch][bl].st2 == uvfits[WHICH_DUMP]->visibilities[0][bl].st2) ) 
					  cout << "Stands not all the same in UVfits baseline " << bl << endl;
				  if ( ! (cal.visibilities[ch][bl].st1 == cal.visibilities[0][bl].st1 && 
						  cal.visibilities[ch][bl].st2 == cal.visibilities[0][bl].st2) ) 
					  cout << "Stands not all the same in cal baseline " << bl << endl;

				  problem = false;
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].st1, cal.visibilities[ch][bl].st1, "st1");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].st2, cal.visibilities[ch][bl].st2, "st2");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].frequency, cal.visibilities[ch][bl].frequency, "frequency");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].uv_length, cal.visibilities[ch][bl].uv_length, "uv_length");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].k_perp, cal.visibilities[ch][bl].k_perp, "k_perp");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].length, cal.visibilities[ch][bl].length, "length");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].bl_code, cal.visibilities[ch][bl].bl_code, "bl_code");
				  problem |= check(abs(uvfits[WHICH_DUMP]->visibilities[ch][bl].value), abs(cal.visibilities[ch][bl].value), "value");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].weight, cal.visibilities[ch][bl].weight, "weight");
				  problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].weight>0, true, "weight > 0");
				  problem |= check(cal.visibilities[ch][bl].weight>0, true, "weight > 0");

				  if ( problem ) {
					  cout << uvfits[WHICH_DUMP]->visibilities[ch][bl].st1 << " " << uvfits[WHICH_DUMP]->visibilities[ch][bl].st2 << " " << cal.visibilities[ch][bl].st1 << " " << cal.visibilities[ch][bl].st2 << " " << ch << endl;
					  uvfits[WHICH_DUMP]->visibilities[ch][bl].print();
					  cal.visibilities[ch][bl].print();
				  }
				  
			  } else {
				problem = false;
				problem |= check(uvfits[WHICH_DUMP]->visibilities[ch][bl].weight==0, true, "weight == 0");
    			        problem |= check(cal.visibilities[ch][bl].weight<0, true, "weight < 0");
				if ( problem ) {
				  uvfits[WHICH_DUMP]->visibilities[ch][bl].print();
				  cal.visibilities[ch][bl].print();
			    } 
				 
			  }

		  }
	  
  }
  cout << "Done checking\n";
  

}
