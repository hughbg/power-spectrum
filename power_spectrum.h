/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include "grid.h"
#include "visibilities.h"

/*
  A slight misnomer, it may not contain power, but complex values ready to be converted to power.
  Contains a 2-D grid implemented as a Grid class, which contains the transformed visibility data. 
  It loads a file of calibrated visibilities and converts to k_par and k_perp.

  To do: This could inherit from Grid.
*/


class PowerSpectrum {
public:
  string fname;
  int dump;
  int *frequencies;
  int num_channels, fft_size, which_pol;
  int chan_width;
  float max_length, lst_seconds;
  Grid kk_space, reduced_kk_space;
  bool positive_negative_separate;
  char window[64];
  
  PowerSpectrum(char *fname, int dump_index, Visibilities& vis, const int fft_size_num, 
	const char* window, const bool positive_negative_separate, const int which_pol);
  PowerSpectrum(const PowerSpectrum&);  
  ~PowerSpectrum() { if ( frequencies != NULL ) delete[] frequencies; frequencies = NULL; }
  PowerSpectrum& operator*=(const PowerSpectrum& ps);
  PowerSpectrum& operator+=(const PowerSpectrum& ps);
  PowerSpectrum& operator/=(const double f);
  PowerSpectrum& operator=(const PowerSpectrum& ps);
  void reduce_by_k_perp(PowerSpectrum *ps, bool setup=true);
  void clip(int max_delay);
  void calc_power();	// from Eqn12 in Parsons
  void save(const char *fname);
};
