import os, sys
import optparse
import numpy as np
from matplotlib import use
use("Agg")
#import matplotlib; matplotlib.rcParams['figure.figsize'] = (8, 6)		# or set whatever is good for the paper
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm

def check_stripey_old(d):
  limit = 0.2
  num_high = 0
  for i in range(d.shape[1]):
    peak = np.max(d[:, i])
    top = d[0, i]
    bottom = d[-1, i]

    if peak > 0 and not ( top < limit*peak and bottom < limit*peak ):
      num_high += 1
      #d[:, i] = 0		# Alters the input
      #print i

  return num_high


def check_stripey(d):

  def logn_hist(a):
    top = np.log10(np.max(a)*1.01)
    bottom = np.log10(np.min(a)*.99)
    bin_width = (top-bottom)/9
    bin_edges = np.arange(bottom, top+bin_width, bin_width)
    return np.histogram(np.log10(a), bins=bin_edges)

  def linear_hist(a):
    top = np.max(a)*1.01
    bottom = np.min(a)*.99
    bin_width = (top-bottom)/9
    bin_edges = np.arange(bottom, top+bin_width, bin_width)
    return np.histogram(a, bins=bin_edges)

  top_rows = np.mean(d[:10], axis=0)
  bottom_rows = np.mean(d[:-10], axis=0)

  hist, bin_edges = linear_hist(top_rows)
  where_max = np.argmax(hist)
  most_common_value = (bin_edges[where_max+1]+bin_edges[where_max])/2

  return int(np.max(top_rows)/most_common_value > 2)


def bresenham(x0, y0, x1, y1):
    "Bresenham's line algorithm https://rosettacode.org/wiki/Bitmap/Bresenham%27s_line_algorithm#Python"
    points = []

    dx = abs(x1 - x0)
    dy = abs(y1 - y0)
    x, y = x0, y0
    sx = -1 if x0 > x1 else 1
    sy = -1 if y0 > y1 else 1
    if dx > dy:
        err = dx / 2.0
        while x != x1:
            points.append((x, y))
            err -= dy
            if err < 0:
                y += sy
                err += dx
            x += sx
    else:
        err = dy / 2.0
        while y != y1:
            points.append((x, y))
            err -= dx
            if err < 0:
                x += sx
                err += dy
            y += sy        
    points.append((x, y))

    return points
	
def outside_wedge_mask_for_best(shape):		# Skip the ringing
  mask = np.full(shape, 1, dtype=np.int)	# Hardwired to current PS
  left = 17; right = 87; top = 35; bottom = 33  # Top and bottom of the line near the wedge, left and right
  limit_left = 9; limit_right = 14
  bres = bresenham(left, top, right, bottom)

  line = np.zeros(shape[1], dtype=np.int)
  for b in bres:		# Make it an easier format
    line[b[0]] = b[1]

  bres = bresenham(left, limit_left, right, limit_right)
  limit_line = np.zeros(shape[1], dtype=np.int)
  for b in bres:		# Make it an easier format
    limit_line[b[0]] = b[1]

  for i in range(mask.shape[0]):
    for j in range(mask.shape[1]):
      if left <= j and j <= right:
        # Find what the other co-ord should be less than to mask
        if i <= line[j] and i > limit_line[j]: 
          mask[i, j] = 0
          mask[-i, j] = 0
          mask[-i-2, j] = 0

  return mask	# If mask is 0 then this is the outside wedge region easy to mask data

def outside_wedge_mask_for_delta(shape):         # Skip the ringing
  mask = np.full(shape, 1, dtype=np.int)        # Hardwired to current PS
  left = 12; right = 87; top = 47; bottom = 43  # Top and bottom of the line near the wedge, left and right
  limit_left = 9; limit_right = 14
  bres = bresenham(left, top, right, bottom)

  line = np.zeros(shape[1], dtype=np.int)
  for b in bres:                # Make it an easier format
    line[b[0]] = b[1]

  bres = bresenham(left, limit_left, right, limit_right)
  limit_line = np.zeros(shape[1], dtype=np.int)
  for b in bres:                # Make it an easier format
    limit_line[b[0]] = b[1]

  for i in range(mask.shape[0]):
    for j in range(mask.shape[1]):
      if left <= j and j <= right:
        # Find what the other co-ord should be less than to mask
        if i <= line[j] and i > limit_line[j]:
          mask[i, j] = 0
          mask[-i, j] = 0
          mask[-i-2, j] = 0

  return mask   # If mask is 0 then this is the outside wedge region easy to mask data




      
def outside_wedge_mask(shape):
  mask = np.full(shape, 1, dtype=np.int)	# Hardwired to current PS
  left = 17; right = 87; top = 48; bottom = 44
  bres = bresenham(left, top, right, bottom)

  line = np.zeros(shape[1], dtype=np.int)
  for b in bres:		# Make it an easier format
    line[b[0]] = b[1]

  for i in range(mask.shape[0]):
    for j in range(mask.shape[1]):
      if left <= j and j <= right:
        # Find what the other co-ord should be less than to mask
        if i <= line[j]: 
          mask[i, j] = 0
          mask[-i, j] = 0
          mask[-i-2, j] = 0

  return mask	# If mask is 0 then this is the outside wedge region easy to mask data
      
def inside_wedge_mask_bad(shape):
  mask = np.full(shape, 1, dtype=np.int)	# Hardwired to current PS
  left = 18; right = 96; top = 52; bottom = 47
  bres = bresenham(left, top, right, bottom)

  line = np.zeros(shape[1], dtype=np.int)
  for b in bres:		# Make it an easier format
    line[b[0]] = b[1]

  for i in range(mask.shape[0]):
    for j in range(mask.shape[1]):
      #if top <= j and j <= right:
        # Find what the other co-ord should be less than to mask
        #if line[j] <= i and i <= 32-line[j]: 
          #mask[i, j] = 0
      if left <= j and j <= right and line[j] <= i and i <= 32-line[j]: mask[i, j] = 0
  return mask

def inside_wedge_mask(shape):
  mask = np.full(shape, 1, dtype=np.int)        # Hardwired to current PS
  left = 18; right = 96; top = 52; bottom = 47
  bres = bresenham(left, top, right, bottom)

  line = np.zeros(shape[1], dtype=np.int)
  for b in bres:                # Make it an easier format
    line[b[0]] = b[1]

  for i in range(mask.shape[0]):
    for j in range(mask.shape[1]):
      #if top <= j and j <= right:
        # Find what the other co-ord should be less than to mask
        #if line[j] <= i and i <= 32-line[j]: 
          #mask[i, j] = 0
      if left <= j and j <= right and line[j] <= i and i <= shape[0]-2-line[j]: mask[i, j] = 0
  return mask



def stats_in_region(data, mask):
  flat_mask = np.ravel(mask)
  flat_data = np.ravel(data)
  data_vals = flat_data[flat_mask==0]
  return np.mean(data_vals), np.std(data_vals)

def min_above(d):
  max_val = np.max(d)
  filled = np.where(d==0, max_val, d)
  return np.min(filled)

def cut_plot(d, limits, hl, hh):
  all_y_max = 2.2e+18
  all_y_min = 9.1e+13
  x_pixel = d.shape[1]/2+3		# Half way along x-axis is where we cut
  x_k_perp = (float(header[3])-float(header[2]))/(d.shape[1]-1)*x_pixel+float(header[2])
  print "Cut plot at k_perp", x_k_perp
  delta_k_par = (float(header[5])-float(header[4]))/(data.shape[0]-1)
  x = [ float(header[4])+i*delta_k_par for i in range(data.shape[0]) ]

  # Need to work out where horizon is at cut line
  horizon_slope = (hh-hl)/(data.shape[1]-1)
  horizon_value = hl+x_pixel*horizon_slope	# This is in k_\par units
  if data.shape[0]%2 != 1:
    print "WARNING: k_par axis is not symetrical around 0"
  k_par_0_pixel = data.shape[0]/2
  k_par_horizon_pixel = horizon_value*(data.shape[0]-1-k_par_0_pixel)/float(header[5])
  k_par_horizon_pixel_top = np.int(np.ceil(k_par_horizon_pixel))+k_par_0_pixel
  k_par_horizon_pixel_bottom = -np.int(np.ceil(k_par_horizon_pixel))+k_par_0_pixel

  plt.ylabel("$P(k)\ [ \mathrm{ mK}^2 (h\mathrm{ Mpc}^{-1})^3 ]$", fontsize=15)
  plt.xlabel("$k_{\parallel}\ [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15)
  #plt.ylim(int(limits[0]), int(limits[1]))
  plt.yscale("log")
  plt.xlim(x[0], x[-1])   # Stops matplotlib extending the axis
  plt.ylim(ymin=all_y_min, ymax=all_y_max)
  print "Cut stats", np.min(d[:, x_pixel-2:x_pixel+3]), np.max(d[:, x_pixel-2:x_pixel+3])
  plt.plot(x, np.mean(d[:, x_pixel-2:x_pixel+3], axis=1), "r")
  plt.plot([ horizon_value, horizon_value ], [0, d[k_par_horizon_pixel_top, x_pixel]*1.8 ], "b")
  plt.plot([ -horizon_value, -horizon_value ], [0, d[k_par_horizon_pixel_bottom, x_pixel]*1.5 ], "b")
  plt.savefig("cut_vertical.png")
  plt.clf()

  y_pixel = int(3.0*d.shape[0]/4)
  print y_pixel, d.shape[0]
  y_k_parallel = (float(header[5])-float(header[4]))/(d.shape[0]-1)*y_pixel+float(header[4])
  print "Cut plot at k_parallel", y_k_parallel, np.std(data[y_pixel]-np.mean(data[y_pixel]))/np.mean(data[y_pixel]), np.min(data[y_pixel]), np.max(data[y_pixel])
  delta_k_perp = (float(header[3])-float(header[2]))/(data.shape[1]-1)
  y = [ float(header[2])+i*delta_k_perp for i in range(data.shape[1]) ]

  plt.ylabel("$P(k)\ [ \mathrm{ mK}^2 (h\mathrm{ Mpc}^{-1})^3 ]$", fontsize=15)
  plt.xlabel("$k_{\perp}\ [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15)
  #plt.ylim(int(limits[0]), int(limits[1]))
  plt.yscale("log")
  plt.xlim(y[0], y[-1])
  plt.ylim(ymin=all_y_min, ymax=all_y_max)
  print "Cut stats", np.min(d[y_pixel-2:y_pixel+3]), np.max(d[y_pixel-2:y_pixel+3])
  plt.plot(y, np.mean(d[y_pixel-2:y_pixel+3], axis=0), "r")
  plt.savefig("cut_horizontal.png")
  plt.clf()

def delta_2_k_plot(d, h):
  max_k = -1e39; min_k = 1e39
  values = []
  k_par_0 = d.shape[0]/2-1
  k_par_sep = (float(h[5])-float(h[4]))/(d.shape[0]-1)
  k_perp_sep = (float(h[3])-float(h[2]))/(d.shape[1]-1)
  mask = outside_wedge_mask_for_best(d.shape)
  for i in range(d.shape[0]):
    for j in range(d.shape[1]):
      if mask[i, j] == 0:
        k_par = float(h[4])+(k_par_0-i)*k_par_sep
        k_perp = float(h[2])+j*k_perp_sep
        k = np.sqrt(k_par**2+k_perp**2)
        k = np.log10(k)
        values.append((k, d[i, j]))
        if k < min_k: min_k = k
        if k > max_k: max_k = k

  num_bins = 10
  binned = np.zeros(num_bins)
  weights = np.zeros(num_bins, dtype=np.int)
  for v in values:
    which_bin = int( (v[0]-min_k)/(max_k-min_k)*(len(binned)-2) )       # Should be len(bins)-1 but don't want just 1 value in top bin
    binned[which_bin] += v[1]
    weights[which_bin] += 1

  x = []
  y = []
  for i in range(len(binned)):
    if weights[i] != 0: 
      binned[i] /= weights[i]

      k = i*(max_k-min_k)/(len(binned)-2)+min_k         # Reverse of above

      x.append(k); y.append((10**k)**3*binned[i]/2/(np.pi**2))

  #plt.xlim(0.6, 1.1)
  #plt.ylim(2.4e13, 1.e14)
  x = np.power(10, x)
  plt.plot(x, y, "r")
  for i in range(len(x)): print x[i], y[i]
  plt.xlabel("$k\ [ h\mathrm{ Mpc}^{-1} ]$", fontsize=18)
  plt.ylabel("$\Delta^2_{21} [ \mathrm{ mK}^{2} ]$", fontsize=18)
  locx, labx = plt.xticks()
  locy, laby = plt.yticks()
  plt.xscale("log")
  plt.yscale("log")
  for i in range(len(locx)): labx[i] = locx[i]
  plt.xticks(locx, labx, fontsize=14)
  plt.yticks(fontsize=14)
  for i in range(len(locy)): laby[i] = locy[i]
  #plt.yticks(locy, laby, fontsize=12)
  plt.ylim(ymin=4e11, ymax=5.5e14)
  plt.xlim(xmin=np.min(x)*.9, xmax=np.max(x)*1.1)
 
  #plt.xticks([np.min(x), np.max(x)], [ ( "%.2f" % np.min(x) ), ( "%.2f" % np.max(x) )])
  #plt.yticks([np.min(y), np.max(y)], [ ( "%.1e" % np.min(y) ), ( "%.1e" % np.max(y) )])
  plt.tight_layout()
  plt.savefig("delta_2_k.png")
  plt.clf()


# Load complex data and convert to power by taking abs()
def load_data(f_handle, rows, cols):

  fdata = np.zeros((rows, cols), dtype=np.complex64)
  for i in range(rows):
    line = f_handle.readline()
    l = line.split()
    if len(l) != cols:
      print "Invalid number of columns", len(l), "in row", i
      exit(0)
    if line[0] == "(":
      for j in range(cols): 
        fdata[i, j] = complex(l[j])
    else:
      for j in range(cols): 
        fdata[i, j] = float(l[j])


  #fdata = np.transpose(fdata)
  #fdata = fdata[::-1]
  #dd = np.zeros(108, dtype=np.complex64)
  #dd[5:15] = fdata[:, 46][5:15]
  #dd[94:104] = fdata[:, 46][94:104]
  #dd = np.fft.fftshift(dd)
  #ff = np.fft.ifft(dd)
  #plt.plot(np.real(ff))
  #plt.show()
  #exit()

  return np.abs(fdata)

# MAIN

o = optparse.OptionParser()
o.add_option('--cuts', dest='cuts', action='store_true', default=False,
                help='Vertical and horizontal cuts')
o.add_option('--delta', dest='delta', action='store_true', default=False,
                 help='delta^2 k')
o.add_option('--mask', dest='mask', action='store_true', default=False,
                 help='show mask')
o.add_option('--delta_mask', dest='delta_mask', action='store_true', default=False,
                 help='show delta^2 mask')


opts, args = o.parse_args(sys.argv[1:])
    
if len(args) != 1:
    print "Bad args"; exit(1)
    exit(1)
else:
    fname = args[0]
        


for line in open(fname):
  l = line.split()

if l[3] == "False": positive_negative_separate = False
else: positive_negative_separate = True
horizon =l[4]
horizon_low = float(horizon.split(":")[0])
horizon_high = float(horizon.split(":")[1])
if l[5] == "fast": fast = True
else: fast = False

#if fast:
#  os.system("./ps_fast "+sys.argv[1])
#else: os.system("./power_spectrum "+sys.argv[1])

f = open(fname)
header = f.readline().split()
data = load_data(f, int(header[0]), int(header[1]))	
f.close()

if int(header[0]) != data.shape[0] or int(header[1]) != data.shape[1]:
  print "Error with sizes, not the same as indicated"
  exit(1)

if fast:
  a, uv_max, b, c = bbox(data)
  data = data[:uv_max, :500]
  header[3] = uv_max/10
  header[5] = 500

print "Plotting"
print "Min", np.min(data), "Max", np.max(data), "Min above 0", min_above(data)


#clip_val = 1e39
#data = np.where(data>clip_val, clip_val, data)
chop_left = 0
if chop_left > 0:
  increment = (float(header[3])-float(header[2]))/(data.shape[0]-1)
  new_k_p_left = float(header[2])+chop_left*increment

  increment = (horizon_high-horizon_low)/(data.shape[0]+1)  # -1
  new_h_low = horizon_low+chop_left*increment

  data = data[chop_left:]
  header[2] = new_k_p_left
  horizon_low = new_h_low

data = data.transpose()
data = data[::-1]



vmin = 3.9e13
vmax = 1.32e21

plt.text(0.018, 0.373, "$P(k)\ [ \mathrm{ mK}^2 (h\mathrm{ Mpc}^{-1})^3 ]$", fontsize=15)
if fast:
  plt.xlabel("UV length", fontsize=15)
  plt.ylabel("Delay spectrum FFT index", fontsize=15)
else:
  plt.xlabel("$k_{\perp}\ [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15)
  plt.ylabel("$k_{\parallel}\ [ h\mathrm{ Mpc}^{-1} ]$", fontsize=15, labelpad=-2)
if opts.mask:
  plt.imshow(data*outside_wedge_mask_for_best(data.shape)*inside_wedge_mask(data.shape), norm=LogNorm(), interpolation="none", extent=[ float(header[2]), float(header[3]), float(header[4]), float(header[5]) ], aspect="auto", cmap="rainbow", vmin=vmin, vmax=vmax)
elif opts.delta_mask:
  plt.imshow(data*outside_wedge_mask_for_delta(data.shape), norm=LogNorm(), interpolation="none", extent=[ float(header[2]), float(header[3]), float(header[4]), float(header[5]) ], aspect="auto", cmap="rainbow", vmin=vmin, vmax=vmax)
else:
  plt.imshow(data, norm=LogNorm(), interpolation="none", extent=[ float(header[2]), float(header[3]), float(header[4]), float(header[5]) ], aspect="auto", cmap="rainbow", vmin=vmin, vmax=vmax)


plt.colorbar(pad=0.12)
plt.autoscale(False)

# Thing about plot - when given a set of points, the last point is the last axis location.
# Thing about imshow - the same applies, given a list of length 10 in Python, the extent is [ 0, 9 ].
if positive_negative_separate:
  plt.plot( [ header[2], header[3] ], [ horizon_low, horizon_high ],"b")
  plt.plot( [ header[2], header[3] ], [ -horizon_low, -horizon_high ],"b") 
else:
  plt.plot( [ header[2], header[3] ], [ horizon_low, horizon_high ],"b")



ax1 = plt.gca()
ax1.tick_params("both" ,direction="out")
ax2 = ax1.twinx()
ax2.set_ylabel("$Delay\ (ns)$", fontsize=15, labelpad=-9)
# Get delays
for line in open(sys.argv[1]): pass
delay_bottom = float(line.split()[6].split(":")[0])
delay_top = float(line.split()[6].split(":")[1])
ticks = [ -1500, -1000, -500, 0, 500, 1000, 1500 ]
normalize_ticks = [ (ticks[i]-delay_bottom)/(delay_top-delay_bottom) for i in range(len(ticks)) ]
labels = [ str(ticks[i]) for i in range(len(ticks)) ]
ax2.set_yticks(normalize_ticks)
ax2.set_yticklabels(ticks)
ax2.tick_params("both", direction="out")
ax3 = ax1.twiny()
# Get UV
uv_bottom = float(line.split()[7].split(":")[0])
uv_top = float(line.split()[7].split(":")[1])
ticks = [ 1, 11, 21, 31 ]
normalize_ticks = [ (ticks[i]-uv_bottom)/(uv_top-uv_bottom) for i in range(len(ticks)) ]
labels = [ str(ticks[i]) for i in range(len(ticks)) ]
ax3.set_xticks(normalize_ticks)
ax3.set_xticklabels(ticks)
ax3.set_xlabel("$UV length$", fontsize=15, labelpad=-1)
ax3.tick_params("both" ,direction="out")
plt.tight_layout()
plt.savefig("kk_space_log.png")
#plt.show()
