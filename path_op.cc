#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <string>

using namespace std;

char *basename(char *path);

// https://stackoverflow.com/questions/2736753/how-to-remove-extension-from-file-name
// remove_ext: removes the "extension" from a file spec.
//   mystr is the string to process.
//   dot is the extension separator.
//   sep is the path separator (0 means to ignore).
// Returns an allocated string identical to the original but
//   with the extension removed. It must be freed when you're
//   finished with it.
// If you pass in NULL or the new string can't be allocated,
//   it returns NULL.



string remove_ext (char* mystr, char dot, char sep) {
    char *lastdot, *lastsep;
    char retstr[PATH_MAX];


    // Error checks and allocate string.

    if (mystr == NULL)
        return NULL;

    memset(retstr, 0, PATH_MAX);

    // Make a copy and find the relevant characters.

    strcpy (retstr, mystr);
    lastdot = strrchr (retstr, dot);
    lastsep = (sep == 0) ? NULL : strrchr (retstr, sep);

    // If it has an extension separator.

    if (lastdot != NULL) {
        // and it's before the extenstion separator.

        if (lastsep != NULL) {
            if (lastsep < lastdot) {
                // then remove it.

                *lastdot = '\0';
            }
        } else {
            // Has extension separator with no path separator.

            *lastdot = '\0';
        }
    }

    // Return the modified string.

    return string(retstr);
}


string strip_file_name(char *name) {
  return remove_ext(basename(name), '.','/');
}

/*void main() {
  puts(strip_file_name("/mnt/md0/pipe/uvdump_t6.uvfits"));
}*/

