/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <cstring>
#include <fstream>
#include <mutex>
#include "fftw3.h"
#include "conversions.h"
#include "power_spectrum.h"
#ifdef COHERENT
#include "noise_settings.h"
#include "sfactor.h"
#endif

extern void check_not_null(void *a, const char *message);
extern void make_window(double*, const char*, int);
extern "C" void nfft_forward(double **vis, double **locations, int len);

#define REAL 0
#define IMAG 1
#define GRID_SIZE 1024
#define MAX_LENGTH 600  //meters
#define SQR(x) ((x)*(x))

using namespace std;

mutex fft_mtx;

const int c = 299792458;

// Utility functions

static void switch_complex(complex<double> c, fftw_complex& fftc) {
  fftc[REAL] = c.real();
  fftc[IMAG] = c.imag();
}

static complex<double>switch_complex(fftw_complex fftc) {
  return complex<double>(fftc[REAL], fftc[IMAG]);
}

static double fft_index_to_delay(const double fft_index, const int fft_size, int CHAN_WIDTH) {
  return (double)fft_index/fft_size/CHAN_WIDTH*1e9;
}

static int delay_to_fft_index(const double delay, const int fft_size, int CHAN_WIDTH) {	
	// index is relative to centre of FFT (DC component) index 0 means fft_size/2 index in FFT
						// delay in nanosec
  return (int)round((delay*fft_size*CHAN_WIDTH)/1e9);
}

static double delay_to_fft_location(const double delay, const int fft_size, int CHAN_WIDTH) {    // index is relative to centre of FFT (DC component) index 0 means fft_size/2 index in FFT
                                                // delay in nanosec. Same function as above but returns double.
  return (delay*fft_size*CHAN_WIDTH)/1e9;
}

static void do_nfft(fftw_complex *in, fftw_complex *out, int len) {
	double *values, *locations;
	
	values = new double[2*len]; check_not_null(values, "in do_nfft");
	locations = new double[len]; check_not_null(locations, "in do_nfft");
	for (int i=0; i<len; ++i) {
		values[2*i] = in[i][REAL]; 
		values[2*i+1] = in[i][IMAG];
		locations[i] = i;
	}
	nfft_forward(&values, &locations, len);
	for (int i=len/2; i<len; ++i) {		// copy and shift
		out[i-len/2][REAL] = values[2*i]*len;
		out[i-len/2][IMAG] = values[2*i+1]*len;
	}
	for (int i=0; i<len/2; ++i) {		// copy and shift
		out[i+len/2][REAL] = values[2*i]*len;
		out[i+len/2][IMAG] = values[2*i+1]*len;
	}
	// Now reverse. All these manipulations are for complex numbers not real
        // Must have even length
	for (int i=1; i<len/2; ++i) {
          fftw_complex tmp;
          tmp[REAL] = out[i][REAL]; tmp[IMAG] = out[i][IMAG];
          out[i][REAL] = out[len-i][REAL]; out[i][IMAG] = out[len-i][IMAG]; 
          out[len-i][REAL] = tmp[REAL]; out[len-i][IMAG] = tmp[IMAG];
        }
        // Fix sign
	for (int i=0; i<len; ++i) 
	  if ( i%2 == 1 ) {
	    out[i][REAL] = -out[i][REAL];
	    out[i][IMAG] = -out[i][IMAG];
	  }
	delete[] values; delete[] locations;
	
}

PowerSpectrum::PowerSpectrum(const PowerSpectrum& ps) {

  frequencies = new int[ps.num_channels]; check_not_null(frequencies, "in PowerSpectrum constructor from existing");
  memcpy(frequencies, ps.frequencies, ps.num_channels*sizeof(int));
  num_channels = ps.num_channels;
  fft_size = ps.fft_size;
  chan_width = ps.chan_width;
  max_length = ps.max_length;
  which_pol = ps.which_pol;
  positive_negative_separate = ps.positive_negative_separate;
  memcpy(window, ps.window, 64);

  kk_space = ps.kk_space;
  reduced_kk_space = ps.reduced_kk_space;

}

PowerSpectrum& PowerSpectrum::operator=(const PowerSpectrum& ps) {

  if ( frequencies != NULL ) delete[] frequencies;
  frequencies = new int[ps.num_channels]; check_not_null(frequencies, "in PowerSpectrum operator=");
  memcpy(frequencies, ps.frequencies, ps.num_channels*sizeof(int));

  num_channels = ps.num_channels;
  fft_size = ps.fft_size;
  chan_width = ps.chan_width;
  max_length = ps.max_length;
  which_pol = ps.which_pol;
  positive_negative_separate = ps.positive_negative_separate;
  memcpy(window, ps.window, 64);

  kk_space = ps.kk_space;
  reduced_kk_space = ps.reduced_kk_space;


}


#ifdef COHERENT
// https://docs.google.com/presentation/d/1umzx7IVMMxkkbO48nX8SJ4xuh0otaWsswXGNiHLKYII/edit?usp=sharing
#define NUM_SAMPLES HOURS*400
#define COHERENT_NUM_SAMPLES (MINUTES*60)/9
complex<double> **generate_noise_values(int num_channels, int num_baselines, int which_pol, int scrunch, int& num_to_sum) {
    stringstream command;
    command << "./noise_integration " << num_channels << " " << num_baselines << " ";
    if ( which_pol == 0 ) { 
      double average_noise = (NOISE_RMS_REAL_0+NOISE_RMS_IMAG_0)/2/sqrt(scrunch); command << average_noise << " " << average_noise;
    } else if ( which_pol == 1 ) { 
      double average_noise = (NOISE_RMS_REAL_1+NOISE_RMS_IMAG_1)/2/sqrt(scrunch); command << average_noise << " " << average_noise;
    } else {
      cerr << "Unimplemented pol " << which_pol << " for COHERENT\n";
      exit(1);
    }
    command << " " << NUM_SAMPLES << " " << (int)round(COHERENT_NUM_SAMPLES);
    system(command.str().c_str());
    complex<double> **noise = new complex<double>*[num_channels]; check_not_null(noise, "for noise array for COHERENT");
    for (int i=0; i<num_channels; ++i) {
      noise[i] = new complex<double>[num_baselines]; check_not_null(noise[i], "for noise array for COHERENT baselines");
    }
    ifstream infile("noise.dat");
    if ( infile.is_open() ) {
      infile >> num_to_sum;
      cout << "Num power values over time from sim " << num_to_sum << endl;
      for (int ch=0; ch<num_channels; ++ch) {
        cout << ch << " " << flush;
        for (int bl=0; bl<num_baselines; ++bl) {
            double d1, d2;
            infile >> d1; infile >> d2;
            noise[ch][bl] = complex<double>(d1, d2);
        }
      }
      cout << endl;
      infile.close();
    } else {
      cerr << "Failed to open noise file\n";
      exit(1);
    }
    return noise;
}
#endif


// Visibility data is Fourier transformed and gridded by baseline and channel
PowerSpectrum::PowerSpectrum(char *name, int dump_index, Visibilities& vis, const int fft_size_num, const char *window_s, const bool p_n_sep, const int pol) {		// only 1 time step in vis. p_n_sep means positive/negative FFT sides separate
  double *window_data;
  fftw_complex *in, *out;  
  fftw_plan plan;
  double horizon, fft_location;

  // Save parameters
  fft_size = fft_size_num;
  strcpy(window, window_s);
  positive_negative_separate = p_n_sep;
  fname = name; dump = dump_index;

  // Get stuff from vis  
  frequencies = new int[vis.num_channels]; check_not_null(frequencies, "in PowerSpectrum");
  for (int i=0; i<vis.num_channels; ++i) frequencies[i] = vis.frequencies[i];
  num_channels = vis.num_channels;
  max_length = vis.max_length();
  chan_width = vis.chan_width;
  lst_seconds = vis.lst_seconds;
  which_pol = pol;

  // Setup window for FFT
  window_data = new double[vis.num_channels]; check_not_null(window_data, "in PowerSpectrum");
  make_window(window_data, window, vis.num_channels);

  // Setup arrays for FFT
  fft_mtx.lock();	// fftw is not thread safe
  in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft_size); check_not_null(in, "for fft in PowerSpectrum");
  out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * fft_size); check_not_null(out, "for fft in PowerSpectrum");
  plan = fftw_plan_dft_1d(fft_size, in, out, FFTW_FORWARD, FFTW_ESTIMATE); check_not_null(plan, "for fft in PowerSpectrum");
  for (int i=0; i<fft_size; ++i) {
    in[i][REAL] = in[i][IMAG] = 0;
    out[i][REAL] = out[i][IMAG] = 0;
  }


  // Setup 2-D grid for kk_space. See Grid class. One axis is baseline, the other is just the FFT indexes
  // of the FFT which later are converted to k_par. 
  if ( positive_negative_separate ) {
    kk_space.setup(vis.num_baselines, fft_size, 0, vis.num_baselines, -fft_size/2, fft_size/2, 2);
  } else {
    kk_space.setup(vis.num_baselines, fft_size/2, 0, vis.num_baselines, 0, fft_size/2, 2);
  }

#ifdef COHERENT
  int num_to_sum;
  complex<double> **noise = generate_noise_values(vis.num_channels, vis.num_baselines, which_pol, vis.scrunch, num_to_sum);
#endif 

  // File for all baselines separated
  ofstream blfile;
  blfile.open ("bl.dat");

  // Scan all baselines
  for (int bl=0; bl<vis.num_baselines; ++bl) {
    if ( vis.num_used_in_baseline(bl) > 0 ) {
      // If there is at least 1 value to use in the channel, then the FFT will be
      // everywhere non-zero. Baselines would usually be preprocessed (in Visibilities)
      // to be either totally flagged or totally unflagged (after interpolation/deconvolution).

      if ( bl > 0 && bl%1000 == 0 ) cout << "FFT " << bl << endl;
    
      kk_space.set_row_tag(bl, 0, vis.visibilities[num_channels/2][bl].k_perp);		// Attach a k_perp value to each baseline
      kk_space.set_row_tag(bl, 1, vis.visibilities[0][bl].length/c*1e9);	// Attach a horizon in ns to each baseline
      
      // Load visibilities for this baseline into "in" array for FFT
      for (int ch=0; ch<vis.num_channels; ++ch) 
        switch_complex(vis.visibilities[ch][bl].value*window_data[ch], in[ch]);

      fftw_execute(plan);

#ifdef COHERENT
      // Replace values with noise generated values that mimic that the FFT has been done.
      // Variance of re/imag should go up by fft_size, to simulate that, multiply re/imag values by sqrt(fft_size)
      // https://mysupercomputerexperience.blogspot.com/2019/05/scrunching-fft-and-physical-units.html
      // Note that because of the test in the "if .." above, totally flagged baselines will not be filled by
      // coherent noise. This is so we can honour flagged stands and baselines, which are indicated
      // by at least 1 value to use (and usually pre-processed to always be full or totally flagged).
      for (int ch=0; ch<fft_size; ++ch) {
        out[ch][REAL] = noise[ch][bl].real()*sqrt(fft_size);
        out[ch][IMAG] = noise[ch][bl].imag()*sqrt(fft_size);
      }

      // Here is a Python script that simulates N channels (e.g. 436), with stdev=1,
      // being scrunched by 4 and then FFT'd.
      /*
      real = np.random.normal(size=N/4, scale=np.sqrt(1/4.0))*np.sqrt(N/4)	
      imag = np.random.normal(size=N/4, scale=np.sqrt(1/4.0))*np.sqrt(N/4)

      data = np.zeros(len(real), dtype=np.complex64)
      for i in range(len(real)):
        data[i] = complex(real[i], imag[i])

      print np.var((data))
      */
      // The np.sqrt(1/4.0) reduces the stdev to simulate scrunching
      // The np.sqrt(N/4) scales up by the FFT length after scrunching
      // The size=N/4 gives the correct length array at the end
      // Gives sames result as if you did the scrunch then the FFT. 

#endif
 
      //do_nfft(in, out, fft_size);

      // normalize  - https://www.mathworks.com/matlabcentral/answers/162846-amplitude-of-signal-after-fft-operation
      //		https://stackoverflow.com/questions/4855958/normalising-fft-data-fftw
      for (int i=0; i<fft_size; ++i) {
        out[i][REAL] *= chan_width;
        out[i][IMAG] *= chan_width;
      }

      // Write raw information
      blfile << vis.visibilities[0][bl].st1 << " " << vis.visibilities[0][bl].st2 << " " << vis.visibilities[0][bl].k_perp << " ";
      if ( positive_negative_separate ) {
        for (int i=fft_size/2; i<fft_size; ++i) blfile << abs(switch_complex(out[i])) << " ";
        for (int i=0; i<fft_size/2; ++i) blfile << abs(switch_complex(out[i])) << " ";
      } else {
        blfile << abs(switch_complex(out[0])) << " ";
        for (int i=1; i<fft_size/2; ++i) blfile << abs(switch_complex(out[i])+switch_complex(out[fft_size-i])) << " ";
      }
      blfile << endl;
      
 
      // Fold FFT
      if ( !positive_negative_separate ) 
        // fold in negative channels to positive channels in FFT. Sum them - https://www.mathworks.com/matlabcentral/answers/162846-amplitude-of-signal-after-fft-operation
        for (int i=1; i<fft_size/2; ++i) {
          out[i][REAL] += out[fft_size-i][REAL];
          out[i][IMAG] += out[fft_size-i][IMAG];
        }
       
      // Add into grid. Grid index 1 is bl, index 2 is just the FFT index

#if 1	
      // NORMAL OPERATION
      for (int i=0; i<fft_size/2; ++i) {
        kk_space.inc_at(bl, i, switch_complex(out[i]));
      }

      if ( positive_negative_separate ) 
        for (int i=-fft_size/2; i<0; ++i) {
          kk_space.inc_at(bl, i, switch_complex(out[i+fft_size]));    //Eg. 0 1 2 3 -4 -3 -2 -1
        }  
#else
      // VISIBILITY DENSITY
      // This is for visibility density. Also must turn off weighting on command line.
      for (int i=0; i<fft_size/2; ++i) {
        kk_space.inc_at(bl, i, complex<double>(1, 0));
      }

      if ( positive_negative_separate ) 
        for (int i=-fft_size/2; i<0; ++i) {
          kk_space.inc_at(bl, i, complex<double>(1, 0));    //Eg. 0 1 2 3 -4 -3 -2 -1
        }  
#endif
    } else {
      blfile << vis.visibilities[0][bl].st1 << " " << vis.visibilities[0][bl].st2 << 
		" " << vis.visibilities[0][bl].k_perp << " ";
      for (int i=0; i<fft_size/(positive_negative_separate?1:2); ++i) blfile << "0 "; blfile << endl;
    }
  }
  blfile.close();

  fftw_destroy_plan(plan);
  fftw_free(in); fftw_free(out);
  delete[] window_data;
  fft_mtx.unlock();

  // Check nothing is weighted twice
  for (int i=0; i<kk_space.dim1; ++i)
	  for (int j=0; j<kk_space.dim2; ++j)
		  if ( kk_space.weights[i][j] > 1 )
			  cout << "WARNING: some baselines have been added together\n";


}

double get_k_perp_bin_size(int lowest_freq) {
  const double required_uv_length = 0.5;
  Conversions conv;
  double z21cm = conv.f2z(lowest_freq/1e9);
  return conv.k_perp(required_uv_length, z21cm);
}



// Scrunch the baselines into a k_perp axis. Effectively a binning.
void PowerSpectrum::reduce_by_k_perp(PowerSpectrum *ps, bool setup) {	
  double max_k_perp, k_perp, k_perp_bin_size;
  double bl;
  int grid_size;


  // Find out the bin size corresponding to uv = 0.5 at the lowest frequency
  k_perp_bin_size = get_k_perp_bin_size(frequencies[0]);

 
  // Setup 2-D grid for kk_space. See Grid class. One axis is k_perp, the other is just the FFT indexes
  // of the FFT which later are converted to k_par. There are two windows into the grid, the secondary
  // one is indexed the same on x-axis but y-axis is delay
  max_k_perp = 0;
  for (int bl=(int)ps->kk_space.left; bl<(int)ps->kk_space.right; ++bl)
    if ( ps->kk_space.get_row_tag(bl, 0) > max_k_perp ) max_k_perp = ps->kk_space.get_row_tag(bl, 0);
#if 1	
  // Sets the bin size based on uv. Otherwise, just use a hardwired size
  grid_size = ceil(max_k_perp/k_perp_bin_size)+1;
  max_k_perp *= (double)grid_size/(grid_size-1);
#else
  max_k_perp *= (double)GRID_SIZE/(GRID_SIZE-1);
  grid_size = GRID_SIZE;
#endif

  if ( setup ) {
    reduced_kk_space.setup(grid_size, fft_size, 0, max_k_perp, ps->kk_space.bottom, ps->kk_space.top, 1);
    reduced_kk_space.set_window2_extent(0, max_k_perp, fft_index_to_delay(ps->kk_space.bottom, fft_size, chan_width), fft_index_to_delay(ps->kk_space.top, fft_size, chan_width)); // secondary
  } else if ( reduced_kk_space.dim1 == 0 ) {
    cerr << "ERROR: reduced_kk_space not set up\n";
    exit(1);
  }
  cout << "k_perp bin size " << k_perp_bin_size << endl;

  // Scan all baselines
  for (int di=0; di<ps->kk_space.dim1; ++di) {

    ps->kk_space.x_grid2physical(di, bl); 		// could be bl index or k_perp value
    if ( bl > 0 && (int)bl%1000 == 0 ) cout << (int)bl << endl;

    k_perp = ps->kk_space.get_row_tag(bl, 0);
    	
    // Check 
    bool all_zero = true;
    bool same_weights = true;
    bool one_zero = false;
    for (int ch=(int)ps->kk_space.bottom; ch<(int)ps->kk_space.top; ++ch) {
      if ( abs(ps->kk_space.value_at(bl, ch)) != 0 ) all_zero = false;
      if ( ps->kk_space.weight_at(bl, ch) != ps->kk_space.weight_at(bl, 0) ) same_weights = false;
      if ( abs(ps->kk_space.value_at(bl, ch)) == 0 ) one_zero = true;
    }
    if ( (one_zero && !all_zero) || !same_weights ) {
      cerr << "Bad " << one_zero << " " << all_zero << " " << same_weights << endl;
      exit(1);
    } 
    
    // Combine
    for (int ch=(int)ps->kk_space.bottom; ch<(int)ps->kk_space.top; ++ch) {
      if ( abs(ps->kk_space.value_at(bl, ch)) != 0 && ps->kk_space.weight_at(bl, ch) > 0 ) 
        reduced_kk_space.inc_at(k_perp, ch, ps->kk_space.value_at(bl, ch));
    }
              

      // Check going beyond horizon
      /*double horizon, amp, delay, peak=-1, where_peak;
      int fft_index;
      for (int i=0; i<fft_size; ++i)
    	  if ( (amp=abs(switch_complex(out[i]))) > peak ) { peak = amp; where_peak = i; }
      	  // don't have a shifted FFT so emulate
      if ( where_peak >= fft_size/2 ) where_peak -= fft_size;
      horizon = vis.visibilities[0][bl].length/c*1e9;
      // Find out what fft_index this horizon would be, round up to integer, but high precision
      fft_index = (int)ceil(delay_to_fft_location(horizon, fft_size, chan_width));
      if ( abs(where_peak) > fft_index ) cout << "bl " << bl << " beyond horizon " << where_peak << " " << fft_index << endl;*/
      
      // Find horizon, round up to pixel resolution and a bit
    double horizon; double fft_location;
    horizon = ps->kk_space.get_row_tag(bl, 1);   // ns
    fft_location = ceil(delay_to_fft_location(horizon, fft_size, chan_width))+HORIZON_ADD_ON;
    if ( fft_location > reduced_kk_space.get_row_tag(k_perp, 0) ) reduced_kk_space.set_row_tag(k_perp, 0, fft_location);

    
     
  }
    

}

void PowerSpectrum::clip(int max_delay) {
  double max_delay_index, fft_index; 

 // Now some clipping operations.
  // 1. Clip out high delays so the FFT is shorter.
  // 2. Clip any borders where there is no data.
  // At this point the units of the space are (length, fft_index) and in the secondary window (length, delay)

  if ( reduced_kk_space.dim1 == 0 ) {
    cerr << "Error: reduced_kk_space is empty in clip()\n";
    exit(1);
  }
  cout << "Clipping using " << max_delay << "\n";

  // Find out which fft index has delay of MAX_DELAY and clip
  reduced_kk_space.y_physical2grid_window2(max_delay, max_delay_index);
  reduced_kk_space.y_grid2physical(max_delay_index, fft_index);
  
  cout << "Delay of " << max_delay << " -> FFT location " << fft_index << " resolution " << max_delay/fft_index << " ns/pixel" << endl;
  if ( positive_negative_separate ) reduced_kk_space.clip(reduced_kk_space.left, reduced_kk_space.right, -fft_index, fft_index+1);
  else reduced_kk_space.clip(reduced_kk_space.left, reduced_kk_space.right, 0, fft_index+1);
  reduced_kk_space.print_sizes();
   
  // Clip any zero borders
  reduced_kk_space.auto_clip();
  reduced_kk_space.print_sizes(); 
}

// this_ps *= ps
PowerSpectrum& PowerSpectrum::operator*=(const PowerSpectrum& ps) {
  if ( !kk_space.same_size(ps.kk_space) ) {
    cerr << "ERROR: attempt to multiply power spectra that don't have the same co-ordinates and ranges\n";
	// it is possible to combine overlapping grids but I don't see when that would happen, and it would leave some values not operated on
    exit(1);
  }

  // Straight multiplication but the weights complicate things. A zero weight means "don't use", so we have to ditch that grid value.
  // Multiplication does not change the weights, only addition does
  for (int i=0; i<kk_space.dim1; ++i) 
    for (int j=0; j<kk_space.dim2; ++j) {
      if ( kk_space.weights[i][j] == 0 || ps.kk_space.weights[i][j] == 0 ) {
        kk_space.weights[i][j] = 0;
        kk_space.grid[i][j] = 0;
      } else {
        kk_space.grid[i][j] *= conj(ps.kk_space.grid[i][j]);
	if ( kk_space.weights[i][j] != 1  && ps.kk_space.weights[i][j] != 1 ) {				
		// When multiplication is done it should be on FFT'd weight=1 data. We leave the weight=1 for the result of the multiply
          cerr << "Error: Unexpected: multiplying a grid value with weight > 1 " << kk_space.weights[i][j] << " " << ps.kk_space.weights[i][j] << endl;
          exit(1);
        }
      }
    }
  kk_space.raise_row_tags(ps.kk_space.row_tags[0], 0);

  return *this;
}

// this_ps += ps 
PowerSpectrum& PowerSpectrum::operator+=(const PowerSpectrum& ps) {
  if ( !kk_space.same_size(ps.kk_space) ) {
    cerr << "ERROR: attempt to add power spectra that don't have the same co-ordinates and ranges\n";
    exit(1);
  }

  // Addition of two grids changes the weights in the modified grid, for averaging purposes.

  for (int i=0; i<kk_space.dim1; ++i) 
    for (int j=0; j<kk_space.dim2; ++j) {
      if ( kk_space.weights[i][j] == 0 && ps.kk_space.weights[i][j] == 0 ) {
        kk_space.grid[i][j] = 0;
      } else if ( kk_space.weights[i][j] == 0 && ps.kk_space.weights[i][j] != 0 ) {
        kk_space.weights[i][j] = 1;
        kk_space.grid[i][j] = ps.kk_space.grid[i][j];
      } else if ( kk_space.weights[i][j] != 0 && ps.kk_space.weights[i][j] == 0 ) {
        // Do nothing
      } else if ( kk_space.weights[i][j] != 0 && ps.kk_space.weights[i][j] != 0 ) {   
	  // We have values to add 
        kk_space.grid[i][j] += ps.kk_space.grid[i][j];
        kk_space.weights[i][j] += ps.kk_space.weights[i][j];
	if ( kk_space.weights[i][j] != 1 && ps.kk_space.weights[i][j] != 1 ) {		// When addition is done it should be on FFT'd weight=0/1 data
          cerr << "Error: Unexpected: accumulating a grid value with weight > 1 " << kk_space.weights[i][j] << " " << ps.kk_space.weights[i][j] << endl;
          exit(1);
        }
      }
    }

  kk_space.raise_row_tags(ps.kk_space.row_tags[0], 0);

  return *this;
}

// this_ps /= f
PowerSpectrum& PowerSpectrum::operator/=(const double f) {

  for (int i=0; i<kk_space.dim1; ++i) 
    for (int j=0; j<kk_space.dim2; ++j) {
      kk_space.grid[i][j] /= f;
    }
  return *this;
}

void PowerSpectrum::calc_power() {
  Conversions conv;
  const double kB = 1.38064852e-23;
  double omega;
  double mean_frequency, lambda, B;
  complex<double> P, V;

  // Now all the values have to be converted to power using Eqn 12 in Parsons 2012
  // Needed parameters are as follows:
  // Ω (primary beam) = 1
  // X2Y is in the conversion routines
  // B is the bandwidth. We have stored the frequencies observed so can get this
  // λ is mean wavelength, can get from mean frequency
  // Visibilities start in units of Jansky but then are delay transformed. See notes from paper

  if ( reduced_kk_space.dim1 == 0 ) {
    cerr << "Error: reduced_kk_space is empty in calc_power()\n";
    exit(1);
  }

  mean_frequency = 0;
  for (int i=0; i<num_channels; ++i) mean_frequency += frequencies[i];
  mean_frequency /= num_channels;

  //cout << "B and mean freq wrong in calc_power\n";
  lambda = c/mean_frequency;
  B = num_channels*chan_width;
  omega = 1;
  cout << "Bandwidth " << B << " mean lambda " << lambda << " omega " << omega << endl;
	
  for (int i=0; i<reduced_kk_space.dim1; ++i) {

    for (int j=0; j<reduced_kk_space.dim2; ++j) {

      P = reduced_kk_space.grid[i][j];     // Complex type, but P(k) is supposed to be calculated from V squared not V
      P *= 1e-26/kB;
      P *= 1e-26/kB; 
      P *= SQR(SQR(lambda)/2)*(conv.X2Y(GHz(mean_frequency), GHz(chan_width))/(omega*B));
      reduced_kk_space.grid[i][j] = 1e-3*P; 
    }
  }
}


#ifdef BIN
void PowerSpectrum::save(const char *fname) {
  Conversions conv;
  double k_par_1, k_par_top, k_par_bottom, k_perp_left, k_perp_right, horizon_left, horizon_right, len;
  double horizon_left_grid, horizon_right_grid;

  // Change delay axis to k_par units. This just means chaging the "extent" of the axis, cf. matplotlib imshow but not identical. The end values are beyond the data, like Python.
  // In matplotlib they are the end data.
  // Each FFT index has a unique k_par because these are the modes of the FFT. Each FFT index also has a unique delay.
  // Lots of conversions needed.

  // At this point the units of the space are (bin, fft_index) and in the secondary window (bin, delay)
  // Calculate the extent of the space in k_perp/k_par
  


  k_par_1 = conv.k_par(GHz(frequencies[0]), fft_size, GHz(chan_width));    // 2 pi/longest wavelength, mode 1
  k_par_bottom = kk_space.bottom*k_par_1;                       // kk_space.bottom..top  is an FFT index from 0...fft_size/2 or -fft_size/2..fft_size/2
  k_par_top = kk_space.top*k_par_1;
  
  // Convert horizon to grid co-ords
  horizon_left = kk_space.row_tags[0];
  horizon_right = kk_space.row_tags[kk_space.dim1-1];
  kk_space.y_physical2grid(horizon_left, horizon_left_grid);
  kk_space.y_physical2grid(horizon_right, horizon_right_grid);

  // Convert horizon to delay, for printing
  kk_space.y_grid2physical_window2(horizon_right_grid, horizon_right);
  cout << "Horizon at max k_perp " << horizon_right << endl;
  
  k_perp_left = kk_space.left2;
  k_perp_right = kk_space.right2;

  // Switch units to k_perp/k_par
  kk_space.set_new_extent(k_perp_left, k_perp_right, k_par_bottom, k_par_top);
  
  // Get horizon in k_par
  kk_space.y_grid2physical(horizon_left_grid, horizon_left);
  kk_space.y_grid2physical(horizon_right_grid, horizon_right);
  
  kk_space.print_sizes();

  kk_space.save_grid(fname);
  ofstream myfile;
  myfile.open (fname, ios::app); 

  myfile << "# " << fft_size << " " << window << " " << (positive_negative_separate?"True":"False") << " " << horizon_left << ":" << horizon_right << " slow\n";

  myfile.close();
}
#else
#ifdef NFFT_EQ
void PowerSpectrum::save(const char *fname) {
  Conversions conv;
  double k_par_1, k_par_top, k_par_bottom, k_perp_left, k_perp_right, horizon_left, horizon_right, len;
  double horizon_left_grid, horizon_right_grid;

  // Change delay axis to k_par units. This just means chaging the "extent" of the axis, cf. matplotlib imshow but not identical. The end values are beyond the data, 
  // like Python. In matplotlib they are the end data.
  // Each FFT index has a unique k_par because these are the modes of the FFT. Each FFT index also has a unique delay.
  // Lots of conversions needed.

  // At this point the units of the space are (baseline, fft_index) and in the secondary window (baseline, delay)
  // Calculate the extent of the space in k_perp/k_par
  
  k_perp_left = kk_space.left2;
  k_perp_right = kk_space.right2;


  k_par_1 = conv.k_par(GHz(frequencies[0]), fft_size, GHz(chan_width));    // 2 pi/longest wavelength, mode 1
  k_par_bottom = kk_space.bottom*k_par_1;                       // kk_space.bottom..top  is an FFT index from 0...fft_size/2 or -fft_size/2..fft_size/2
  k_par_top = kk_space.top*k_par_1;
  
  // Convert horizon to grid co-ords
  horizon_left = kk_space.row_tags[0];
  horizon_right = kk_space.row_tags[kk_space.dim1-1]; 
  kk_space.y_physical2grid(horizon_left, horizon_left_grid);
  kk_space.y_physical2grid(horizon_right, horizon_right_grid);

  // Convert horizon to delay, for printing
  kk_space.y_grid2physical_window2(horizon_right_grid, horizon_right);
  cout << "Horizon at max k_perp " << horizon_right << endl;
  

  // Switch units to k_perp/k_par
  kk_space.set_new_extent(k_perp_left, k_perp_right, k_par_bottom, k_par_top);
  
  // Get horizon in k_par
  kk_space.y_grid2physical(horizon_left_grid, horizon_left);
  kk_space.y_grid2physical(horizon_right_grid, horizon_right);
  
  kk_space.print_sizes();

  kk_space.save_grid(fname);
  ofstream myfile;
  myfile.open (fname, ios::app); 

  myfile << "# " << fft_size << " " << window << " " << (positive_negative_separate?"True":"False") << " " << horizon_left << ":" << horizon_right << " slow\n";

  myfile.close();
}
#else
double k_perp_to_uv(double kp, int frequency) {
    Conversions conv;
        
    // Convert to kp as k_perp and make things consistent
    double z21cm = conv.f2z(GHz(frequency));

    double k_perp = kp;
    double uv_length;
    if ( kp == 0 ) uv_length = 0;
    else uv_length = conv.uv_length(k_perp, z21cm);
    return uv_length;
}


void PowerSpectrum::save(const char *fname) {
  Conversions conv;
  double k_par_1, k_par_top, k_par_bottom, k_perp_left, k_perp_right, horizon_left, horizon_right, len;
  double horizon_left_grid, horizon_right_grid, delay_bottom, delay_top;

  if ( reduced_kk_space.dim1 == 0 ) {
    cerr << "Error: reduced_kk_space is empty in save()\n";
    exit(1);
  }

  // Change delay axis to k_par units. This just means chaging the "extent" of the axis, cf. matplotlib imshow but not identical. The end values are beyond the data, 
  // like Python. In matplotlib they are the end data.
  // Each FFT index has a unique k_par because these are the modes of the FFT. Each FFT index also has a unique delay.
  // Lots of conversions needed.

  // At this point the units of the space are (length, fft_index) and in the secondary window (length, delay)
  // Calculate the extent of the space in k_perp/k_par
  
  k_perp_left = reduced_kk_space.left;
  k_perp_right = reduced_kk_space.right;


  k_par_1 = conv.k_par(GHz(frequencies[0]), fft_size, GHz(chan_width));    // 2 pi/longest wavelength, mode 1
  k_par_bottom = reduced_kk_space.bottom*k_par_1;                       // kk_space.bottom..top  is an FFT index from 0...fft_size/2 or -fft_size/2..fft_size/2
  k_par_top = reduced_kk_space.top*k_par_1;
  
  // Convert horizon to grid co-ords, will be the same if no positive_negative_separate,
  // but will be shifted if they are separate
  horizon_left = reduced_kk_space.row_tags[0][0];
  horizon_right = reduced_kk_space.row_tags[0][reduced_kk_space.dim1-1]; 
  reduced_kk_space.y_physical2grid(horizon_left, horizon_left_grid);
  reduced_kk_space.y_physical2grid(horizon_right, horizon_right_grid);

  // Find what delay we actually have
  reduced_kk_space.y_grid2physical_window2(0, delay_bottom);
  reduced_kk_space.y_grid2physical_window2(reduced_kk_space.dim2-1, delay_top);


  // Convert horizon to delay, for printing
  reduced_kk_space.y_grid2physical_window2(horizon_right_grid, horizon_right);
  cout << "Horizon at max k_perp " << horizon_right << endl;

  // Switch units to k_perp/k_par
  reduced_kk_space.set_new_extent(k_perp_left, k_perp_right, k_par_bottom, k_par_top);
  
  // Get horizon in k_par. Horizon values are just grid values
  reduced_kk_space.y_grid2physical(horizon_left_grid, horizon_left);
  reduced_kk_space.y_grid2physical(horizon_right_grid, horizon_right);
  
  reduced_kk_space.print_sizes();

  reduced_kk_space.save_grid(fname);
  ofstream myfile;
  myfile.open (fname, ios::app); 

  myfile << "# " << fft_size << " " << window << " " << (positive_negative_separate?"True":"False") << " " << horizon_left << ":" << horizon_right << " slow " << delay_bottom << ":" << delay_top << " " << 
		k_perp_to_uv(k_perp_left, frequencies[0]) << ":" << k_perp_to_uv(k_perp_right, frequencies[0])  << " " << which_pol << endl;

  myfile.close();
}
#endif
#endif
 
