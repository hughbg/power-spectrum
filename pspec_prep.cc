// This is a rip-off of pspec_prep developed for HERA.
// Not my code.

#include <complex>
#include <cmath>
#include <cstdlib>
#include <fftw3.h>
#include <iostream>
#include <mutex>
using namespace std;

struct Info {
  bool success;
  fftw_complex *res, *_d_cl;
  double tol;
  int iter;
  string term;
  double score;
};

extern mutex fft_mtx;		// fftw is not reentrant

static void print_info(Info info) {
    cout << "Success: " << info.success << " To:  " << info.tol << " Iter: " << info.iter << " Term: " << info.term << " Score: " << info.score << endl;
}

static void fft(fftw_complex* in, fftw_complex *out, int N, int sign) {
    fft_mtx.lock();
    fftw_plan p;

    //in = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    //out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * N);
    p = fftw_plan_dft_1d(N, in, out, sign, FFTW_ESTIMATE);

    fftw_execute(p); 

    fftw_destroy_plan(p);
    fft_mtx.unlock();
}

extern void print_c(fftw_complex);

static void print_ca(fftw_complex *data, int length) {
    for (int i=0; i<4; ++i) cout << data[i][0] << " " << data[i][1] << endl; cout << "--\n";
    for (int i=length-4; i<length; ++i) cout << data[i][0] << " " << data[i][1] << endl;
}

static void print_caa(fftw_complex *data, int length) {
    for (int i=0; i<length; ++i) cout << data[i][0] << " " << data[i][1] << endl; 
}

static void scale(fftw_complex *data, int length) {
    for (int i=0; i<length; ++i) { data[i][0] /= length; data[i][1] /= length; }
}

static void print_da(double *data, int length) {
    for (int i=0; i<4; ++i) cout << data[i] << " " << data[i] << endl; cout << "--\n";
    for (int i=length-4; i<length; ++i) cout << data[i] << " " << data[i] << endl;
}

static double rms(fftw_complex *data, int length) {
  double sum=0;

  for (int i=0; i<length; ++i) sum += data[i][0]*data[i][0]+data[i][1]*data[i][1];
  sum /= length;
  return sqrt(sum);
}

#define CIND1R(a,i) a[i][0]
#define CIND1I(a,i) a[i][1]
#define IND1(a, i) a[i]
#define DIM(a,i) length

    //   ____ _                  _     _
    //  / ___| | ___  __ _ _ __ / | __| | ___
    // | |   | |/ _ \/ _` | '_ \| |/ _` |/ __|
    // | |___| |  __/ (_| | | | | | (_| | (__
    //  \____|_|\___|\__,_|_| |_|_|\__,_|\___|
    // Does a 1d complex-valued clean
    static int clean_1d_c(fftw_complex *res, fftw_complex *ker,
            fftw_complex *mdl, int *area, double gain, int maxiter, double tol, int length,
            bool stop_if_div, bool verb, bool pos_def) {
        double maxr=0, maxi=0, valr, vali, stepr, stepi, qr=0, qi=0;
        double score=-1, nscore, best_score=-1;
        double mmax, mval, mq=0;
        double firstscore=-1;
        int argmax=0, nargmax=0, dim=DIM(res,0), wrap_n;
        double *best_mdl=NULL, *best_res=NULL;
        if (!stop_if_div) {
            best_mdl = new double[2*dim];
            best_res = new double[2*dim];
        }
        // Compute gain/phase of kernel
        for (int n=0; n < dim; n++) {
            valr = CIND1R(ker,n); 
            vali = CIND1I(ker,n); //cout << valr << " " << vali << endl;
            mval = valr * valr + vali * vali;
            if (mval > mq && IND1(area,n)) {
                mq = mval; 
                qr = valr;
                qi = vali;
            }
        }
        qr /= mq;
        qi = -qi / mq;
        // The clean loop
        for (int i=0; i < maxiter; i++) {
	//cout << i << endl;
            nscore = 0;
            mmax = -1;
            stepr = (double) gain * (maxr * qr - maxi * qi);
            stepi = (double) gain * (maxr * qi + maxi * qr);
            CIND1R(mdl,argmax) += stepr;
            CIND1I(mdl,argmax) += stepi;    //cout << gain << " " << stepr << " " << stepi << " " << nscore << " " << dim << " cc\n";
            // Take next step and compute score
            for (int n=0; n < dim; n++) {
                wrap_n = (n + argmax) % dim;
                CIND1R(res,wrap_n) -= CIND1R(ker,n) * stepr - \
                                        CIND1I(ker,n) * stepi;
                CIND1I(res,wrap_n) -= CIND1R(ker,n) * stepi + \
                                        CIND1I(ker,n) * stepr;
                valr = CIND1R(res,wrap_n);
                vali = CIND1I(res,wrap_n);
                mval = valr * valr + vali * vali;
                nscore += mval; //cout << nscore << " " << dim << " cc\n";
                if (mval > mmax && IND1(area,wrap_n)) {
                    nargmax = wrap_n;
                    maxr = valr;
                    maxi = vali;
                    mmax = mval;
                }
            } 
            nscore = sqrt(nscore / dim);
            if (firstscore < 0) firstscore = nscore;
            if (verb != 0)
                printf("Iter %d: Max=(%d), Score = %f, Prev = %f\n", \
                    i, nargmax, (double) (nscore/firstscore), \
                    (double) (score/firstscore));
            if (score > 0 && nscore > score) {
                if (stop_if_div) {
                    // We've diverged: undo last step and give up
                    CIND1R(mdl,argmax) -= stepr;
                    CIND1I(mdl,argmax) -= stepi;
                    for (int n=0; n < dim; n++) {
                        wrap_n = (n + argmax) % dim;
                        CIND1R(res,wrap_n) += CIND1R(ker,n) * stepr - CIND1I(ker,n) * stepi;
                        CIND1I(res,wrap_n) += CIND1R(ker,n) * stepi + CIND1I(ker,n) * stepr;
                    }
                    return -i;
                } else if (best_score < 0 || score < best_score) {
                    // We've diverged: buf prev score in case it's global best
                    for (int n=0; n < dim; n++) {
                        wrap_n = (n + argmax) % dim;
                        best_mdl[2*n+0] = CIND1R(mdl,n);
                        best_mdl[2*n+1] = CIND1I(mdl,n);
                        best_res[2*wrap_n+0] = CIND1R(res,wrap_n) + CIND1R(ker,n) * stepr - CIND1I(ker,n) * stepi;
                        best_res[2*wrap_n+1] = CIND1I(res,wrap_n) + CIND1R(ker,n) * stepi + CIND1I(ker,n) * stepr;
                    }
                    best_mdl[2*argmax+0] -= stepr;
                    best_mdl[2*argmax+1] -= stepi;
                    best_score = score;   //cout <<"xx\n";
                    i = 0;  // Reset maxiter counter
                }
            } else if (score > 0 && (score - nscore) / firstscore < tol) {
                // We're done
                if (best_mdl != NULL) { delete[] best_mdl; delete[] best_res; }
                return i;
            } else if (not stop_if_div && (best_score < 0 || nscore < best_score)) { //cout <<best_score << " " << nscore << " yy\n";
                i = 0;  // Reset maxiter counter
            }
            score = nscore;   
            argmax = nargmax;
        }
        // If we end on maxiter, then make sure mdl/res reflect best score
        if (best_score > 0 && best_score < nscore) {
            for (int n=0; n < dim; n++) {
                CIND1R(mdl,n) = best_mdl[2*n+0];
                CIND1I(mdl,n) = best_mdl[2*n+1];
                CIND1R(res,n) = best_res[2*n+0];
                CIND1I(res,n) = best_res[2*n+1];
            }
        }
        if (best_mdl != NULL) { delete[] best_mdl; delete[] best_res; }
        return maxiter;
    }



static struct Info clean(fftw_complex *im, fftw_complex *ker, double tol, int *area, bool stop_if_div, int maxiter, int length) {
    /* This standard Hoegbom clean deconvolution algorithm operates on the 
    assumption that the image is composed of point sources.  This makes it a 
    poor choice for images with distributed flux.  In each iteration, a point 
    is added to the model at the location of the maximum residual, with a 
    fraction (specified by 'gain') of the magnitude.  The convolution of that 
    point is removed from the residual, and the process repeats.  Termination 
    happens after 'maxiter' iterations, or when the clean loops starts 
    increasing the magnitude of the residual.  This implementation can handle 
    1 and 2 dimensional data that is real valued or complex.
    gain: The fraction of a residual used in each iteration.  If this is too
        low, clean takes unnecessarily long.  If it is too high, clean does
        a poor job of deconvolving. */

    fftw_complex *mdl, *res;
    double score;
    int iter;
    struct Info info;

    //if mdl is None:
    //    mdl = n.zeros(im.shape, dtype=im.dtype)
    //    res = im.copy()

    mdl = new fftw_complex[length]; for (int i=0; i<length; ++i) mdl[i][0] = mdl[i][1] = 0;
    res = new fftw_complex[length]; for (int i=0; i<length; ++i) { res[i][0] = im[i][0]; res[i][1] = im[i][1]; }

    
    iter = clean_1d_c(res, ker, mdl, area, 0.1, maxiter, tol,  length, stop_if_div, false, false);
    score = rms(res, length);
    info.success = iter > 0 && iter < maxiter;
    info.tol = tol;
    if ( iter < 0 ) {
      info.term = "divergence";
      info.iter = -iter;
    } else if ( iter < maxiter ) {
      info.term = "tol";
      info.iter = iter;
    } else {
      info.term = "maxiter";
      info.iter = iter;
    }
    info.res = res;
    info.score = score;
  
    info._d_cl = mdl;

    return info;
}


static void multiply_in(fftw_complex *a1, double *a2, int length) {
  for (int i=0; i<length; ++i) {
    a1[i][0] *= a2[i];
    a1[i][1] *= a2[i];
  }
}

static void multiply_in(fftw_complex *a1, fftw_complex *a2, int length) {
  fftw_complex res;

  for (int i=0; i<length; ++i) {
    res[0] = a1[i][0]*a2[i][0]-a1[i][1]*a2[i][1];
    res[1] = a1[i][0]*a2[i][1]+a1[i][1]*a2[i][0];
    a1[i][0] = res[0]; a1[i][1] = res[1];
  }
}

static void add_in(fftw_complex *a1, fftw_complex *a2, int length) {
  for (int i=0; i<length; ++i) {
    a1[i][0] += a2[i][0];
    a1[i][1] += a2[i][1];
  }
}

static void subtract_in(fftw_complex *a1, fftw_complex *a2, int length) {
  for (int i=0; i<length; ++i) {
    a1[i][0] -= a2[i][0];
    a1[i][1] -= a2[i][1];
  }
}


// This is the entry point
fftw_complex *pspec_prep(fftw_complex *d, double *window, double tol, int *area, int length, bool opts_model) {
  // tol is the value specified by --clean
  // window is the FFT window, like hamming
  // area is a mask that indicate where to do the convolving
  // it has the same length as the data array. Then beginning
  // has a few 1's, and the end has a few 1's, with the middle being 0.
  // there could be some kind of wrap around operating here or it
  // might be in FFT ordering. Looks like thhe latter. DC FFT component
  // is at 0.

  fftw_complex *_d, *_w, *w, *res, *d_mdl, *d_copy, *w_copy, *result;
  struct Info info;

  d_copy = new fftw_complex[length]; 
  for (int i=0; i<length; ++i) { 
    d_copy[i][0] = d[i][0];
    d_copy[i][1] = d[i][1];
  }
 
 
  w = new fftw_complex[length];		// w must be zero'd where there are flagged out (0) values, 1 otherwise
  for (int i=0; i<length; ++i)
    if ( d[i][0] == 0 and d[i][1] == 0 ) w[i][0] = w[i][1] = 0;
    else { w[i][0] = 1; w[i][1] = 0; }

  w_copy = new fftw_complex[length]; 
  for (int i=0; i<length; ++i) { 
    w_copy[i][0] = w[i][0];
    w_copy[i][1] = w[i][1];
  }

  // Skip these because we always set these options.
  // if not opts.nophs: d = aa.phs2src(d, zen, i, j)
  // if not opts.nogain: d /= aa.passband(i,j)

   multiply_in(d, window, length);
   multiply_in(w, window, length);
 
   _d = new fftw_complex[length];
   _w = new fftw_complex[length];

   fft(d, _d, length, FFTW_BACKWARD);
   fft(w, _w, length, FFTW_BACKWARD);

   info = clean(_d, _w, tol, area, false, 100, length); 
   delete[] _d; delete[] _w; 
   
   d_mdl = new fftw_complex[length];
   if ( opts_model ) {  // This is the one we use
     scale(info.res, length);   // ?? why - but it has to be
     add_in(info._d_cl, info.res, length);     
     fft(info._d_cl, d_mdl, length, FFTW_FORWARD); 
     result = d_mdl;
     delete[] d_copy;   // Haven't used it
   } else {     
     fft(info._d_cl, d_mdl, length, FFTW_FORWARD);
     multiply_in(d_mdl, w_copy, length);   
     subtract_in(d_copy, d_mdl, length); 
     delete[] d_mdl; 
     result = d_copy;
   }

  delete[] info._d_cl; delete[] info.res; delete[] w; delete[] w_copy; 
  
  // Need to delete this result after it's used
  return result;
}

void gen_skypass_delay(double max_bl, double sdf, int nchan, int& uthresh, int& lthresh, double max_bl_add=0.) {
    // max_bl_add should NOT be used, because we add pixels onto the horizon by changing the 1-D clean area.
    // max_bl is the baseline length in nanosec
    // sdf is the channel width in GHz
   double bin_dly = 1. / (sdf * nchan);
   max_bl += max_bl_add;
   uthresh = (int)ceil(max_bl/bin_dly + 1.5);
   lthresh = (int)floor(-max_bl/bin_dly - 0.5);
}

#ifdef TEST
#include <fstream>
void print_c(fftw_complex c) {
  cout << c[0] << " " << c[1] << endl;
}

#include <iomanip>

int main(int argc, char *argv[]) {
  fftw_complex *x, *y, *result;
  double *window;
  int *area;

  cout << std::setprecision(10);
  
  x = new fftw_complex[436]; window = new double[436]; area = new int[436];
  window = new double[436];
 
  double re, im;
  FILE *f = fopen("signal.dat", "r");
  int i = 0;
  while ( i < 436 ) { 
      fscanf(f, "%lf %lf\n", &re, &im);
      x[i][0] = re; x[i][1] = im; 
      ++i;
  }
  fclose(f); 
  ifstream win("window.dat");
  i = 0;
  while ( win >> re ) { 
      window[i] = re; 
      ++i;
  }
  win.close();
  
  for (int i=0; i<436; ++i) area[i] = 0;
  area[0] = area[1] = area[2] = 1; area[434] = area[435] = 1;
  result = pspec_prep(x, window, 1e-8, area, 436, true); 
  
  print_caa(result, 436);exit(0);
  
  //for (int i=0; i<436; ++i) print_c(result[i]);
  delete[] result;
}
#endif
