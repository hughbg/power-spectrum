import numpy as np
from conversions import f2z, X2Y			# Get conversions from https://drive.google.com/file/d/101EAnhwCIBQra6viTVontQKBSQ1xu--S/view

# Constants

NUM_TIME_STEPS = 2
if NUM_TIME_STEPS < 2: raise RuntimeError("NUM_TIME_STEPS must be 2 or more")
if NUM_TIME_STEPS % 2 == 1: raise RuntimeError("NUM_TIME_STEPS must be an even number")
NUM_CHANNELS = 436
CHAN_WIDTH = 24000
MEAN_FREQUENCY = 48300000.0
kB = 1.38064852e-23				# Boltzmann
NOISE_RMS_REAL = 1416.1/np.sqrt(2)		# Polarization XX values for the noise
NOISE_RMS_IMAG = 1415.9/np.sqrt(2)

# Routines

def get_random_baseline_data():
  """
  Generate random visibilities for a baseline (over all frequency channels), 
  and do the FFT (delay_transform)
  """

  # Form the array of random visibilities

  real_vals = np.random.normal(loc=0.0, scale=NOISE_RMS_REAL, size=NUM_CHANNELS)
  imag_vals = np.random.normal(loc=0.0, scale=NOISE_RMS_IMAG, size=NUM_CHANNELS)

  visibilities = [ complex(real_vals[i], imag_vals[i]) for i in range(NUM_CHANNELS) ]


  # Delay transform the visibilities - FFT

  delay_transformed = np.fft.fftshift(np.fft.fft(visibilities))		# No window over the FFT


  # Normalize FFT values - see Appendix A.1.2 of the paper on the overleaf. The fft 
  # calculates area, but knows nothing about the physical width of the array elements,
  # and uses a width of 1. We have to fix that by incorporating the true width.

  delay_transformed *= CHAN_WIDTH

  return delay_transformed


# __MAIN__

# Get NUM_TIME_STEPS instances of the baseline data and multiply
# adjacent pairs and then add the results (section 2.3 in paper).

many_baseline_instances = [ get_random_baseline_data() for i in range(NUM_TIME_STEPS) ] 


for i in range(0, NUM_TIME_STEPS, 2):
  many_baseline_instances[i] *= np.conj(many_baseline_instances[i+1])	# Multiply adjacent time steps

V_2 = np.zeros(NUM_CHANNELS, dtype=np.complex64)
for i in range(0, NUM_TIME_STEPS, 2):
  V_2 += many_baseline_instances[i]					# Accumulate/add - every second one 
V_2 /= NUM_TIME_STEPS/2							# This is an average.



# We have the integrated V_2 for the baseline, integrated over lots of time steps.
# Now have to calculate power.



# X2Y is a function in the conversions but needs a redshift. 
# The redshift is going to be calculated from the mean frequency.
z = f2z(MEAN_FREQUENCY/1e9)         # f2z expects GHz

# B is the bandwidth
B = CHAN_WIDTH*NUM_CHANNELS

# lambda is based on the mean frequency
c = 299792458				
lambda_ = c/MEAN_FREQUENCY

# omega is the beam area which can be approximated as 1 for now
omega = 1

# Now we can calculate power P.
P = V_2 * (lambda_**2/(2*kB))**2 * X2Y(z)/(omega*B)

P *= 1e-61		# Get rid of power of 10 factors in the units. See the Overleaf Appendix A.2
P *= 1e6		# Convert K^2 to mk^2

print np.abs(np.mean(P))		# Result: the average power for the baseline
	
# I have done NUM_TIME_STEPS = 2 and 1000000
# For 2:       5.815335e+14
# For 1000000: 7.886423e+11
# It varies.


