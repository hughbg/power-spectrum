/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <unistd.h>
#include <cstring>
#include <fenv.h>
#include <iostream>
#include <vector>
#include <thread>
#include "power_spectrum.h"
#include "flags.h"
#include "vis_chain.h"

extern string strip_file_name(char*);
extern void check_not_null(void *, const char *);
#define MAX_DELAY 1500
#define MAX_DELAY_BIG 5100

void usage() {
  cerr << "noise_estimate [ options ] uvfits_file ...\n";
  cerr << "Options:\n";
  cerr << "	-1:		use only the first time step in the first UV fits file (default: false)\n";
  cerr << "	-p pol:		which polarization to use (default: 0). 0:XX, 1:YY, 2:XY, 3:YX\n";
  cerr << "     -c 		ignore channel flags (default: false)\n";
  cerr << "     -t num          number of threads to use (default: 1)\n";
}

class ThreadData {
public:
	char *fname;
	Flags* flags; 
	int which_pol;
	int file_index;
	bool one_only;
	bool ignore_channel_flags;
	thread *pthread;
	
	ThreadData(char *_fname, Flags* _flags, int _which_pol, int _file_index, bool _one_only, bool _ignore_channel_flags) {
	  fname = _fname;
          flags = _flags;
          which_pol = _which_pol;
          file_index = _file_index;
          one_only = _one_only;
          ignore_channel_flags = _ignore_channel_flags;
        }

};

void get_noise_from_file(char *fname, Flags* flags, int which_pol, int file_index, bool one_only, bool ignore_channel_flags) {
  Visibilities **all;
  vector<Visibilities*> vis;
  int num_times, num_channels;

  load_uvdata(fname, all, which_pol,file_index, 1, num_times, num_channels, false, one_only, ignore_channel_flags);

  for (int j=0; j<num_times; ++j) {
    cout << "Flag status " << strip_file_name(fname) << " step " << j << " " << (flags->in(flags->time_step_flags, strip_file_name(fname), j)==0?"not flagged":"flagged") << endl;
    if ( flags->in(flags->time_step_flags, strip_file_name(fname), j) ) {
      delete all[j];
    } else 
      vis.push_back(all[j]);

    if ( one_only ) break;
  }
  delete[] all;

  VisChain vc(436, 32640);
  for (int i=0; i<vis.size(); ++i) {
    vis[i]->uncache();

    for (int ch=0; ch<vis[i]->num_channels && ch+1 < vis[i]->num_channels; ch+=2) for (int bl=0; bl<vis[i]->num_baselines; ++bl)
      if ( vis[i]->visibilities[ch][bl].in_use && vis[i]->visibilities[ch+1][bl].in_use ) {
			//if ( abs(all[j]->visibilities[ch][bl].value-all[j]->visibilities[ch+1][bl].value) > 10000 )
			//	cout << j <<" " <<"Outlier " << ch << " " << "bl " << bl << " " << all[j]->visibilities[ch][bl].st1 << " "<<all[j]->visibilities[ch][bl].st2 << " " << abs(all[j]->visibilities[ch][bl].value-all[j]->visibilities[ch+1][bl].value)<<endl;

        vc.add(vis[i]->visibilities[ch][bl].value-vis[i]->visibilities[ch+1][bl].value, (float)vis[i]->visibilities[ch][bl].length, ch, bl); 
      }

    delete vis[i];
    if ( one_only ) break;
  }

  vc.dump(fname);
       
}

void thread_run(ThreadData *data) {
  get_noise_from_file(data->fname, data->flags, data->which_pol, data->file_index, data->one_only, data->ignore_channel_flags);
}

int main(int argc, char *argv[]) {
  vector<PowerSpectrum*> all_spectra;
  PowerSpectrum *result_ps;
  string fname;
  vector<ThreadData*> threads;
  Flags flags;
  int c, fft_size, which_pol, num_channels, scrunch_num, num_threads;
  const char *window="blackmanharris", *fft_size_s="8192", *which_pol_s="0", *scrunch_s="1", *num_threads_s="1";
  bool one_only=false, positive_negative_separate=false, interpolate =false, save_every=false;
  bool fft_specified=false, ignore_channel_flags=false, apply_weights=false;

  if ( argc == 1 ) {
    usage();
    return 0;
  }
  feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);  // Enable all floating point exceptions but FE_INEXACT

  while ((c = getopt (argc, argv, "t:r:p:f:w:sid1kcW")) != -1)
    switch (c) {
      case '1':
        one_only = true;
        break;
      case 'p':
        which_pol_s = optarg;
        break;
      case 't':
        num_threads_s = optarg;
        break;
      case 'c':
        ignore_channel_flags = true;
        break;
      case '?':
        if (optopt == 'w')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
        return 1;
      default:
        return 1;
      }


  if ( !( strcmp(which_pol_s, "0") == 0 || strcmp(which_pol_s, "1") == 0 || strcmp(which_pol_s, "2") == 0 || strcmp(which_pol_s, "3") == 0 ) ) {
    cerr << "Invalid pol. Expect 0 to 3\n";
    return 1;
  }
  which_pol = atoi(which_pol_s);


  for (int i=0; i<strlen(num_threads_s); ++i)
    if ( !isdigit(num_threads_s[i]) ) {
      cerr << "Invalid number of threads " << num_threads_s << endl;
      return 1;
    }
  num_threads = atoi(num_threads_s);


  flags.load_time_flags();

  // Read in all the calibrated data files and convert each to power spectra. The files
  // should have the same bandwidth and number of channels and the same uvlengths.
  // However they represent different times. 

  for (int i=optind; i<argc; ++i) {
    ThreadData *tdata = new ThreadData(argv[i], &flags, which_pol, i-optind, one_only, ignore_channel_flags); 
    if ( tdata == NULL ) {
      cerr << "Failed to allocate tdata\n";
      return 1;
    }

    cout << "Starting thread " << argv[i] << endl;
    tdata->pthread = new thread(thread_run, tdata); 
    if ( tdata->pthread == NULL ) {
      cerr << "Failed to allocate pthread\n";
      return 1;
    }	

    threads.push_back(tdata);

    if ( threads.size() == num_threads || i == argc-1 || one_only ) {		// Wait for existing threads 
      cout << threads.size() << " " << i << " " << argc << " waiting\n";
      for (int it=0; it<threads.size(); ++it) {
        threads[it]->pthread->join();

        delete threads[it]->pthread;
        delete threads[it];
      }
      threads.clear();

  
    }

    if ( one_only ) break;
  }


  return 0;

}
