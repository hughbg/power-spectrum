/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/


/*
Calculate cosmological distances. These are based on the conversions.py file from HERA
software. The original Python script that I had from HERA  used equations from Furlanetto, 
which are approximations, and I replaced them with cosmological distances from Hogg. 
I see that HERA have done the same.
*/


#include <cmath>
#include "conversions.h"

// C version of https://github.com/telegraphic/21cmSense/blob/master/conversion_utils.py
// with k_par() added.
// With the true Hogg calculations added.

#define SQR(x) ((x)*(x))
#define CUBE(x) ((x)*(x)*(x))

const double aipy_const_arcmin = 0.0002908882086657216;
const double f21cm = 1.42040575177e9;

	// Cosmological parameters
const double Omega_M = 0.31;
const double Omega_k = 0;
const double Omega_Lambda = 0.69;

const int c = 299792458;	// Units m/s
const double h = 0.68;
	
const int workspace_length = 5000;


// GSL integration routines ----------------------------------

/*
These rountines use the GSL integration routine to integrate the equation
for cosmological distances described by Hogg.
*/

static double func(double z, void *params) {

  return 1/sqrt(Omega_M*CUBE(1+z)+Omega_k*SQR(1+z)+Omega_Lambda);
}

// Between two redshifts
static double gsl_integrate2_E(gsl_integration_workspace *w, double z1, double z2) {
  double result, error;

  gsl_function F;
  F.function = &func;
  F.params = NULL;

  gsl_integration_qag(&F, z1, z2, 0, 1e-7, GSL_INTEG_GAUSS61, workspace_length, w, &result, &error); 

  //printf ("result          = % .18f\n", result);
  //printf ("estimated error = % .18f\n", error);
  //printf ("intervals       = %zu\n", w->size);
  return result;

}

// Distance to redshift from earth
static double gsl_integrate_E(gsl_integration_workspace *w, double z) {
  double result, error;

  gsl_function F;
  F.function = &func;
  F.params = NULL;

  gsl_integration_qag(&F, 0, z, 0, 1e-7, GSL_INTEG_GAUSS61, workspace_length, w, &result, &error); 

  //printf ("result          = % .18f\n", result);
  //printf ("estimated error = % .18f\n", error);
  //printf ("intervals       = %zu\n", w->size);
  return result;

}

// End GSL ---------------------------------------------------


// ------------------- Furlanetto Eqns, still using GHz frequencies, code from HERA converted to C++

// No h in these equations which there is in Furlanetto

//Convert frequency (GHz) to redshift for 21cm line.
double Conversions::f2z(double fq /*GHz*/) {
    double F21 = GHz(f21cm); 
    return (F21 / fq - 1);
}

//Convert frequency (GHz) to redshift for 21cm line.
double Conversions::z2f(double z) {
    double F21 = GHz(f21cm);
    return (F21 / (z + 1));
}

//Multiply by this to convert an angle on the sky to a transverse distance in Mpc/h at redshift z
double Conversions::dL_dth(double z) {
    // """[h^-1 Mpc]/radian, from Furlanetto et al. (2006)"""
    return 1.9 * (1./aipy_const_arcmin) * pow((1+z) / 10., .2);
}

//Multiply by this to convert a bandwidth in GHz to a line of sight distance in Mpc/h at redshift z
double Conversions::dL_df(double z, double omega_m) {
    //"""[h^-1 Mpc]/GHz, from Furlanetto et al. (2006)"""		// Eqn 4
    return (1.7 / 0.1) * pow((1+z) / 10., .5) * pow(omega_m/0.15, -0.5) * 1e3;
}

//Multiply by this to convert a baseline length in wavelengths (at the frequency corresponding to redshift z) into a tranverse k mode in h/Mpc at redshift z
double Conversions::dk_du(double z) {
    // """2pi * [h Mpc^-1] / [wavelengths], valid for u >> 1."""
    return 2*M_PI / dL_dth(z); // from du = 1/dth, which derives from du = d(sin(th)) using the small-angle approx
}

//Multiply by this to convert eta (FT of freq.; in 1/GHz) to line of sight k mode in h/Mpc at redshift z
double Conversions::dk_deta(double z) {
    // """2pi * [h Mpc^-1] / [GHz^-1]"""
    return 2*M_PI / dL_df(z);		// k = 2 pi /wavelength
}

// scalar conversion between observing and cosmological coordinates
double Conversions::F_X2Y(double z) {
    // """[h^-3 Mpc^3] / [str * GHz]"""
    return pow(dL_dth(z), 2) * dL_df(z);
}

double Conversions::F_X(double z) {
  return dL_dth(z);
}

double Conversions::F_Y(double z) {
  return dL_df(z);
}

// ------------------------- NEW conversions to make C++ class and use Hogg


Conversions::Conversions() { 
#ifdef COHERENT
	double H0 = 100*h*1000;      // Convert units to meters, hence the 1000 factor
#else
        double H0 = 100*h*1000;
#endif
	D_H = c/H0;           // Units Mpc/h
	w = NULL;
}
Conversions::~Conversions() { if ( w != NULL ) gsl_integration_workspace_free(w); }


// Calculated from Hogg
double Conversions::absolute_distance(double z) {
	if ( w == NULL ) w = gsl_integration_workspace_alloc(workspace_length);
	return D_H*gsl_integrate_E(w, z);
}

// Transverse distance of an angle. This is NOT simply 1/q, because strange
// things happen when u ~ 1. i.e. when there s only 1 fringe covering the
// whole sky. 1/q is suitable for when u >> 1.
double Conversions::uv2distance(double uv, double z) {
	// Angle between minima
	double q_m = 2*asin(1/(2*uv));
	double distance = absolute_distance(z);
	return q_m*distance;
}

// Line-of-sight distance corresponding to channel width
double Conversions::channel2distance(double f /*GHz*/, double chan_width /*GHz*/) {

	double z = f2z(f-chan_width/2);
	double new_z = f2z(f+chan_width/2);
	if ( w == NULL ) w = gsl_integration_workspace_alloc(workspace_length);
	return D_H*gsl_integrate2_E(w, new_z, z);
}

// k mode is just 2 pi/length of FFT
double Conversions::k_perp(double uv, double z) {
  return 2*M_PI/uv2distance(uv, z);
}

// uv_length from k_perp
double Conversions::uv_length(double k_perp, double z) {
  double uv_distance = 2*M_PI/k_perp;
  double q_m = uv_distance/absolute_distance(z);
  double uv = 1/sin(q_m/2)/2;
  return uv;
}

// We allow that X is about constant
double Conversions::X(double f /*GHz*/) {
  return absolute_distance(f2z(f));
}

// We don't allow that Y is actually a constant for us. It needs to be calculated becaue it is in fact a derivative.
double Conversions::Y(double f/*GHz*/, double chan_width/*GHz*/) {
  double bottom_dist = channel2distance(f, chan_width/2);
  double top_dist = channel2distance(f, 3*chan_width/2); 
  double slope = (top_dist-bottom_dist)/chan_width;
  return slope;
}

double Conversions::X2Y(double f/*GHz*/, double chan_width/*GHz*/) {
  double _X = X(f);
  return _X*_X*Y(f, chan_width);
}

// Integrate along a frequency range where the conversion from delta-nu to k_par is changing due to redshift. Divide the frequency
// range into channels and calculate the conversion for each of them and sum them. This like dk_deta(), but calls dL_df() many
// times on small frequency separations.
// f1: starting frequency. num: num_channels. chan_width: channel width, 24000Hz.

double Conversions::k_par(double f1 /*GHz*/, int num, double chan_width /*GHz*/) {
    double dist, f;

    dist = 0.0;
    f = f1;
    for (int i=0; i<num; ++i) {
      dist += channel2distance(f, chan_width);
      f += chan_width;
    }
    return 2*M_PI/dist;
}


// ------------------------------- MAIN (testing)
static Conversions conv;

#include <iostream>
#include <iomanip>
using namespace std;

// These routines convert an angle on the sky to distance in Mpc
double Integral_transverse_distance(double angle, double z) {
 
  double D_C = conv.absolute_distance(z);    // E is dimensionless so units Mpc/h
  double D_M = D_C;       		// for flat universe Omega_k = 0
  return D_M*angle;
}

double Furlanetto_transverse_distance(double angle, double z) {
  return angle*conv.dL_dth(z)/h;
}

// Convert the chan_width at a frequency of f at redshift z into a los distance
double Integral_los_distance(double z, double new_z) {
 
  // Top frequency 
  double D_C_top = conv.absolute_distance(z);	// E is dimensionless so units Mpc/h

  // Bottom frequency 
  double D_C_bottom = conv.absolute_distance(new_z); 

  return D_C_bottom-D_C_top;
}

double Furlanetto_los_distance(double chan_width /*Hz*/, double z) {

  return GHz(chan_width)*conv.dL_df(z, Omega_M)/h;   
}

void transverse_distance() {    // over 24kHz for different redshift
  const double angle = 0.1;
  double z = 5.0;
  int i;
 
  for (i=0; i<400; ++i) {
    cout << z << " " << Furlanetto_transverse_distance(aipy_const_arcmin, z) << " " << Integral_transverse_distance(aipy_const_arcmin, z) << endl;
    z += 0.1;
  }
}

void los_distance() {	// over 24kHz for different redshift
  double start_f = 100e6;	// All in Hz
  double end_f = 30e6;
  double chan_width = 24e3;
  double f = start_f;
 
  while ( f >= end_f ) {
    double z = conv.f2z(GHz(f)); 
    double new_z = z+f21cm/(f-chan_width)-f21cm/f;
    cout << f << " " << z << " " <<  Furlanetto_los_distance(chan_width, z) << " " << Integral_los_distance(z, new_z) << endl;
    f -= chan_width*10;
  }

}

int main(int argc, char *argv[]) {
  if ( argc != 3 ) {
    cerr << "Usage: X2Y_shift from_frequency to_frequency (frequencies in Hz)\n";
    return 1;
  }
	
  // Test:
  //cout << "The 2 numbers on each line should be about the same\n";
  //double f = 50e6;
  //cout << conv.X(GHz(f)) << " " << conv.F_X(conv.f2z(GHz(f)))/h << endl;
  //cout << conv.Y(GHz(f), GHz(24e3)) << " " << conv.F_Y(conv.f2z(GHz(f)))/h << endl;
  double f_in = atof(argv[1]);	// These must be in Hz
  double f_out = atof(argv[2]);
  
  cout << conv.X2Y(GHz(f_out), GHz(24e3))/conv.X2Y(GHz(f_in), GHz(24e3)) << endl;
  cout << conv.F_X2Y(conv.f2z(GHz(f_out)))/conv.F_X2Y(conv.f2z(GHz(f_in))) << endl;

  cout << conv.X2Y(GHz(f_in), GHz(24e3)) << " " << conv.X2Y(GHz(f_out), GHz(24e3)) << endl;
}
// Have to do X2Y and going the other way. Clean up k_par using chan_width
