/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/



#include <iostream>
#include <sstream>
#include <fstream>
#include <cstdlib>
#include "times.h"

vector<string> split(string line) {
   vector<string> tokens;
   string token;

   for (int i=0; i<line.size(); ++i) if ( line[i] == '\t' ) line[i] = ' ';
   stringstream l(line);

   while ( getline(l, token, ' ') )
     if ( token.size() > 0 ) tokens.push_back(token);

   return tokens;
}

void FileTimes::load(const char *fname) {
	ifstream file(fname);
	string line;
	vector<string> words;
        FileTime ftime;

	if ( file.is_open() ) {
	    while( getline(file, line) ) 
	      if ( line.size() > 0 && line[0] != '#' ) {
	    	  words = split(line);
	    	  ftime.fname = words[0];
	    	  ftime.time = atof(words[1].c_str());
	    	  file_times.push_back(ftime);
	    	  
	    	  //cout << baseline.st1 << " " << baseline.st2 <<endl;
	      }
              if ( file_times.size() == 0 ) cout << "No times in " << fname << endl;

	} else cout << "No times in " << fname << endl;


}

double FileTimes::get_time(string fname) {
  for (int i=0; i<file_times.size(); ++i) 
    if ( file_times[i].fname == fname ) return file_times[i].time;
  return -1;
}

#ifdef MAIN
extern string strip_file_name(char *name);
int main(int argc, char *argv[]) {
  FileTimes ft;

  ft.load("../../collection/split/uvfits_files_times.txt");
  
  cout << ft.get_time(strip_file_name("../main/2018-05-07-22_06_09_0016744009621312_2.uvfits"));
  return 0;
}
#endif
