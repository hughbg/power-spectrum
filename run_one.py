import os, sys
import subprocess


def check_file(fname):
  if not os.path.exists(fname):
    print "Missing", fname
    exit(1)

check_file(sys.argv[1])
check_file("cuwarp.in")
check_file("antenna_locations.txt")


subprocess.call([ "python",  "/home/leda/hgarsden/make_header.py", sys.argv[1] ])
ra = dec = None
for line in open("header.txt"):
  ls = line.split()
  if len(ls) >= 2:
    if ls[0] == "RA_HRS": ra = ls[1]
    if ls[0] == "DEC_DEGS": dec = ls[1]
    if ls[0] == "N_CHANS": nchan = ls[1]

outf = open("x.in", "w")
for line in open("cuwarp.in"):
  if not ( line[:len("BaseFilename")] == "BaseFilename" or line[:len("ObservationImageCentreRA")] == "ObservationImageCentreRA" or line[:len("ObservationImageCentreDec")] == "ObservationImageCentreDec" 
		or line[:len("FscrunchChan")] == "FscrunchChan" or line[:len("NumberOfChannels")] == "NumberOfChannels" ):
    outf.write(line)

outf.write("BaseFilename="+sys.argv[1]+"\n")
outf.write("ObservationImageCentreRA="+ra+"\n")
outf.write("ObservationImageCentreDec="+dec+"\n")
#outf.write("FscrunchChan="+nchan+"\n")
outf.write("FscrunchChan=1\n")
outf.write("NumberOfChannels="+nchan+"\n")
outf.close()



"""
print "Delay fitting"
print "Creating flagged_stands.dat from flagged_tiles.txt"
subprocess.call([ "cp", "flagged_tiles.txt", "flagged_stands.dat" ])  
print "Creating flagged_baselines.dat from flagged_baselines.txt"
subprocess.call([ "cp", "flagged_baselines.txt", "flagged_baselines.dat" ])
if os.path.exists("flagged_channels.txt"):
  print "flagged_channels.txt exists, but not implemented in delay fitting"
check_file("physical.dat")
check_file("stands.py")
check_file("flags.py")
check_file("lwa_ovro.telescope.json")

subprocess.call([ "/home/leda/anaconda2/bin/python", "delay_fit.py", sys.argv[1], "0" ])
os.rename("instr_config_fit.txt", "instr_config.txt")
"""

if os.path.exists("cal_ratios.dat"): os.remove("cal_ratios.dat")
print "Run cuwarp"
subprocess.call([ "./rts_node_cpu",  "x.in" ])
print "Analyze"

p2 = subprocess.Popen(["ls", "-t"], stdout=subprocess.PIPE)
last_file = p2.communicate()[0].split("\n")[0]
print "Log",last_file
subprocess.call([ "grep", "FLUX", last_file ])
if os.path.exists("BandpassCalibration_node001.dat"):
  subprocess.call([ "python", "bandpass_stats.py", "BandpassCalibration_node001.dat" ])
else: print "No bandpass file"
exit()
subprocess.call([ "python", "plot_source_cal.py" ])
subprocess.call([ "python", "plot_cal_convergence.py" ])



