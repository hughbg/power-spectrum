import sys
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter


def log_transform(im):
    '''returns log(image) scaled to the interval [0,1]'''
    try:
        (min, max) = (im[im > 0].min(), im.max())
        if (max > min) and (max > 0):
            return (np.log10(im.clip(min, max)) - np.log10(min)) / (np.log10(max) - np.log10(min))*np.log10(max)
    except:
        pass
    return im



fig = plt.figure()
ax = fig.gca(projection='3d')
data = np.loadtxt(sys.argv[1])
data = data.transpose() # ch is first index
X = np.arange(data.shape[1])
Y = np.arange(data.shape[0])
X, Y = np.meshgrid(X, Y)
surf = ax.plot_surface(X, Y, data, cmap=cm.coolwarm, linewidth=0, antialiased=False)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
plt.show()
exit()
"""
data = np.loadtxt(sys.argv[1])
data = data.transpose()	# ch is first index
plt.imshow(log_transform(data), aspect="auto")
plt.colorbar()
plt.show()
exit()

data = np.argmax(data, axis=0)
data = data[data!=0]
plt.plot(data)
plt.show()
"""
