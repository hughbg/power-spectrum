/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <cstdlib>
using namespace std;

struct Baseline {
	int st1, st2;
};

struct RFIFlag {
  int ch, iter;
};

struct TimeStepFlag {
  string fname;
  int time_step;
};

class Flags {
public:
  vector<int> stand_flags, channel_flags;
  vector<RFIFlag> rfi_channel_flags;
  vector<Baseline> baseline_flags;
  vector<TimeStepFlag> time_step_flags;

  void load_vis_flags(int file_index=0);
  void load_time_flags();
  vector<string> split(string line);
  void load_flags(const char *fname, vector<int>& flags);			// channels and tiles are simple
  void load_flags(const char *fname, vector<Baseline>& flags);		// baselines more complicated, each line has 2 stand numbers
  void load_flags(string, vector<RFIFlag>& flags);	// rfi flagged channels more complicated, they change from 1 dump to the next
  void load_flags(const char *, vector<TimeStepFlag>& flags);	// time steps are specified with file name (no ext) and dump within the file
  bool in(vector<int>& flags, int num);			// channel or stand
  bool in(vector<Baseline>& flags, int st1, int st2);
  bool in(vector<RFIFlag>& flags, int ch, int iter);
  bool in(vector<TimeStepFlag>& flags, string fname, int dump);
};

