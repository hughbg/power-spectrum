/*
 * This program integrates telescope noise over thousands of hours of observation.
 * Currently it implements delay-transformed noise but can be modified not do the delay-transform
 * (used for wedge power spectrum)
 * Given a telescope with N baselines and M frequency channels, NxM visibilities (here noise) are captured
 * in a single observation. For observing over thousands of hours, there will be N*M*K visibilities,
 * where K is the number of observations in those hours. For any real telescope observing for hundreds or
 * thousands of hours, N*M*K gets up ito the trillions. To generate and integrate all those
 * values we use GPUs. Noise values are random but must be generated from an appropriate distribution,
 * requiring the use of a rand() library.
 * This program does coherent and incoherent integration. More information is in the paper.
 */
#include <stdio.h>
#include <stdlib.h>

#include <cuda.h>
#include <curand_kernel.h>
#include <cuComplex.h>

#define CUDA_CALL(x) do { if((x) != cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__); \
    return EXIT_FAILURE;}} while(0)

__global__ void generate_normal_kernel_trace(int n, double stdev, double *real, double *imag) {
    int id = 1;
    double2 x;
    curandStateMRG32k3a state;

    curand_init(0, id, 0, &state);

    /* Generate pseudo-random normals */
    for(int i = 0; i < n; i++) {
        x = curand_normal2_double(&state);
        real[i] = x.x*stdev;
        imag[i] = x.y*stdev;
    }

}


__global__ void generate_normal_kernel(int n, int coherent_n, double stdev_real, double stdev_imag, cuDoubleComplex *result) {
    int id = threadIdx.x + blockIdx.x * blockDim.x;
    int i, j, num_pairs_added=0;
    double2 rands;
    curandStateMRG32k3a state;
    cuDoubleComplex c_result = make_cuDoubleComplex(0, 0);
    curand_init(0, id, 0, &state);

    /* Generate pseudo-random normals */
    i = 0;
    while ( i < n ) {	// The sample_count must be set so there is the right number of sets. Done in main().
      cuDoubleComplex c1 = make_cuDoubleComplex(0, 0);
      for (j=0; j<coherent_n; ++j) {
        rands = curand_normal2_double(&state);
        cuDoubleComplex c = make_cuDoubleComplex(rands.x*stdev_real, rands.y*stdev_imag); 
        c1 = cuCadd(c1, c);
        ++i;
      }
      c1 = make_cuDoubleComplex(cuCreal(c1)/coherent_n, cuCimag(c1)/coherent_n);
      cuDoubleComplex c2 = make_cuDoubleComplex(0, 0);
      for (j=0; j<coherent_n; ++j) {
        rands = curand_normal2_double(&state);
        cuDoubleComplex c = make_cuDoubleComplex(rands.x*stdev_real, rands.y*stdev_imag);  
        c2 = cuCadd(c2, c);
        ++i;
      }
      c2 = make_cuDoubleComplex(cuCreal(c2)/coherent_n, cuCimag(c2)/coherent_n);

      c_result = cuCadd(c_result, cuCmul(c1, c2)); ++num_pairs_added;
    }

    /* Store results */
    result[id] = make_cuDoubleComplex(cuCreal(c_result)/num_pairs_added, cuCimag(c_result)/num_pairs_added);
}

int trace() {
    int i;
    double *dev_real, *dev_imag, *host_real, *host_imag;
    int sampleCount = 10000;
    double stdev = 21.5;

    /* Allocate space for results on host */
    host_real = (double *)calloc(sampleCount, sizeof(double));
    host_imag = (double *)calloc(sampleCount, sizeof(double));

    /* Allocate space for results on device */
    CUDA_CALL(cudaMalloc((void **)&dev_real, sampleCount*sizeof(double)));
    CUDA_CALL(cudaMalloc((void **)&dev_imag, sampleCount*sizeof(double)));

    /* Generate and use normal pseudo-random  */
    generate_normal_kernel_trace<<<1, 1>>>(sampleCount, stdev, dev_real, dev_imag);

    /* Copy device memory to host */
    CUDA_CALL(cudaMemcpy(host_real, dev_real, sampleCount*sizeof(double), cudaMemcpyDeviceToHost));
    CUDA_CALL(cudaMemcpy(host_imag, dev_imag, sampleCount*sizeof(double), cudaMemcpyDeviceToHost));

    /* Show result */
    for(i = 0; i < sampleCount; i++) {
        printf("%lf %lf\n", host_real[i], host_imag[i]);
    }

    /* Cleanup */
    CUDA_CALL(cudaFree(dev_real));
    CUDA_CALL(cudaFree(dev_imag));

    free(host_real);
    free(host_imag);
    return EXIT_SUCCESS;
}


int main(int argc, char *argv[])
{

    cuDoubleComplex *devResults, *hostResults;
    int num_channels, num_baselines, sample_count, coherent_sample_count, chunks, extra;
    double stdev_real, stdev_imag;
    FILE *f;
    int i;

    if ( argc != 7 ) {
      fprintf(stderr, "noise program got wrong args\n");
      fprintf(stderr, "noise_integration <num_channels> <num_baselines> <stdev real> <stdev imag> <num samples in time> <num samples to coherent average>\n");
      return 1;
    }

    num_channels = atoi(argv[1]); num_baselines = atoi(argv[2]);
    stdev_real = atof(argv[3]); stdev_imag = atof(argv[4]);
    sample_count = atoi(argv[5]); coherent_sample_count = atoi(argv[6]);

    printf("noise generator got values %d %d %f %f %d %d\n", num_channels, num_baselines, stdev_real, stdev_imag, sample_count, coherent_sample_count);
    printf("The second last value is the total number of samples; when multiplied by 9s it should equal the total hours (in seconds)\n");
    printf("However because there must be enough samples to evenly cover the days, this value may be adjusted (see any later Warning)\n");
    printf("The last value is the number of days multiplied by the number of samples in a coherently averaged \"chunk\" within a day,\n");
    printf("these are all coherently added. Example: if coherent averaging in day can be done for only 3 samples (3 correlator dumps),\n");
    printf("and there are 100 days, then we coherently average 300 samples (it doesn't matter what order they are averaged in). This\n");
    printf("is one \"chunk\" running through 100 days\n");
    //return trace();

    // Calculate if we have enough samples for the coherent averaging and the multiplication of visibility pairs
    // with none left over
    if ( sample_count < 2*coherent_sample_count ) {
      fprintf(stderr, "Not enough samples to do coherent averaging then pair multiplication.\n");
      return EXIT_FAILURE;
    }
    chunks = sample_count/coherent_sample_count;      // remainder
    if ( chunks%2 == 1 ) chunks += 1;
    else if ( chunks*coherent_sample_count != sample_count ) chunks += 2;
    extra = chunks*coherent_sample_count-sample_count;
    if ( extra != 0 ) {
      printf("Warning: there are %d samples short. ", extra);
      sample_count += extra;
      printf("Using %d samples.\n", sample_count);
    }

    // Recheck
    if ( (sample_count/coherent_sample_count)%2 == 1 || sample_count%coherent_sample_count != 0 ) {
      fprintf(stderr, "Something went wrong with the calculation of chunks.\n");
      return EXIT_FAILURE;
    }

    printf("There will be %d chunks of %d samples coherently averaged, and then %d values incoherently averaged (after pair multiplication).\n", 
	sample_count/coherent_sample_count, coherent_sample_count, sample_count/(2*coherent_sample_count));
    printf("To find the number of snapshots in a chunk, divide %d by %d and the number of days\n", sample_count, sample_count/coherent_sample_count);
    puts("A chunk applies is the number of time-contiguous snapshots at a particular LST that can be coherently integrated");

    /* Allocate space for results on host */
    hostResults = (cuDoubleComplex *)calloc(num_baselines*num_channels, sizeof(cuDoubleComplex));

    /* Allocate space for results on device */
    CUDA_CALL(cudaMalloc((void **)&devResults, num_baselines*num_channels*sizeof(cuDoubleComplex)));

    /* Set results to 0 */
    CUDA_CALL(cudaMemset(devResults, 0, num_baselines*num_channels*sizeof(cuDoubleComplex)));

    /* Generate and use normal pseudo-random  */
    generate_normal_kernel<<<num_baselines, num_channels>>>(sample_count, coherent_sample_count, stdev_real, stdev_imag, devResults);

    /* Copy device memory to host */
    CUDA_CALL(cudaMemcpy(hostResults, devResults, num_baselines*num_channels*sizeof(cuDoubleComplex), cudaMemcpyDeviceToHost));

    /* Show result */
    f = fopen("noise.dat", "w");
    fprintf(f, "%d\n", sample_count/(2*coherent_sample_count));
    for (i=0; i<num_channels*num_baselines; ++i)
        fprintf(f, "%lf %lf\n", cuCreal(hostResults[i]), cuCimag(hostResults[i]));
    fclose(f);    

    /* Cleanup */
    CUDA_CALL(cudaFree(devResults));
    free(hostResults);

    return EXIT_SUCCESS;
}
