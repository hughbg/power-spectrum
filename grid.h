/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <complex>
using namespace std;

/*
  A 2-D array of complex numbers that handles translation between array indexes and real-world (physical) co-ordinates,
  and has clipping and other operations. Once setup, the array can be accessed using physical co-ordinates.
  Array co-ordinates can be fractional, they will only be converted to integer when necessary.
*/

class Grid {
public:
  int dim1, dim2;			// array dimensions
  double left, right, bottom, top;	// the extent of the grid in physical co-ordinates, which may be k_par/k_perp or frequency/uv length
  double left2, right2, bottom2, top2;
  complex<double> **grid;
  int **weights;
  double **row_tags;
  int num_row_tags;
  string cache_file;

  Grid() { grid = NULL; dim1 = dim2 = left = right = bottom = top = 0; left2 = right2 = bottom2 = top2 = 0; num_row_tags = 0; row_tags = NULL; weights = NULL; }
  Grid(int d1, int d2, double l, double r, double b, double t, int num_row_tags) { grid = NULL; row_tags = NULL; weights = NULL; setup(d1, d2, l, r, b, t, num_row_tags); }
  Grid(const Grid& g);
  void setup(int d1, int d2, double l, double r, double b, double t, int num_row_tags);		// left, right ... is in physical co-ords
  ~Grid() { clear(); }
  void clear();
  void check();
  void copy_grids(const Grid& g);					// arrays must already be allocated and the right size
  Grid& operator=(const Grid& g);
  void reset_weights(int weight);
  int sum_weights();
  void inc_at(double x, double y, complex<double> value);
  void inc_grid_at(double i, double j, complex<double> value);
  complex<double> value_at(double x, double y);
  int weight_at(double x, double y);
  complex<double> value_at_grid(double i, double j);
  int weight_at_grid(double i, double j);
  void set_at(double x, double y, complex<double> val);             // physical co-ords
  void set_grid_at(double i, double j, complex<double> val);		// grid co-ords - integers
  void physical2grid(double x, double y, double& i, double& j);
  void x_physical2grid(double x, double& j);
  void y_physical2grid(double y, double& j);
  void physical2grid_window2(double x, double y, double& i, double& j);
  void y_physical2grid_window2(double y, double& j);
  void grid2physical(double i, double j, double& x, double& y);   
  void grid2physical_window2(double i, double j, double& x, double& y);   
  void x_grid2physical(double i, double& x);
  void y_grid2physical(double j, double& y);   
  void y_grid2physical_window2(double j, double& y);
  void set_new_extent(double l, double r, double b, double t);
  void set_window2_extent(double l, double r, double b, double t);
  void set_row_tag(double x, int which, double val);
  void raise_row_tags(double *rt, int which);
  double get_row_tag(double x, int which);
  void cache(string fname = string(""));
  void uncache();
  bool all_zero();
  void clip_grid(int il, int ir, int jb, int jt);			// il ... is in grid co-ords
  void bbox(int& il, int& ir, int& jb, int& jt);
  void clip(double left, double right, double bottom, double top);
  void auto_clip();		// Clip out edges where there are zeros
  void apply_weights();
  bool same_size(const Grid& g);
  double min();
  double max();
  void gabs();
  void save_grid(const char *fname);
  void save_weights(const char *fname);
  void flip();
  void print_sizes();
  void print_row_tags();
  void print();
};

