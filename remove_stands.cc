/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Convert TRIANGULAR DADA format to UVfits.

  triang2uvfits64 /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/header.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/antenna_locations.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/instr_config.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/2015-04-08-20_15_03_0001133593833216.dada x.uvfits
*/

#include <stdio.h>
#include <unistd.h>
#include <cstring>
#include "uvfits.h"

// get uvfits.h and uvfits.c from cuwarp. Convert SLA calls to PAL. Change "station" to stand. Make writeUVFITS extern "C"
      

extern "C" {
  int writeUVFITS(char *filename, uvdata *data);
  int readUVFITS(char *filename, uvdata **data);
}

const int NUM_BASELINES64 = 31375; //2080;	no autocorrelations
const int NUM64_STANDS = 251; //64;
const int NUM256_STANDS = 256;


static void usage() {
  fprintf(stderr, "Usage: remove_stands <input uvfits> <output uvfits>\n");
  fprintf(stderr, "Currently removes stand 0\n");
  
}

static int encodeBaseline(int stand1_index, int stand2_index) {
  int i, tmp;

  if ( stand1_index < 0 || NUM64_STANDS <= stand1_index || stand2_index < 0 || NUM64_STANDS <= stand2_index ) {
    fprintf(stderr, "Invalid stand number to baseline encoding. S1: %d S2: %d\n", stand1_index, stand2_index);
    exit(1);
  }

  if ( stand1_index > stand2_index ) {
    tmp = stand1_index;
    stand1_index = stand2_index;
    stand2_index = tmp;
  }

  ++stand1_index; ++stand2_index;		// encoding uses 1-based indexing

  if (stand2_index > 255) {
    return stand1_index*2048 + stand2_index + 65536;
  } else {
    return stand1_index*256 + stand2_index;
  }

}

static void check(void *p) {
	if ( p == NULL ) {
		fprintf(stderr, "Failed to allocate memory\n");
		exit(1);
	}
}


static void insert_codes(float *codes) {
  int bl_index = 0;

  for (int st1=0; st1<NUM64_STANDS; ++st1) 
    for(int st2=st1+1; st2<NUM64_STANDS; ++st2) {

      codes[bl_index] = encodeBaseline(st1, st2); 
      ++bl_index;
    }
}

static int indexes256[NUM256_STANDS][NUM256_STANDS];

int main(int argc, char *argv[]) {
  uvdata *data;
  int i, bl_index256, bl_index64;

  if ( argc != 3 ) { usage(); return 1; }

  if ( access(argv[1],R_OK) == -1 ) {
    fprintf(stderr,"Failed to find %s\n",argv[1]);
    return 1;
  }
  
  
  readUVFITS(argv[1], &data);
  data->source->ra /= 15;
  
  // make baseline indexes for 256. The indexing is a bit different from usual because 
  // there are no auto correlations
  bl_index256 = 0;
  for (int st1=0; st1<NUM256_STANDS; ++st1) 
     for (int st2=st1+1; st2<NUM256_STANDS; ++st2) {
       indexes256[st1][st2] = bl_index256;
       ++bl_index256;
     }
  
  for (int i=0; i<data->n_vis; ++i) data->n_baselines[i] = NUM_BASELINES64;     
  
  for (int i=0; i<data->n_vis; ++i) {
	float *old_baseline = data->baseline[i];
    data->baseline[i] = (float*)malloc(NUM_BASELINES64*sizeof(float));
    check(data->baseline[i]);
    insert_codes(data->baseline[i]);   
    free(old_baseline);
  }
  


  // Scan the baselines as if there are 64 stands, and copy data
  for (int i=0; i<data->n_vis; ++i) {
	  float *visdata = (float*)malloc(NUM_BASELINES64*data->n_freq*data->n_pol*2*sizeof(float));    // factor 2 because complex
	  check(visdata);
	  float *weightdata = (float*)malloc(NUM_BASELINES64*data->n_freq*data->n_pol*sizeof(float));   
	  check(weightdata);
      double *u = (double*)malloc(NUM_BASELINES64*sizeof(double));
      check(u);
      double *v = (double*)malloc(NUM_BASELINES64*sizeof(double));
      check(v);
      double *w = (double*)malloc(NUM_BASELINES64*sizeof(double));
      check(w);
      
	  bl_index64 = 0;
  	  for (int st1=0; st1<NUM64_STANDS; ++st1) 
  		  for (int st2=st1+1; st2<NUM64_STANDS; ++st2) {
  			  bl_index256 = indexes256[st1][st2];
    	   
  			  for (int f=0; f<data->n_freq; ++f)
  				  for (int p=0; p<data->n_pol; ++p) {
  					  for (int c=0; c<2; ++c) {
  						  visdata[(bl_index64*(data->n_pol*data->n_freq) + f*data->n_pol + p)*2+c] = data->visdata[i][(bl_index256*(data->n_pol*data->n_freq) + f*data->n_pol + p)*2+c];
  					  }	
  					  weightdata[bl_index64*(data->n_pol*data->n_freq) + f*data->n_pol + p] = data->weightdata[i][bl_index256*(data->n_pol*data->n_freq) + f*data->n_pol + p];
 					  u[bl_index64] = data->u[i][bl_index256];
 					  v[bl_index64] = data->v[i][bl_index256];
 					  w[bl_index64] = data->w[i][bl_index256];
  				  }
     	      
    	 	  bl_index64++;
     	  }  

      free(data->visdata[i]); free(data->weightdata[i]); free(data->u[i]); free(data->v[i]); free(data->w[i]);
      data->visdata[i] = visdata; data->weightdata[i] = weightdata; data->u[i] = u; data->v[i] = v; data->w[i] = w;
  }
  
  
  // Change the array table
  ant_table *antennas = (ant_table*)malloc(NUM64_STANDS*sizeof(ant_table));
  for (int i=0; i<NUM64_STANDS; ++i) antennas[i] = data->array->antennas[i];
  data->array->n_ant = NUM64_STANDS;
  free(data->array->antennas); data->array->antennas = antennas;
  strcpy(data->array->name, "OVRO-LWA");
  writeUVFITS(argv[2], data);

}
