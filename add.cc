/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/

/*
Add the visibilities in two UVFits files. Not used. Was used for time integration.
*/

#include <iostream>
#include <cstring>
#include "uvfits.h"

using namespace std;

#define NUM_POLS 4

template<typename T>
void bad(T v1, T v2, const char *what) {
  if ( v1 != v2 ) {
    cout << what << " aren't all the  same. " << v1 << " " << v2 << endl;
    exit(1);
  }
}


int main(int argc, char *argv[]) {
  int test_bl, test_ch_in, test_ch_out;
  uvdata *data1, *data2;

  if ( argc != 4 ) {
    cerr << "Expecting 2 UVfits files in, 1 out\n";
    return 1;
  }

  readUVFITS(argv[1], &data1);
  readUVFITS(argv[2], &data2);

  // Test output for visual check
  test_bl = 2070; test_ch_in = 60;
  cout << "Test in 1\n";
  for (int j=0; j<5; ++j) {
    int index = (0+data1->n_pol*(test_ch_in+data1->n_freq*(test_bl+j)))*2;

    cout << j << endl;
    cout << data1->u[data1->n_vis-1][j+test_bl] << " " << data1->v[data1->n_vis-1][j+test_bl] << " " << data1->w[data1->n_vis-1][j+test_bl] << " " << data1->n_baselines[data1->n_vis-1] << " " << data1->baseline[data1->n_vis-1][j+test_bl] << endl;
    cout << data1->visdata[data1->n_vis-1][index] << " "  << data1->visdata[data1->n_vis-1][index+1] << " " << data1->weightdata[data1->n_vis-1][index] << endl;

  }
  test_bl = 2070; test_ch_in = 60;
  cout << "Test in 2\n";
  for (int j=0; j<5; ++j) {
    int index = (0+data2->n_pol*(test_ch_in+data2->n_freq*(test_bl+j)))*2;

    cout << j << endl;
    cout << data2->u[data2->n_vis-1][j+test_bl] << " " << data2->v[data2->n_vis-1][j+test_bl] << " " << data2->w[data2->n_vis-1][j+test_bl] << " " << data2->n_baselines[data2->n_vis-1] << " " << data2->baseline[data2->n_vis-1][j+test_bl] << endl;
    cout << data2->visdata[data2->n_vis-1][index] << " "  << data2->visdata[data2->n_vis-1][index+1] << " " << data2->weightdata[data2->n_vis-1][index] << endl;

  }

  bad(data2->n_vis, data1->n_vis, "n_vis");
  bad(data2->n_pol, data1->n_pol, "n_pol");
  bad(data2->pol_type, data1->pol_type, "pol_type");
  bad(data2->n_IF, data1->n_IF, "n_IF");
  bad(data2->n_vis, data1->n_vis, "n_vis");
  bad(data2->freq_delta, data1->freq_delta, "freq_delta");
  bad(data2->source->ra, data1->source->ra, "ra");
  bad(data2->source->dec, data1->source->dec, "dec");
  bad(data2->cent_freq, data1->cent_freq, "cent_freq");
  for (int j=0; j<data2->n_vis; ++j) {
    bad(data2->date[j], data1->date[j], "date");
    bad(data2->n_baselines[j], data1->n_baselines[j], "n_baselines");

    for (int k=0; k<data2->n_baselines[j]; ++k) {
      bad(data2->baseline[j][k], data1->baseline[j][k], "baseline codes");
    }
  }

  cout << "Adding\n";

  for (int ch=0; ch<data2->n_freq; ++ch) {
    for (int vis=0; vis<data2->n_vis; ++vis)
      for (int bl=0; bl<data2->n_baselines[vis]; ++bl)

        for (int pol=0; pol<NUM_POLS; ++pol) {
          data1->visdata[vis][2*(pol+data1->n_pol*(ch+data1->n_freq*bl))] +=
                      data2->visdata[vis][2*(pol+data2->n_pol*(ch+data2->n_freq*bl))];
          data1->visdata[vis][2*(pol+data1->n_pol*(ch+data1->n_freq*bl))+1] +=
                        data2->visdata[vis][2*(pol+data2->n_pol*(ch+data2->n_freq*bl))+1];
        }
  }
  data1->source->ra /= 15;

 
  cout << "Writing " << argv[argc-1] << endl;
  writeUVFITS(argv[argc-1], data1);

  freeUVFITSdata(data1);
  freeUVFITSdata(data2);

  readUVFITS(argv[argc-1], &data1);

  cout << "Test read in\n";
  for (int j=0; j<5; ++j) {
    int index = (0+data1->n_pol*(test_ch_in+data1->n_freq*(test_bl+j)))*2;

    cout << j << endl;
    cout << data1->u[data1->n_vis-1][j+test_bl] << " " << data1->v[data1->n_vis-1][j+test_bl] << " " << data1->w[data1->n_vis-1][j+test_bl] << " " << data1->n_baselines[data1->n_vis-1] << " " << data1->baseline[data1->n_vis-1][j+test_bl] << endl;
    cout << data1->visdata[data1->n_vis-1][index] << " "  << data1->visdata[data1->n_vis-1][index+1] << " " << data1->weightdata[data1->n_vis-1][index] << endl;

  }
}
