/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <iostream>
#include <iomanip>
#include <complex>
#include "uvfits.h"

using namespace std;

#define NUM_POLS 4
#define HORIZON_ADD_ON 1	// pixels in the FFT array - you have to work out the delay it corresponds to

class Visibility {
public:
  int st1, st2;
  double u, v, w, uv_length, k_perp, length;
  float bl_code;
  complex<double> value;
  double weight;
  int bin_weight;
  int bin, bin_u, bin_v;
  bool in_use;
  
  Visibility() { st1 = st2 = -1; uv_length = k_perp = length = bl_code = 0; value = complex<double>(0, 0); weight = -1; in_use = true; bin_weight = bin_u = bin_v = 0; }
  void set(double values[2+2*NUM_POLS], int freq, int which_pol);
  void reset_k_perp(double uv_length, int frequency);
  void reset_uv(double new_u, double new_v, int freq);
  void print() { cout << "VIS " << bl_code << " " << st1 << " "  << st2 << " "  << setprecision(8) << u << " " << v << " " << w << " " << uv_length << " " << k_perp << " " << value << " " << abs(value) << " " << weight << " " << bin_weight << " " << in_use << endl; }
};

class Visibilities {
public:
  int num_baselines, num_channels, chan_width;
  int *frequencies;
  double max_length();		// in meters
  double max_k_perp();
  double max_uv_length();
  double lst_seconds;
  string cache_file;
  int scrunch;
  Visibility **visibilities;


  Visibilities() { visibilities = NULL; frequencies = NULL; num_baselines = num_channels = 0; chan_width = 24000; scrunch = 1; lst_seconds = 0; }
  Visibilities(uvdata *data, int time, int file_index, int which_pol, bool ignore_channel_flags, char *fname);
  ~Visibilities();
  Visibility *find(int st1, int st2, int channel);
  Visibility *find(float code, int channel);
  void cache(string fname);
  void uncache();
  void sort_by_uv_length();
  void interpolate();
#ifdef NOISE
  void replace_with_noise(int which_pol);
#endif
  int num_used_in_baseline(int bl);
  void flag_short_baselines();
  void flag_partial_baselines();
  void flag_whole_baseline(int bl);
  void flag_whole_channel(int ch);
  bool whole_baseline_flagged(int bl);
  bool whole_channel_flagged(int ch);
  void deconvolve(const char *window);
  void do_scrunch(const int num);	// scrunch channels
  void stats(double& min, double& max, double& mean);
  void report();	// on zeros
};

extern void load_uvdata(char *fname, Visibilities**& all, const int which_pol, const int file_index, const int scrunch_num, int& num_times, int& num_channels, const char *window, const bool number_only, const bool one_only, const bool ignore_channel_flags, const bool interpolate);
extern int get_channels(char *fname);
