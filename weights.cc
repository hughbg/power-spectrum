/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include "visibilities.h"

int main(int argc, char *argv[]) {
  int num;
  Visibilities **all;
  int junk;

  load_uvdata(argv[1], all, 0, 1, junk, junk, false, true, false);
  all[0]->report();
}

