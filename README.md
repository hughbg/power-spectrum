# Power Spectrum

Produces 21cm "wedge" power spectra from calibrated interferometric visibilities. The input are UVFits files containing  visibilities, these are FFT'd, gridded, and placed in k-mode space. This program is currently used in-house
by the [LEDA](http://www.tauceti.caltech.edu/leda) project. It is not (yet) meant for public use, but support will be provided if you want to try it.

The program is written in C++ and intended to be built and run on Linux machines.

Required packages: 

* fftw3 
* cfitsio
* NNFFT for non-equispaced FFTs
* GNU GSL for numerical integration
* BLAS for algebra

There is a Makefile, the program is called "power_spectrum" and is built using "make". There are ancillary programs that may be helpful, these need to be made separately. 
Run the power_spectrum program like this:


```
power_spectrum [ options ] uvfits_file ...
Options:
	-s          separate positive and negative sides of the FFT (default: false)
	-i          interpolate flagged channels using a spline function (default: false)
	-d          save power spectra for all the correlator dumps (default: false)
	-f size     FFT size (default: 2x number of channels)
	-1          use only the first time step in the UV fits file (default: false)
	-p pol      which polarization to use (default: XX). XX,YY, XY, YX
	-w window   what window to put over the FFT (default: none)
	-r num      scrunch num channels into 1 (default: no scrunch)
	-c          ignore channel flags (default: false)
	-W          apply weight to k_perp bins (default: false)
	-t num      number of threads to use (default: 1)
```
The input data is in UVFits format. The UVFits is read by routines in code file uvfits.c, supplied by by [MWA](http://www.mwatelescope.org). A UVFits file contains visibilities from the baselines and frequency channels,  from several  observations of the sky at different times
(separated by 9s for LEDA data).  Visibilities are loaded by the power_spectrum program into the Visibilities class. 

Once visibilities are loaded, the PowerSpectrum class does the Fourier transform of the baselines (delay-transform), and it does this on all the snapshots in all the files passed on the command line,
producing many grids, by time,  of delay-transformed visibilities. Reduction steps are then performed (multiplying and integrating and binning) to produce a single grid as output. This is then converted to power in P(k) units
and the axes are converted to k-perp, k-parallel by calculating cosmological distances (conversions.cc).

The Grid class is fundamental. It provides a 2-D array with indexing using physical values (floating point), and  allows multiple co-ordinate systems to overlay on the same grid. 
 Grids can be clipped, scaled, binned, summed etc. The power spectra are stored in Grids.

The output of the program is a file called "power_spectrum.dat" in the form of a 2-D grid saved to a text file. It is plotted with the script plot_ps.py. The file has a header line and a
 footer line with the raw grid data in between. The header/footer provides information
about the physical extent of the grid, where the horizon line is, and so on.


Some algorithms have been implemented in a certain way due to the low frequencies of OVRO-LWA. The research paper will explain more about this.

[Documentation](https://hughbg.bitbucket.io/ps-doc)
