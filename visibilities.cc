/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <unistd.h>
#include <fenv.h>
#include <iomanip>
#include <cstdlib>
#include <cstring>
#include <fstream>
#include <random>
#include "fftw3.h"
#include "visibilities.h"
#include "conversions.h"
#include "flags.h"
#include "times.h"

extern string strip_file_name(char *);
extern void gen_skypass_delay(double max_bl, double sdf, int nchan, int& uthresh, int& lthresh, double max_bl_add=0.);
extern fftw_complex *pspec_prep(fftw_complex *d, double *window, double tol, int *area, int length, bool opts_model);
void make_window(double* window_data, const char *window, int length);

extern "C" { 
	void nfft_equispace(double **vis, double **locations, int len, double max_location, int *new_len);
	void nfft_equispace_test(double **vis, double **uv, int len, int *new_len);
}


void check_not_null(void *a, const char *message) {
  if ( a == NULL ) {
    cerr << "Failed to get memory " << message << "\n";
    exit(1);
  }
}


#ifdef NOISE
#include "noise_settings.h"
#endif

static const int c = 299792458;

void Visibility::set(double values[4+3*NUM_POLS], int freq, int which_pol) {
    Conversions conv;
    
    st1 = (int)values[0]/256;
    st2 = (int)values[0]%256;
    u = values[1];
    v = values[2];
    w = values[3];
    uv_length = sqrt(u*u+v*v+w*w);

    value = complex<double>(values[4+which_pol*3], values[4+which_pol*3+1]);
    weight = values[4+which_pol*3+2];
    length = uv_length*c/freq;

    // Convert uv_length to k_perp
    if ( uv_length >= 0.5 ) {	// sin^-1 will work
    
    	double z21cm = conv.f2z(freq/1e9);
    	k_perp = conv.k_perp(uv_length, z21cm);
        //cout << k_perp << " " << uv_length<<endl;

    }	// else set to not in use, done later on, so as not to confuse the checks against the UVfits weights

}

void Visibility::reset_uv(double new_u, double new_v, int freq) {
    Conversions conv;

    u = new_u;
    v = new_v;
    uv_length = sqrt(u*u+v*v);

    if ( uv_length < 0.5 ) {
     	in_use = false;
     	//cout << "out\n";
     } else {
     
     	double z21cm = conv.f2z(freq/1e9);
     	k_perp = conv.k_perp(uv_length, z21cm);
     	length = uv_length*c/freq;
     }
}

void Visibility::reset_k_perp(double kp, int frequency) {
	Conversions conv;
	
    // Convert to kp as k_perp and make things consistent
    double z21cm = conv.f2z(GHz(frequency));

    k_perp = kp;
    if ( kp == 0 ) uv_length = 0;
    else uv_length = conv.uv_length(k_perp, z21cm);
    length = uv_length*c/frequency;
}

// For sorting
struct bl_pair {
   double uv_length;
   int bl;
};

int compare (const void *a, const void *b) {
  double diff = ((bl_pair*)a)->uv_length - ((bl_pair*)b)->uv_length;
  if ( diff < 0 ) return -1;
  else if ( diff > 0 ) return 1;
  else return 0;
}

void Visibilities::stats(double& out_min, double& out_max, double& out_mean) {
  double sum = 0;
  double min = 1e39;
  double max = 0;
  int count = 0;
  for (int ch=0; ch<num_channels; ++ch) 
    for (int bl=0; bl<num_baselines; ++bl) 
      if ( visibilities[ch][bl].in_use ) {
        double val = abs(visibilities[ch][bl].value);
	if ( val < min ) min = val;
        if ( val > max ) max = val;
        sum += val;
        ++count;
      }
        
  if ( count == 0 ) {
    cerr << "No visibilities in use!\n";
    exit(1);
  }
  out_min = min;
  out_max = max;
  out_mean = sum/count;
}

void Visibilities::sort_by_uv_length() {
  bl_pair pairs[num_baselines];
  for (int bl=0; bl<num_baselines; ++bl) {
    pairs[bl].bl = bl;
    pairs[bl].uv_length = visibilities[0][bl].uv_length;
  }

  qsort (pairs, num_baselines, sizeof(bl_pair), compare);  

  Visibility** new_visibilities = new Visibility*[num_channels]; check_not_null(new_visibilities, "in sort_by_uv_length");
  for (int ch=0; ch<num_channels; ++ch) {
    new_visibilities[ch] = new Visibility[num_baselines]; check_not_null(new_visibilities[ch], "in sort_by_uv_length");
  }

  for (int ch=0; ch<num_channels; ++ch) 
    for (int bl=0; bl<num_baselines; ++bl) 
      new_visibilities[ch][bl] = visibilities[ch][pairs[bl].bl];


  if ( visibilities != NULL ) {
    for (int ch=0; ch<num_channels; ++ch)
      delete[] visibilities[ch];
    delete[] visibilities;
  }

  visibilities = new_visibilities;

  /*for (int ch=0; ch<num_channels; ++ch)
    for (int bl=0; bl<num_baselines; ++bl)
      cout << visibilities[ch][bl].uv_length << " " << frequencies[ch] << endl;*/
}


static bool close(double x, double y) {
  if ( abs(x-y)/y < 0.00001 ) return true;
  else return false;
}

static void save_array(const char *fname, double *l, double *a, int len) {
  FILE *f;
  f = fopen(fname, "w");
  for (int i=0; i<len; ++i) {
    fprintf(f, "%f %f %f\n", l[i], a[2*i], a[2*i+1]);
  }
  fclose(f);
}

int Visibilities::num_used_in_baseline(int bl) {
  int num_in_use = 0;
  for (int ch=0; ch<num_channels; ++ch)
    if ( visibilities[ch][bl].in_use ) ++num_in_use;
  return num_in_use;
}

bool Visibilities::whole_baseline_flagged(int bl) {
  return num_used_in_baseline(bl) == 0;
}

void Visibilities::flag_whole_baseline(int bl) {
  for (int ch=0; ch<num_channels; ++ch) {
    visibilities[ch][bl].in_use = false;
    visibilities[ch][bl].value = 0;
    visibilities[ch][bl].weight = -abs(visibilities[ch][bl].weight);
  }
}

bool Visibilities::whole_channel_flagged(int ch) {
  int num_in_use = 0;
  for (int bl=0; bl<num_baselines; ++bl)
    if ( visibilities[ch][bl].in_use ) ++num_in_use;
  return num_in_use == 0;
}

void Visibilities::flag_whole_channel(int ch) {
  for (int bl=0; bl<num_baselines; ++bl) {
    visibilities[ch][bl].in_use = false;
    visibilities[ch][bl].value = 0;
    visibilities[ch][bl].weight = -abs(visibilities[ch][bl].weight);
  }
}


// Modified to load individual snapshots
Visibilities::Visibilities(uvdata *data, int time, int file_index, int which_pol, bool ignore_channel_flags, char *fname) {
    const int c = 299792458;
    Flags flags;
    FileTimes file_times;
    double values[4+3*NUM_POLS];
    double max_uv_length=0;
    double min, max, mean;
    
    if ( time < 0 || data->n_vis <= time ) {
          cerr << "Request to extract invalid time from uvfits\n";
          exit(1);
       }

/* Dummy vis
  num_baselines = 4;
  num_channels = 4;
  which_pol = 0;
  chan_width = 24000;
  frequencies = new int[4];
  for (int i=0; i<4; ++i) frequencies[i] = 43000000+i*chan_width;
  visibilities = new Visibility*[4];
  for (int i=0; i<4; ++i) visibilities[i] = new Visibility[4];
  for (int i=0; i<4; ++i)
    for (int j=0; j<4; ++j) {
      values[0] = i+256*j; values[1] = i; values[2] = j; values[3] = 0;
      for (int pol=0; pol<NUM_POLS; ++pol) {
        values[4+pol*3] = 99;
        values[4+pol*3+1] = 20;
        values[4+pol*3+2] = -9;
      }
      visibilities[i][j].set(values, frequencies[i], which_pol);
    }
  return;
*/

    chan_width = data->freq_delta;

    frequencies = new int [data->n_freq]; check_not_null(frequencies, "in Visibilities");
    for (int i=0; i<data->n_freq; ++i) {
      frequencies[i] = (int)data->cent_freq+(i-data->n_freq/2)*(int)data->freq_delta;
    }

    for (int i=1; i<data->n_freq; ++i) {
      if ( frequencies[i-1] >= frequencies[i] ) {
        cerr << "Frequencies are not ordered\n";
        exit(1);
      }
    }

    if ( data->n_pol != NUM_POLS ) {
      cerr << "Not " << NUM_POLS << " pols, can't handle " << data->n_pol << "\n";
      exit(1);
    }

    if ( data->n_IF != 1 ) {
      cerr << "Not 1 IF, can't handle it\n";
      exit(1);
    }

    num_baselines = data->n_baselines[time];
    num_channels = data->n_freq;

    cout << "Num baselines " << data->n_baselines[time] << " Num channels " << data->n_freq  << " Base freq " << frequencies[0] << endl;

    visibilities = new Visibility*[data->n_freq]; check_not_null(visibilities, "in Visibilities");
    for (int ch=0; ch<data->n_freq; ++ch) {
      visibilities[ch] = new Visibility[data->n_baselines[time]]; check_not_null(visibilities[ch], "in Visibilities");
    }

    file_times.load("uvfits_files_times.txt");
    lst_seconds = file_times.get_time(strip_file_name(fname));
    if ( lst_seconds <= 0 ) {
      cerr << "Failed to get an LST time for " << fname << endl;
      exit(1);
    }
 
    flags.load_vis_flags(file_index);     // stands, channels, baselines

    for (int bl=0; bl<data->n_baselines[time]; ++bl) {
      double u0 = data->u[time][bl]; // These uvw are from channel 0. They are equal to uv_length/freq = uv_length*wavelength/c,
      double v0 = data->v[time][bl]; // multiplying by c gives length in meters. Can build correct uv_length for all channels.
      double w0 = data->w[time][bl]; // Frequency used for division is in Hz.

      double u_distance = u0*c;		// meters
      double v_distance = v0*c;
      double w_distance = w0*c;

      for (int freq=0; freq<data->n_freq; ++freq) {
        int st1, st2;
        DecodeBaseline(data->baseline[time][bl], &st1, &st2); --st1; --st2;
        values[0] = st1*256+st2;

        double u = u_distance*frequencies[freq]/c;      // uv_length in units of wavelengths
        double v = v_distance*frequencies[freq]/c;
        double w = w_distance*frequencies[freq]/c;

        values[1] = u; values[2] = v; values[3] = w;

        for (int pol=0; pol<NUM_POLS; ++pol) {
          double weight = data->weightdata[time][bl*(data->n_pol*data->n_freq) + freq*data->n_pol + pol]; 
          int index = pol+data->n_pol*(freq+data->n_freq*bl);     // indexing from readUVFITS in uvfits.c
          values[4+pol*3] = data->visdata[time][index*2];
          values[4+pol*3+1] = data->visdata[time][index*2+1];
          values[4+pol*3+2] = weight;

        }

        visibilities[freq][bl].set(values, frequencies[freq], which_pol);

	// Flags loaded here are used to set "in_use", whereas weights are used in the UVFits to indicated flagged data.

        visibilities[freq][bl].bl_code = data->baseline[time][bl];
        if ( visibilities[freq][bl].st1 == 4 && visibilities[freq][bl].st2 == 50 && freq == 40 ) { cout << bl << " "; visibilities[freq][bl].print(); }
        if ( visibilities[freq][bl].uv_length > max_uv_length ) max_uv_length = visibilities[freq][bl].uv_length;

        if ( flags.in(flags.baseline_flags, visibilities[freq][bl].st1, visibilities[freq][bl].st2) ) visibilities[freq][bl].in_use = false; 


        if ( !ignore_channel_flags )
          if ( flags.in(flags.channel_flags, freq) || flags.in(flags.rfi_channel_flags, freq, time) ) {
            visibilities[freq][bl].in_use = false;
          }

        if ( flags.in(flags.stand_flags, visibilities[freq][bl].st1) || flags.in(flags.stand_flags, visibilities[freq][bl].st2) ) visibilities[freq][bl].in_use = false; 


      }
   
    }
   cout << "Max UV length " << max_uv_length << endl;

   stringstream base;
   base << strip_file_name(fname) << "_" << time << "_warn.dat";
   ofstream warnings(base.str());
  
   // Checks   
   
   for (int bl=0; bl<num_baselines; ++bl) {
      bool all_weight_flagged = true;
      for (int ch=0; ch<num_channels; ++ch) {
        if ( visibilities[ch][bl].weight > 0 ) all_weight_flagged = false;

        if ( !close(visibilities[ch][bl].length, visibilities[0][bl].length) ) {
          (warnings.is_open()?warnings:cerr) << 
			"Baselines don't all have same physical length in channel\n";
	  exit(1);
	}
      }
      if ( !whole_baseline_flagged(bl) && all_weight_flagged ) {		
	// All channels are flagged by weight, but some are in_use
         (warnings.is_open()?warnings:cerr) << 
		"BL " << bl << " (" << visibilities[0][bl].st1 << " " << 
		visibilities[0][bl].st2 << ") is in use but flagged by weight. Setting not in_use.\n";
         flag_whole_baseline(bl);
      }
    }

   for (int bl=0; bl<num_baselines; ++bl) {
      bool all_zero = true;
      for (int ch=0; ch<num_channels; ++ch) {
        if ( abs(visibilities[ch][bl].value) != 0 ) all_zero = false;

      }
      if ( !whole_baseline_flagged(bl) && all_zero ) {	
	  // All channels are 0-valued, but some are in_use
         (warnings.is_open()?warnings:cerr) << "BL " << bl << " (" << 
		visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << 
		") is in use but is all zero. Setting not in_use.\n";
         flag_whole_baseline(bl);
      }
    }


    for (int ch=0; ch<num_channels; ++ch) {
      bool weight_flagged = true;
      for (int bl=0; bl<num_baselines; ++bl) {
        if ( visibilities[ch][bl].weight > 0 ) weight_flagged = false;
        
      }
      if ( !whole_channel_flagged(ch) && weight_flagged ) {	
 	 // All baselines for this channel are flagged by weight, but some are in_use
         (warnings.is_open()?warnings:cerr) << "CH " << ch << 
		" is in use but is flagged by weight. Setting not in_use.\n";
         flag_whole_channel(ch);
      }
    }

    for (int ch=0; ch<num_channels; ++ch) {
      bool all_zero = true;
      for (int bl=0; bl<num_baselines; ++bl) {
        if ( abs(visibilities[ch][bl].value) != 0 ) all_zero = false;
      }
      if ( !whole_channel_flagged(ch) && all_zero ) {	// All vis are 0 but some vis are in_use
         (warnings.is_open()?warnings:cerr) << "CH " << ch << 
		" is in use but is all zero. Setting not in_use.\n";
         flag_whole_channel(ch);
      }
    }

    for (int ch=0; ch<num_channels; ++ch) {
      bool no_weight_flagged = true;
      for (int bl=0; bl<num_baselines; ++bl) {
        if ( visibilities[ch][bl].weight <= 0 ) no_weight_flagged = false;
      }
      if ( whole_channel_flagged(ch) && no_weight_flagged ) {	
	// all vis are not in_use and all weight are > 0 - 
	// the whole channel should be flagged but it is not flagged in the UVFits
        (warnings.is_open()?warnings:cerr) << "CH " << ch << 
		" is not in use but all weight > 0. Setting all weight < 0.\n";
	for (int bl=0; bl<num_baselines; ++bl) visibilities[ch][bl].weight = -9;
      }
    }


   for (int ch=0; ch<num_channels; ++ch) {
      for (int bl=0; bl<num_baselines; ++bl) {
        if ( visibilities[ch][bl].in_use && visibilities[ch][bl].weight < 0 ) {
          (warnings.is_open()?warnings:cerr) << "CH " << ch << " BL " << "(" << visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << ")"  << " in use but weight < 0. Setting not in_use.\n";
          visibilities[ch][bl].in_use = false;
        }
        if ( visibilities[ch][bl].in_use && visibilities[ch][bl].weight == 0 ) {
          (warnings.is_open()?warnings:cerr) << "CH " << ch << " BL " << "(" << visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << ")"  << " in use but weight == 0. Setting not in_use.\n";
          visibilities[ch][bl].in_use = false;
        }
        if ( !visibilities[ch][bl].in_use && visibilities[ch][bl].weight > 0 )
          (warnings.is_open()?warnings:cerr) << "CH " << ch << " BL " << bl << " " << "(" << visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << ")" << " not in use but weight > 0\n";
      }
    }

    if ( warnings.is_open() ) warnings.close();

    /*for (int bl=0; bl<num_baselines; ++bl) {
      bool high_values = false;
      for (int ch=0; ch<num_channels; ++ch)
        if ( abs(visibilities[ch][bl].value) > 1e5 ) high_values = true;
      if ( high_values && !(flags.in(flags.baseline_flags, visibilities[0][bl].st1, visibilities[0][bl].st2) || flags.in(flags.stand_flags, visibilities[0][bl].st1) || flags.in(flags.stand_flags, visibilities[0][bl].st2)) ) {
         cerr << "BL " << bl << " (" << visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << ") is in use but is has high values. Flagging it.\n";
         for (int ch=0; ch<num_channels; ++ch) visibilities[ch][bl].in_use = false;
      }
    }*/

    //sort_by_uv_length(); 


    // Add noise
    /*double max_val = 0;
    for (int ch=0; ch<num_channels; ++ch)
      for (int bl=0; bl<num_baselines; ++bl)
        if ( abs(visibilities[ch][bl].value) > max_val ) max_val = abs(visibilities[ch][bl].value);
    for (int ch=0; ch<num_channels; ++ch)
      for (int bl=0; bl<num_baselines; ++bl) {
        visibilities[ch][bl].value = complex<double>(visibilities[ch][bl].value.real()+((0.5-(double)rand()/RAND_MAX)*max_val/100),
                        visibilities[ch][bl].value.imag()+((0.5-(double)rand()/RAND_MAX)*max_val/100));
        if ( visibilities[ch][bl].st1 == 4 && visibilities[ch][bl].st2 == 46 && ch == 40 ) visibilities[ch][bl].print();
     }
     */
   /* 
    // Calc UV length change
    for (int bl=0; bl<num_baselines; ++bl) {
      if ( visibilities[0][bl].in_use ) {
        cout << "BL " << bl << " " << (visibilities[num_channels-1][bl].uv_length-visibilities[0][bl].uv_length)/visibilities[0][bl].uv_length*100 << "% " << visibilities[num_channels-1][bl].uv_length-visibilities[0][bl].uv_length << endl;
        // Liu criteria
        double b = visibilities[0][bl].length;
        double theta = 1; //3*M_PI/180;
        double Bband = frequencies[num_channels-1]-frequencies[0];
        double c = 299792458;
        //cout << b << " " << Bband << " " << "if " << b*theta << " >> " << c/Bband << " then bad\n";
        if ( b*theta > c/Bband*2 ) cout << "bad\n"; else cout << "good\n"; 
        if ( b*theta > c/Bband*2 ) for (int ch=0; ch<num_channels; ++ch) visibilities[ch][bl].in_use = false;
      }
    }  */

    stats(min, max, mean);
    cout << "Vis stats min " << min << " " << max << " " << mean << endl;


    cout << "Done loading\n";


  /* Cut edges of the band. Put in separate method */
 /*
  int cut = 4;
  int new_num_channels = num_channels-2*cut;
  Visibility **new_vis = new Visibility*[new_num_channels]; check_not_null(new_vis, "in scrunch");
  int *new_freq = new int[new_num_channels];

  for (int ch=0; ch<new_num_channels; ++ch) {
    new_vis[ch] = new Visibility[num_baselines]; check_not_null(new_vis[ch], "in scrunch");
  }
  for (int ch=0; ch<num_channels; ++ch) {
    if ( cut <= ch && ch < num_channels-cut ) {
      for (int bl=0; bl<num_baselines; ++bl)
        new_vis[ch-cut][bl] = visibilities[ch][bl];
      new_freq[ch-cut] = frequencies[ch];
    }
  }

  for (int ch=0; ch<num_channels; ++ch) delete[] visibilities[ch];
  delete visibilities;
  visibilities = new_vis;
  delete frequencies;
  frequencies = new_freq;
  num_channels = new_num_channels;
  */
}

void Visibilities::flag_short_baselines() {
    // Get rid of baselines that have a uv < 1 anywhere, and single visibilities with uv_length < 0.5
    for (int bl=0; bl<num_baselines; ++bl) {
       bool got_short_uv = false;
       for (int ch=0; ch<num_channels; ++ch)
         if ( visibilities[ch][bl].uv_length < 1 ) got_short_uv = true;
       if ( got_short_uv )
         flag_whole_baseline(bl);

       for (int ch=0; ch<num_channels; ++ch)
         if ( visibilities[ch][bl].uv_length < 0.5 ) { 
           visibilities[ch][bl].in_use = false;
	   visibilities[ch][bl].weight = -abs(visibilities[ch][bl].weight);
         }
    }
    cout << "Flagged out uv < 1 baselines, and uv_length < 0.5\n";
}

void Visibilities::flag_partial_baselines() {
  const double threshold = 0.75;   // fewer than this many channels and wipe the whole baseline
  for (int bl=0; bl<num_baselines; ++bl) {
    int num_in_use = 0;
    for (int ch=0; ch<num_channels; ++ch)
      if ( visibilities[ch][bl].in_use ) ++num_in_use;
    if ( (double)num_in_use/num_channels < threshold ) 
      flag_whole_baseline(bl);
  }
  cout << "Flagged out partial baselines\n";
}

#ifdef NOISE
void Visibilities::replace_with_noise(int which_pol) {
    // A channel that has a noise value put into it should not be flagged. Therefore,
    // unflag such channels, _unless_ the whole baseline is flagged out, because we
    // have to honour baseline and stand flagging.

    default_random_engine generator_real_0, generator_imag_0, generator_real_1, generator_imag_1;
    normal_distribution<double> distribution_real_0(0, NOISE_RMS_REAL_0);   // Different polarizations
    normal_distribution<double> distribution_imag_0(0, NOISE_RMS_IMAG_0);
    normal_distribution<double> distribution_real_1(0, NOISE_RMS_REAL_1);
    normal_distribution<double> distribution_imag_1(0, NOISE_RMS_IMAG_1);

    generator_real_0.seed(14592.90092760546); generator_imag_0.seed(180522.42912004725); 
    generator_real_1.seed(93272.4624326688); generator_imag_1.seed(2152.1163543751);

    for (int bl=0; bl<num_baselines; ++bl) {
      bool baseline_used = !whole_baseline_flagged(bl);
      for (int ch=0; ch<num_channels; ++ch) {

        if ( which_pol == 0 ) visibilities[ch][bl].value =
          complex<double>(distribution_real_0(generator_real_0), distribution_imag_0(generator_imag_0));
        else if ( which_pol == 1 ) visibilities[ch][bl].value =
          complex<double>(distribution_real_1(generator_real_1), distribution_imag_1(generator_imag_1));
        else {
          cerr << "Unimplemented pol " << which_pol << " for NOISE\n";
          exit(1);
        }
        if ( baseline_used ) visibilities[ch][bl].in_use = true;
      }
    }
}
#endif

extern void interpolate(vector<complex<double> >& array);

void Visibilities::interpolate() {
  for (int bl=0; bl<num_baselines; ++bl) {
    if ( !whole_baseline_flagged(bl) ) {
      vector<complex<double> > baseline;

      for (int ch=0; ch<num_channels; ++ch) 
        if ( visibilities[ch][bl].in_use ) baseline.push_back(visibilities[ch][bl].value);
        else baseline.push_back(0);

      ::interpolate(baseline);

      for (int ch=0; ch<num_channels; ++ch) { 
        visibilities[ch][bl].value = baseline[ch];
        visibilities[ch][bl].in_use = true;
        visibilities[ch][bl].weight = abs(visibilities[ch][bl].weight);
      }
    }
  }
  cout << "Interpolated usable baselines\n";
}

double Visibilities::max_length() {
	double ml = 0;
	
	for (int ch=0; ch<num_channels; ++ch)
		for (int bl=0; bl<num_baselines; ++bl)
			if ( visibilities[ch][bl].in_use && visibilities[ch][bl].length > ml ) ml = visibilities[ch][bl].length;
	return ml;
}

double Visibilities::max_k_perp() {
	double ml = 0;
        int where_ch, where_bl;
	
	for (int ch=0; ch<num_channels; ++ch)
		for (int bl=0; bl<num_baselines; ++bl) {
			if ( visibilities[ch][bl].in_use && visibilities[ch][bl].k_perp > ml ) {
			  ml = visibilities[ch][bl].k_perp;
			  where_bl = bl; where_ch = ch;
			}
	}
	//cout << "max_k_perp " << ml << " at " << where_ch << " " << where_bl<< endl;
	return ml;
}

double Visibilities::max_uv_length() {
	double ml = 0;
	
	for (int ch=0; ch<num_channels; ++ch)
		for (int bl=0; bl<num_baselines; ++bl) { 
			if ( visibilities[ch][bl].in_use && visibilities[ch][bl].uv_length > ml ) ml = visibilities[ch][bl].uv_length;
		}
	return ml;
}


Visibility *Visibilities::find(int st1, int st2, int channel) {
  if ( visibilities == NULL ) return NULL;
  if ( channel < 0 || num_channels <= channel ) {
    cerr << "Invalid channel in find\n";
    exit(1);
  }

  for (int bl=0; bl<num_baselines; ++bl) 
    if ( visibilities[channel][bl].st1 == st1 && visibilities[channel][bl].st2 == st2 ) return &visibilities[channel][bl];

  return NULL;
}

Visibility *Visibilities::find(float code, int channel) {
  if ( visibilities == NULL ) return NULL;
  if ( channel < 0 || num_channels <= channel ) {
    cerr << "Invalid channel in find\n";
    exit(1);
  }
 
  for (int bl=0; bl<num_baselines; ++bl)
    if ( visibilities[channel][bl].bl_code == code ) return &visibilities[channel][bl];

  return NULL;
}

inline bool exists(string name) {
    return ( access( name.c_str(), F_OK ) != -1 );
}

Visibilities::~Visibilities() {
  if ( frequencies != NULL ) {
    delete[] frequencies; frequencies = NULL;
  }
  if ( visibilities != NULL ) {
    for (int ch=0; ch<num_channels; ++ch)
      delete[] visibilities[ch];
    delete[] visibilities;
  }

  visibilities = NULL;
  num_channels = num_baselines = 0;

  if ( cache_file.length() > 0 && exists(cache_file) ) remove(cache_file.c_str());

}

void Visibilities::deconvolve(const char *window) {
  fftw_complex *signal = new fftw_complex[num_channels];
  fftw_complex *result;
  double *window_data = new double[num_channels];
  int *area = new int [num_channels];

  make_window(window_data, window, num_channels);
  for (int bl=0; bl<num_baselines; ++bl) {
    if ( num_used_in_baseline(bl) > num_channels/2 && num_used_in_baseline(bl) < num_channels ) {
      // Calculate where the horizon is in the fft and set the area array.
      double horizon = visibilities[0][bl].length/c*1e9;
      int uthresh, lthresh;
      gen_skypass_delay(horizon, chan_width/1e9, num_channels, uthresh, lthresh, 0);
      for (int i=0; i<num_channels; ++i) area[i] = 0;
      for (int i=0; i<=uthresh+HORIZON_ADD_ON; ++i) area[i] = 1;
      for (int i=num_channels-lthresh-HORIZON_ADD_ON; i<num_channels; ++i) area[i] = 1;

      for (int ch=0; ch<num_channels; ++ch) {
        if ( visibilities[ch][bl].in_use ) {
          signal[ch][0] = visibilities[ch][bl].value.real();
          signal[ch][1] = visibilities[ch][bl].value.imag();
        } else signal[ch][0] = signal[ch][1] = 0;     // this indicates missing data
      }

      result = pspec_prep(signal, window_data, 1e-8, area, num_channels, true);
      for (int ch=0; ch<num_channels; ++ch) {
        visibilities[ch][bl].value = complex<double>(result[ch][0], result[ch][1]);
        visibilities[ch][bl].in_use = true;
      }
      delete[] result;
    } else 
      for (int ch=0; ch<num_channels; ++ch) {
        visibilities[ch][bl].value = 0;
        visibilities[ch][bl].in_use = false;
      }

  }
  delete[] signal; delete[] window_data; delete[] area;
  cout << "Done deconvolution\n";
}

void Visibilities::do_scrunch(const int num) {
  int scrunch_count;
  int new_num_channels = num_channels/num;
  int *new_freqs;
  Visibility** new_vis; 

  if ( visibilities == NULL ) return;

  cout << "Scrunching by " << num << " New num channels " << new_num_channels;
  new_vis = new Visibility*[new_num_channels]; check_not_null(new_vis, "in scrunch");
  for (int ch=0; ch<new_num_channels; ++ch) {
    new_vis[ch] = new Visibility[num_baselines]; check_not_null(new_vis[ch], "in scrunch");
  }
  for (int bl=0; bl<num_baselines; ++bl) {

    for (int ch=0; ch<new_num_channels; ++ch) {		// looping over new channels not old
    
	// Need to averege all uv_lengths etc. but do not include visibility values if flagged.
	// I had visibilities are summed not averaged but that is bad because it will skew the result when there are flagged channels.
        // It must be an average, and the channel width increases by 4, so it balances out. 
        // http://mysupercomputerexperience.blogspot.com/2019/05/scrunching-fft-and-physical-units.html.
      memset(&new_vis[ch][bl], 0, sizeof(Visibility));  // zero all new values

      // These don't depend on in_use, they are no vsiibility values
      new_vis[ch][bl].st1 = visibilities[ch*num][bl].st1; 
      new_vis[ch][bl].st2 = visibilities[ch*num][bl].st2; 
      new_vis[ch][bl].bl_code = visibilities[ch*num][bl].bl_code;
      for (int i=0; i<num; ++i) { 
        new_vis[ch][bl].uv_length += visibilities[ch*num+i][bl].uv_length;
        new_vis[ch][bl].k_perp += visibilities[ch*num+i][bl].k_perp;
        new_vis[ch][bl].length += visibilities[ch*num+i][bl].length;
        new_vis[ch][bl].weight += visibilities[ch*num+i][bl].weight;
      }
      new_vis[ch][bl].uv_length /= num;
      new_vis[ch][bl].k_perp /= num;
      new_vis[ch][bl].length /= num;
      new_vis[ch][bl].weight /= num;    // Dunno what to do about this, but it's not used in power spectrum generation
 
#ifdef NOISE
      // Average all the noise values, ignore channel flagging
      new_vis[ch][bl].in_use = false;
      for (int i=0; i<num; ++i) { 
        new_vis[ch][bl].value += visibilities[ch*num+i][bl].value; 
        // If one was good then make this new channel good, actually power spectrum will ignore flagged channels 
        // for noise data and will not interpolate them. However the interaction with pspec_prep will be more
        // complicated.
        if ( visibilities[ch*num+i][bl].in_use ) new_vis[ch][bl].in_use = true;	
      }
      new_vis[ch][bl].value /= num;
#else
      // Now combine visibility values, which depend on flagging      
      new_vis[ch][bl].in_use = true;
      scrunch_count = 0;
      for (int i=0; i<num; ++i) { 
        if ( visibilities[ch*num+i][bl].in_use ) { 
          new_vis[ch][bl].value += visibilities[ch*num+i][bl].value; 
          ++scrunch_count; 
        }
      }  
      if ( scrunch_count != 0 ) new_vis[ch][bl].value /= scrunch_count;
      else new_vis[ch][bl].in_use = false;  
#endif
    }
  }
  
  for (int ch=0; ch<num_channels; ++ch) delete[] visibilities[ch]; 
  delete visibilities;

  if ( frequencies != NULL ) {
    new_freqs = new int[new_num_channels]; check_not_null(new_freqs, "in scrunch");
    for (int ch=0; ch<new_num_channels; ++ch) {
      new_freqs[ch] = frequencies[ch*num];
      for (int i=1; i<num; ++i) new_freqs[ch] += frequencies[ch*num+i];
      new_freqs[ch] /= num;
    }
    delete[] frequencies;
    frequencies = new_freqs;
    cout << " New base freq " << frequencies[0];
  }
  cout << endl;

  chan_width *= num;
  visibilities = new_vis;
  num_channels = new_num_channels;
  scrunch = num;
}

static bool in_v(vector<int>& v, int num) {
  bool found = false; 
  for (int i=0; i<v.size(); ++i) if ( v[i] == num ) found = true;
  return found;
}

void Visibilities::report() {
  bool all_zero, all_weight_flagged, has_zero_weight;
  vector<int> ch_zero_weight, stands_all_zero, stands_weight_flagged, stands_zero_weight;

  cout << "---------------- report -----------------\n";

  for (int st=0; st<256; ++st) {
    all_zero = true;
    all_weight_flagged = true;
    has_zero_weight = false;
    for (int ch=0; ch<num_channels; ++ch) {
        for (int bl=0; bl<num_baselines; ++bl) {
          if ( visibilities[ch][bl].st1 == st || visibilities[ch][bl].st2 == st ) {

            if ( abs(visibilities[ch][bl].value) > 0 ) all_zero = false;
            if ( visibilities[ch][bl].weight > 0 ) all_weight_flagged = false;
  	    if ( visibilities[ch][bl].weight == 0 ) has_zero_weight = true;
          }
        }
    }
    if ( all_zero  ) { cout << "st " << st << " all zero" << endl; stands_all_zero.push_back(st); }
    if ( all_weight_flagged ) { cout << "st " << st <<  " all weight flagged" << endl; stands_weight_flagged.push_back(st); }
    if ( has_zero_weight )  { cout << "st " << st <<  " has a zero weight" << endl;  stands_zero_weight.push_back(st); }
  }
  
 

  for (int ch=0; ch<num_channels; ++ch) {
      all_zero = true;
      all_weight_flagged = true;
      has_zero_weight = false;
      for (int bl=0; bl<num_baselines; ++bl) {
        if ( abs(visibilities[ch][bl].value) > 0 ) all_zero = false;
        if ( visibilities[ch][bl].weight > 0 ) all_weight_flagged = false;
        if ( visibilities[ch][bl].weight == 0 ) has_zero_weight = true;
      }
      if ( all_zero  ) cout << "ch " << ch << " all zero" << endl;
      if ( all_weight_flagged ) cout << "ch " << ch <<  " all weight flagged" << endl;
      if ( has_zero_weight )  { cout << "ch " << ch <<  " has a zero weight" << endl; ch_zero_weight.push_back(ch); }

  }

  for (int bl=0; bl<num_baselines; ++bl) {
    all_zero = true;
    all_weight_flagged = true;
    has_zero_weight = false;
    for (int ch=0; ch<num_channels; ++ch) {

      if ( abs(visibilities[ch][bl].value) > 0 ) all_zero = false;
      if ( visibilities[ch][bl].weight > 0  ) all_weight_flagged = false;
      if ( visibilities[ch][bl].weight == 0 && !in_v(ch_zero_weight, ch) ) has_zero_weight = true;      
    } 
    if ( all_zero && !( in_v(stands_all_zero, visibilities[0][bl].st1) || in_v(stands_all_zero, visibilities[0][bl].st2)) ) cout << "bl " << visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << " all_zero\n";
    if ( all_weight_flagged && !(in_v(stands_weight_flagged, visibilities[0][bl].st1) || in_v(stands_weight_flagged, visibilities[0][bl].st2)) ) cout << "bl " << visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << " all flagged by weight\n";
    if ( has_zero_weight && !(in_v(stands_zero_weight, visibilities[0][bl].st1) || in_v(stands_zero_weight, visibilities[0][bl].st2)) ) cout << "bl " << visibilities[0][bl].st1 << " " << visibilities[0][bl].st2 << " has_zero_weight " << has_zero_weight << endl;
  }

  
}

void Visibilities::cache(string fname) {

  if ( fname.length() == 0 and cache_file.length() == 0) {
    cerr << "cache() passed empty file name\n";
    exit(1);
  }

  if ( fname.length() == 0 ) fname = cache_file;

  ofstream out;
  out.open(fname.c_str(), ios::out | ios::binary);
 
  if ( !out ) {
    cerr << "Failed to open cache file: " << fname << endl;
    exit(1);
  }

  for (int i=0; i<num_channels; ++i) {
    out.write((const char*)visibilities[i], num_baselines*sizeof(Visibility));
    delete[] visibilities[i];
  }
  out.close();

  cache_file = fname;

  delete[] visibilities;
  visibilities = NULL;
}

void Visibilities::uncache() {
  ifstream in;

  if ( cache_file.length() == 0 ) {
    cerr << "uncache() has an empty file name\n";
    exit(1);
  }

  if ( visibilities != NULL ) {
    cerr << "Attempt to uncache visibilities not cached\n";
    exit(1);
  }

  in.open(cache_file.c_str(), ios::in | ios::binary);

  if ( !in ) {
    cerr << "Failed to open cache file in uncache(): " << cache_file << endl;
    exit(1);
  }

  visibilities = new Visibility*[num_channels]; check_not_null(visibilities, "in uncache");
  for (int i=0; i<num_channels; ++i) {
    visibilities[i] = new Visibility[num_baselines]; check_not_null(visibilities[i], "in uncache");
    in.read((char*)visibilities[i], num_baselines*sizeof(Visibility));
  }
  in.close();
  remove(cache_file.c_str());
}


void load_uvdata(char *fname, Visibilities**& all, const int which_pol, const int file_index, 
	const int scrunch_num, int& num_times, int& num_channels, const char *window, const bool info_only, 
	const bool one_only, const bool ignore_flagged_channels, const bool do_interpolation) {
  uvdata *data;
 
  if ( readUVFITS(fname, &data) != 0 ) {
    cerr << "readUVFITS failed\n";
    exit(1);
  }

  num_times = one_only?1:data->n_vis;	// correlator dumps i.e. time steps
  num_channels = data->n_freq/scrunch_num;

  if ( info_only ) { 
    all = NULL;
    freeUVFITSdata(data);
    return;
  }

  all = new Visibilities*[one_only?1:data->n_vis]; check_not_null(all, "in load_uvdata");
  
  for (int i=0; i<(one_only?1:data->n_vis); ++i) {
    all[i] = new Visibilities(data, i, file_index, which_pol, ignore_flagged_channels, fname); check_not_null(all[i], "in load_uvdata");
    all[i]->flag_short_baselines(); all[i]->flag_partial_baselines();
#ifdef NOISE
#ifndef COHERENT
    all[i]->replace_with_noise(which_pol);	// only for incoherent integrating
#endif
#endif
    if ( do_interpolation ) all[i]->interpolate();
    else all[i]->deconvolve(window);
    if ( scrunch_num > 1 ) all[i]->do_scrunch(scrunch_num);
    num_channels = all[i]->num_channels;
    stringstream fcache; fcache << "vis_" << file_index << "_" << i << ".dat"; 
    all[i]->cache(fcache.str());
    //all[i]->report();
  }

  freeUVFITSdata(data);

}

int get_channels(char *fname) {
  uvdata *data;

  if ( readUVFITS(fname, &data) != 0 ) {
    cerr << "readUVFITS failed\n";
    exit(1);
  }

  return data->n_freq;
}


  //to do: free data
  // merging multiple times, do the clipping at the end. uvw changes
  // miriad changes frequency
		// chanles flagged for RFI

