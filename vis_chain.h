/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <complex>
using namespace std;

typedef complex<double> VisType;

class VisLink {
public:
  VisType c;
  float length;
  VisLink *next;
};


// Linked lists of visibilities. One list for each channel/baseline

class VisChain {
  VisLink ***vis;
  int num_channels, num_baselines;

public:
  void add(VisType c, float length, int ch, int bl);
  void report();
  void dump(char *name);
  VisChain(int num_ch, int num_bl);
  ~VisChain();
};
