power_spectrum: main.cc visibilities.cc visibilities.h times.h times.cc pspec_prep.cc window.cc grid.cc grid.h conversions.h conversions.cc power_spectrum.cc power_spectrum.h interpolate.cc flags.h flags.cc uvfits.o nfft_eq.o path_op.o
	g++ -DNONE -std=c++11    -g -Wunused -fomit-frame-pointer -malign-double -fstrict-aliasing -ffast-math -march=native -o power_spectrum main.cc uvfits.o  visibilities.cc times.cc pspec_prep.cc window.cc grid.cc conversions.cc power_spectrum.cc interpolate.cc flags.h flags.cc nfft_eq.o path_op.o str*.o star_*.o -lpal -lcfitsio -lfftw3 -L. -lgsl -lblas -lnfft3 -lpthread

noise_power_spectrum: main.cc visibilities.cc visibilities.h pspec_prep.cc window.cc grid.cc grid.h conversions.h conversions.cc power_spectrum.cc power_spectrum.h interpolate.cc flags.h flags.cc uvfits.o nfft_eq.o path_op.o
	g++ -DNOISE -std=c++11    -g -Wunused -fomit-frame-pointer -malign-double -fstrict-aliasing -ffast-math -march=native -o noise_power_spectrum main.cc uvfits.o  visibilities.cc pspec_prep.cc window.cc grid.cc conversions.cc power_spectrum.cc interpolate.cc flags.h flags.cc nfft_eq.o path_op.o str*.o star_*.o -lpal -lcfitsio -lfftw3 -L.  -lgsl -lblas -lnfft3 -lpthread

coherent_noise_power_spectrum: main.cc visibilities.cc visibilities.h window.cc grid.cc grid.h conversions.h conversions.cc power_spectrum.cc power_spectrum.h interpolate.cc flags.h flags.cc uvfits.o nfft_eq.o path_op.o sfactor.h
	g++ -DNOISE -DCOHERENT -std=c++11    -g -Wunused -fomit-frame-pointer -malign-double -fstrict-aliasing -ffast-math -march=native -o coherent_noise_power_spectrum main.cc uvfits.o  visibilities.cc pspec_prep.cc window.cc grid.cc conversions.cc power_spectrum.cc interpolate.cc flags.h flags.cc nfft_eq.o path_op.o str*.o star_*.o -lpal -lcfitsio -lfftw3 -L.  -lgsl -lblas -lnfft3 -lpthread

stitch:	stitch.cc  uvfits.o 
	g++ -g -Wunused -o stitch stitch.cc uvfits.o str*.o star_*.o -lcfitsio -L. -lpal

subtract_visibilities: subtract_visibilities.cc  uvfits.o
	g++ -g -Wunused -o subtract_visibilities subtract_visibilities.cc uvfits.o str*.o star_*.o -lcfitsio -L. -lpal


clear_vis: clear_vis.cc  flags.h flags.cc uvfits.o
	g++ -g -Wunused -o clear_vis clear_vis.cc flags.cc uvfits.o str*.o star_*.o -lcfitsio -L. -lpal

unstitch: unstitch.cc  uvfits.o
	g++ -g -o unstitch unstitch.cc uvfits.o str*.o star_*.o -lcfitsio -L. -lpal

noise_integration: noise_integration.cu
	nvcc -o noise_integration noise_integration.cu

remove_stands: remove_stands.cc uvfits.o
	g++ remove_stands.cc -o remove_stands uvfits.o str*.o star_*.o -lcfitsio -L. -lpal

noise_estimate: noise_estimate.cc vis_chain.cc visibilities.cc conversions.cc flags.cc uvfits.o path_op.o
	g++ -o noise_estimate -std=c++11 noise_estimate.cc vis_chain.cc visibilities.cc conversions.cc flags.cc path_op.o uvfits.o str*.o star_*.o -L. -lpal -lcfitsio -lgsl -lblas -lpthread

add: add.cc  uvfits.o
	g++ -g -o add add.cc uvfits.o -lcfitsio -L. -lpal

compare:	visibilities.cc visibilities.h compare.cc conversions.h conversions.cc flags.cc flags.h uvfits.o
	g++ -o compare compare.cc visibilities.cc conversions.cc flags.cc uvfits.o -lcfitsio -L. -lpal

weights: weights.cc visibilities.cc flags.cc conversions.cc uvfits.o flags.h visibilities.h conversions.h
	g++ -o weights weights.cc visibilities.cc flags.cc conversions.cc uvfits.o -lcfitsio -L. -lpal -lgsl -lblas

uvfits.o:	uvfits.c uvfits.h
	gcc -c uvfits.c

path_op.o: path_op.cc
	g++ -c path_op.cc
	

conversions: conversions.cc
	g++ -o conversions -DMAIN conversions.cc  -lm -lgsl -lblas
