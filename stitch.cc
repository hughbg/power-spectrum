/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <iostream>
#include <cstring>
#include "uvfits.h"

using namespace std;

static void check_not_null(void *a, const char *message) {
  if ( a == NULL ) {
    cerr << "Failed to get memory " << message << "\n";
    exit(1);
  }
}

#define NUM_POLS 4

template<typename T>
void bad(T v1, T v2, const char *what) {
  if ( v1 != v2 ) {
    cout << what << " aren't all the  same. " << v1 << " " << v2 << endl;
    exit(1);
  }
}


int main(int argc, char *argv[]) {
  int test_bl, test_ch_in, test_ch_out, num_frequencies, result_ch;
  float cent_freq;
  uvdata *data[argc-2];
  uvdata result;

  num_frequencies = 0;		
  for (int i=1; i<argc-1; ++i) {
    cout << "Loading " << argv[i] << endl;
    readUVFITS(argv[i], &data[i-1]);
    num_frequencies += data[i-1]->n_freq;
    cout << "Cent Freq " << (int)data[i-1]->cent_freq << " Num dumps " << data[i-1]->n_vis << endl;
    cent_freq += data[i-1]->cent_freq;
  }

  
  cent_freq /= argc-2;
  if ( num_frequencies%2 == 0 ) cent_freq += data[0]->freq_delta;
  cout << "New center frequency " << cent_freq << endl;

  // Test output for visual check
  test_bl = 2070; test_ch_in = 60;
  cout << "Test in\n";
  for (int j=0; j<5; ++j) {  
    int index = (0+data[argc-3]->n_pol*(test_ch_in+data[argc-3]->n_freq*(test_bl+j)))*2;
   
    cout << j << endl;
    cout << data[argc-3]->u[data[argc-3]->n_vis-1][j+test_bl] << " " << data[argc-3]->v[data[argc-3]->n_vis-1][j+test_bl] << " " << data[argc-3]->w[data[argc-3]->n_vis-1][j+test_bl] << " " << data[argc-3]->n_baselines[data[argc-3]->n_vis-1] << " " << data[argc-3]->baseline[data[argc-3]->n_vis-1][j+test_bl] << endl;
    cout << data[argc-3]->visdata[data[argc-3]->n_vis-1][index] << " "  << data[argc-3]->visdata[data[argc-3]->n_vis-1][index+1] << " " << data[argc-3]->weightdata[data[argc-3]->n_vis-1][index] << endl;
    
  }


  for (int i=1; i<argc-2; ++i) {
    bad(data[i]->n_vis, data[0]->n_vis, "n_vis");
    bad(data[i]->n_pol, data[0]->n_pol, "n_pol");
    bad(data[i]->pol_type, data[0]->pol_type, "pol_type");
    bad(data[i]->n_IF, data[0]->n_IF, "n_IF");
    bad(data[i]->n_vis, data[0]->n_vis, "n_vis");
    bad(data[i]->freq_delta, data[0]->freq_delta, "freq_delta");
    bad(data[i]->source->ra, data[0]->source->ra, "ra");
    bad(data[i]->source->dec, data[0]->source->dec, "dec");
    for (int j=0; j<data[i]->n_vis; ++j) {
      bad(data[i]->date[j], data[0]->date[j], "date");
      bad(data[i]->n_baselines[j], data[0]->n_baselines[j], "n_baselines");

      for (int k=0; k<data[i]->n_baselines[j]; ++k) {
        bad(data[i]->baseline[j][k], data[0]->baseline[j][k], "baseline codes"); 
      }
    }
    if ( data[i]->cent_freq != data[i-1]->cent_freq+2616000 ) {
      cerr << "Frequencies not ordered\n";
      exit(1);
    }
    
  }

  cout << "Stitching\n";

  // make result
  memcpy(&result, data[0], sizeof(uvdata));   // using data[0] preserves uvw at the lowest frequency

  result.n_freq = num_frequencies;
  result.cent_freq = cent_freq;
  result.source->ra /= 15;		// because of an issue with write
  result.weightdata = new float*[data[0]->n_vis]; check_not_null(result.weightdata, "in main");
  for (int i=0; i<data[0]->n_vis; ++i) { result.weightdata[i] = new float[result.n_pol*result.n_freq*result.n_baselines[i]]; check_not_null(result.weightdata[i], "in main"); }
  result.visdata = new float*[data[0]->n_vis]; check_not_null(result.visdata, "in main");
  for (int i=0; i<data[0]->n_vis; ++i) { result.visdata[i] = new float[result.n_pol*result.n_freq*result.n_baselines[i]*2]; check_not_null(result.visdata[i], "in main"); }

  result_ch = 0;
  for (int i=0; i<argc-2; ++i) {
    for (int ch=0; ch<data[i]->n_freq; ++ch) {
      for (int vis=0; vis<data[i]->n_vis; ++vis) 
        for (int bl=0; bl<data[i]->n_baselines[vis]; ++bl) 
        
          for (int pol=0; pol<NUM_POLS; ++pol) {
            result.weightdata[vis][bl*(data[i]->n_pol*result.n_freq) + result_ch*data[i]->n_pol + pol] =
			data[i]->weightdata[vis][bl*(data[i]->n_pol*data[i]->n_freq) + ch*data[i]->n_pol + pol]; 
            if ( result.weightdata[vis][bl*(data[i]->n_pol*result.n_freq) + result_ch*data[i]->n_pol + pol] == 0 ) 
	      result.weightdata[vis][bl*(data[i]->n_pol*result.n_freq) + result_ch*data[i]->n_pol + pol] = -9;
            result.visdata[vis][2*(pol+data[i]->n_pol*(result_ch+result.n_freq*bl))] =
			data[i]->visdata[vis][2*(pol+data[i]->n_pol*(ch+data[i]->n_freq*bl))];
            result.visdata[vis][2*(pol+data[i]->n_pol*(result_ch+result.n_freq*bl))+1] =
			data[i]->visdata[vis][2*(pol+data[i]->n_pol*(ch+data[i]->n_freq*bl))+1];
          }


      ++result_ch;
    }
    if ( i > 1 ) freeUVFITSdata(data[i]);
  }



  // Test output for visual check
  cout << "Test out\n";
  test_ch_out = num_frequencies-data[argc-3]->n_freq+test_ch_in; 
  for (int j=0; j<5; ++j) {  
    int index = (0+result.n_pol*(test_ch_out+result.n_freq*(test_bl+j)))*2;
   
    cout << j << endl;
    cout << result.u[result.n_vis-1][j+test_bl] << " " << result.v[result.n_vis-1][j+test_bl] << " " << result.w[result.n_vis-1][j+test_bl] << " " << result.n_baselines[result.n_vis-1] << " " << result.baseline[result.n_vis-1][j+test_bl] << endl;
    cout << result.visdata[result.n_vis-1][index] << " " << result.visdata[result.n_vis-1][index+1] << " " << result.weightdata[result.n_vis-1][index] << endl;
    
  }
 
  cout << "Writing " << argv[argc-1] << endl;
  writeUVFITS(argv[argc-1], &result);


  readUVFITS(argv[argc-1], &data[0]);

  cout << "Test read in\n";
  cout << "Cent Freq " << data[0]->cent_freq << " Num freq " << data[0]->n_freq << endl;
  for (int j=0; j<5; ++j) {  
    int index = (0+data[0]->n_pol*(test_ch_out+data[0]->n_freq*(test_bl+j)))*2;
   
    cout << j << endl;
    cout << data[0]->u[data[0]->n_vis-1][j+test_bl] << " " << data[0]->v[data[0]->n_vis-1][j+test_bl] << " " << data[0]->w[data[0]->n_vis-1][j+test_bl] << " " << data[0]->n_baselines[data[0]->n_vis-1] << " " << data[0]->baseline[data[0]->n_vis-1][j+test_bl] << endl;
    cout << data[0]->visdata[data[0]->n_vis-1][index] << " " << data[0]->visdata[data[0]->n_vis-1][index+1] << " " << data[0]->weightdata[data[0]->n_vis-1][index] << endl;
    
  }
}


  

