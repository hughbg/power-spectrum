/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <iostream>
#include <cstring>
#include "uvfits.h"

using namespace std;

#define NUM_POLS 4
#define N_CHAN 109

#define EVEN(x) x%2 == 0

static void check_not_null(void *a, const char *message) {
  if ( a == NULL ) {
    cerr << "Failed to get memory " << message << "\n";
    exit(1);
  }
}

int main(int argc, char *argv[]) {
  uvdata *data, new_uvdata;
  int num_subbands, result_ch;
  double bottom_chan_freq;
  char new_name[64];

  if ( argc != 2 ) {
    cerr << "Usage: unstitch <uvfits file name>\n";
    exit(1);
  }

  readUVFITS(argv[1], &data);

  if ( data->n_freq%N_CHAN != 0 ) {
    cerr << "Number of channels not multiple of " << N_CHAN << "\n";
    exit(1);
  }

  num_subbands = data->n_freq/N_CHAN;
  bottom_chan_freq = data->cent_freq-(data->freq_delta*(data->n_freq/2));
  if ( EVEN(data->n_freq) ) bottom_chan_freq += data->freq_delta/2;	// Assuming centre was in between channels

  memcpy(&new_uvdata, data, sizeof(uvdata)); 

  new_uvdata.n_freq = N_CHAN;
  new_uvdata.cent_freq = bottom_chan_freq+(N_CHAN/2)*data->freq_delta;
  new_uvdata.source->ra /= 15;		// because of an issue with write
  new_uvdata.weightdata = new float*[data->n_vis]; check_not_null(new_uvdata.weightdata, "in main");
  for (int i=0; i<data->n_vis; ++i) { new_uvdata.weightdata[i] = new float[new_uvdata.n_pol*new_uvdata.n_freq*new_uvdata.n_baselines[i]]; check_not_null(new_uvdata.weightdata[i], "in main"); }
  new_uvdata.visdata = new float*[data->n_vis]; check_not_null(new_uvdata.visdata, "in main");
  for (int i=0; i<data->n_vis; ++i) { new_uvdata.visdata[i] = new float[new_uvdata.n_pol*new_uvdata.n_freq*new_uvdata.n_baselines[i]*2]; check_not_null(new_uvdata.visdata[i], "in main"); }

  for (int ns=0; ns<num_subbands; ++ns) {		// Big loop over the sub-bands we want to generate
    result_ch = 0;
    new_uvdata.cent_freq = bottom_chan_freq+ns*N_CHAN*data->freq_delta+(N_CHAN/2)*data->freq_delta;
    for (int ch=ns*N_CHAN; ch<(ns+1)*N_CHAN; ++ch) {
      for (int vis=0; vis<data->n_vis; ++vis) 
        for (int bl=0; bl<data->n_baselines[vis]; ++bl) 
          for (int pol=0; pol<NUM_POLS; ++pol) {
            new_uvdata.weightdata[vis][bl*(data->n_pol*new_uvdata.n_freq) + result_ch*data->n_pol + pol] = data->weightdata[vis][bl*(data->n_pol*data->n_freq) + ch*data->n_pol + pol]; 
            if ( new_uvdata.weightdata[vis][bl*(data->n_pol*new_uvdata.n_freq) + result_ch*data->n_pol + pol] == 0 ) 
	      new_uvdata.weightdata[vis][bl*(data->n_pol*new_uvdata.n_freq) + result_ch*data->n_pol + pol] = -9;
            new_uvdata.visdata[vis][2*(pol+data->n_pol*(result_ch+new_uvdata.n_freq*bl))] = data->visdata[vis][2*(pol+data->n_pol*(ch+data->n_freq*bl))];
            new_uvdata.visdata[vis][2*(pol+data->n_pol*(result_ch+new_uvdata.n_freq*bl))+1] = data->visdata[vis][2*(pol+data->n_pol*(ch+data->n_freq*bl))+1];
          }
      ++result_ch;
    }

    // Generate new file name
    sprintf(new_name, "unstitch_%d.uvfits", ns);
    cout << "Writing " << new_name <<  endl;
    writeUVFITS(new_name, &new_uvdata);
    

  }



}
  


