/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include "grid.h"

extern void check_not_null(void*, const char*);

// Establish the grid, which has array dimensions dim1 and dim2, (0..dim1, 0..dim2) but physical dimensions
// that go from left->right on the first axis and bottom->top on the other axis.
// Assumes there is nothing in the grid already. Important routine.
void Grid::setup(int d1, int d2, double l, double r, double b, double t, int nrow_tags) {		// d1, d2, is grid size. left, right ... is the extent in physical co-ords
  if ( d1 < 0 || d2 < 0 ) {
    cerr <<  "Invalid dimension for grid " << d1 << " " << d2 << "\n";
    exit(1);
  }
  if ( r < l || t < b ) {
    cerr << "Invalid physical extent for grid " << left << " " << right << " " << bottom << " " << top << endl;
    exit(1);
  }


  grid = new complex<double>*[d1]; check_not_null(grid, "in grid setup");
  for (int i=0; i<d1; ++i) {
    grid[i] = new complex<double>[d2]; check_not_null(grid[i], "in grid setup");
    for (int j=0; j<d2; ++j) grid[i][j] = complex<double>(0, 0);
  }
  
  weights = new int*[d1]; check_not_null(weights, "in grid setup");
  for (int i=0; i<d1; ++i) {
    weights[i] = new int[d2]; check_not_null(weights[i], "in grid setup");
    for (int j=0; j<d2; ++j) weights[i][j] = 0;
  }
  
  if ( nrow_tags > 0 ) {
	  row_tags = new double*[nrow_tags]; check_not_null(row_tags, "in grid setup");
	  for (int i=0; i<nrow_tags; ++i) { 
		  row_tags[i] = new double[d1]; check_not_null(row_tags[i], "in grid setup");
		  for (int j=0; j<d1; ++j) row_tags[i][j] = 0;
	  }
	  num_row_tags = nrow_tags;
  }
  
  dim1 = d1; dim2 = d2;


  left = l;
  right = r;
  bottom = b;
  top = t;

  left2 = right2 = bottom2 = top2 = 0;
}

// Assignment operator must delete the existing content
Grid& Grid::operator=(const Grid& g) {
  clear();

  setup(g.dim1, g.dim2, g.left, g.right, g.bottom, g.top, g.num_row_tags); 	// allocates correct memory

  copy_grids(g);
}

// Copy constructor, does not delete the existing content
Grid::Grid(const Grid& g) { 

  setup(g.dim1, g.dim2, g.left, g.right, g.bottom, g.top, g.num_row_tags); 	// allocates correct memory

  copy_grids(g);
}


inline bool exists(string name) {
    return ( access( name.c_str(), F_OK ) != -1 );
}

// Delete all memeory and re-initialise
void Grid::clear() {
        if ( grid != NULL ) {
                for (int i=0; i<dim1; ++i) delete[] grid[i];
                delete[] grid;
        }
        if ( weights != NULL ) {
                for (int i=0; i<dim1; ++i) delete[] weights[i];
                delete[] weights;
        }
        if ( row_tags != NULL ) {
                for (int i=0; i<num_row_tags; ++i) delete[] row_tags[i];
                delete row_tags;
        }
        if ( cache_file.length() > 0 && exists(cache_file) ) remove(cache_file.c_str());

  dim1 = dim2 = 0;
  left = right = bottom = top = 0;
  left2 = right2 = bottom2 = top2 = 0;
  num_row_tags = 0;
  grid = NULL; row_tags = NULL; weights = NULL; cache_file = "";
}

// Copy the array inside a grid from one Grid to another Grid
void Grid::copy_grids(const Grid& g) {		// expects everything sized right already
  
  for (int i=0; i<dim1; ++i) {
    memcpy(grid[i], g.grid[i], dim2*sizeof(complex<double>));
    memcpy(weights[i], g.weights[i], dim2*sizeof(int));
  }
  
  for (int i=0; i<num_row_tags; ++i) { 
    memcpy(row_tags[i], g.row_tags[i], dim1*sizeof(double));
  }
}

// This is only for coherent averaging, because some summing has been done on the GPU
void Grid::reset_weights(int weight) {
  for (int i=0; i<dim1; ++i) {
    for (int j=0; j<dim2; ++j) weights[i][j] = weight;
  }
}

int Grid::sum_weights() {
  int sum=0;

 for (int i=0; i<dim1; ++i) {
    for (int j=0; j<dim2; ++j) { cout << weights[i][j] << endl; sum += weights[i][j];}
  }
  return sum;
}

void Grid::check() {
  cout << "Check ------------------\n";
  for (int i=0; i<dim1; ++i) {
    for (int j=0; j<2; ++j) 
      cout << grid[i][j] << " " << weights[i][j] << endl;
  }
  for (int i=0; i<num_row_tags; ++i)
    cout << row_tags[i][0] << endl;
}
  

// add an increment to a value in the grid
void Grid::inc_at(double x, double y, complex<double> value) {		// physical c-ords
  double i, j;
  if ( ! (left <= x && x < right && bottom <= y && y < top) ) {
    cerr << "Attempt to add value to grid outside its range\n";
    cerr << "Physical co-ords " << x << " " <<  y << " dimension " << dim1 << " " << dim2 << " extent " << left << " " << right << " " << bottom << " " << top << endl;
    exit(1);
  }
  physical2grid(x, y, i, j); 

  inc_grid_at(i, j, value);
}

// add an increment to a value in the grid
void Grid::inc_grid_at(double i, double j, complex<double> value) {		// array/integer c-ords
  int ii, jj;

  ii = (int)round(i); jj = (int)round(j);
  if ( ii == -1 ) ii = 0; if ( jj == -1 ) jj = 0;
  if ( ii == dim1 ) ii = dim1-1; if ( jj == dim2 ) jj = dim2-1;

  if ( ! (0 <= ii && ii < dim1 && 0 <= jj && jj < dim2) ) {
    cerr << "Attempt to add value to grid outside its range\n";
    cerr << "Grid co-ords " << i << " " <<  j << " dimension " << dim1 << " " << dim2 << " extent " << left << " " << right << " " << bottom << " " << top << endl;
    exit(1);
  }

  grid[ii][jj] += value;
  ++weights[ii][jj];
}

static bool equal(double f1, double f2) {
  return abs(f1-f2) < 1e-8;
}

// Check if other grid has the same dimensions and ranges
bool Grid::same_size(const Grid& g) {
  //cout << dim1 << " " << dim2 << " " << g.left << " " << g.right << " " << g.bottom << " " << g.top << endl;
  //cout << g.dim1 << " " << g.dim2 << " " << g.left << " " << g.right << " " << g.bottom << " "<< g.top << endl;
  return ( dim1 == g.dim1 && dim2 == g.dim2 && equal(left, g.left) && equal(right, g.right) && equal(bottom, g.bottom) && equal(top, g.top) );
}

// Get value using physical co-ordinates
complex<double> Grid::value_at(double x, double y) {		// physical co-ords
  double i, j;

  if ( ! (left <= x && x < right && bottom <= y && y < top) ) {
    cerr << "Attempt to get value in grid outside its range\n";
    cerr << "Physical co-ords " << x << " " <<  y << endl;
    exit(1);
  }  
  physical2grid(x, y, i, j);
  return value_at_grid(i, j);
}

// Get weight using physical co-ordinates
int Grid::weight_at(double x, double y) {            // physical co-ords
  double i, j;

  if ( ! (left <= x && x < right && bottom <= y && y < top) ) {
    cerr << "Attempt to get value in grid outside its range\n";
    cerr << "Physical co-ords " << x << " " <<  y << endl;
    exit(1);
  }
  physical2grid(x, y, i, j);
  return weight_at_grid(i, j);
}


// Get value using grid array co-ordinates
complex<double> Grid::value_at_grid(double i, double j) {		// grid co-ords
  int ii, jj;

  ii = (int)round(i); jj = (int)round(j);
  if ( ii == -1 ) ii = 0; if ( jj == -1 ) jj = 0;
  if ( ii == dim1 ) ii = dim1-1; if ( jj == dim2 ) jj = dim2-1;

  if ( ! (0 <= ii && ii < dim1 && 0 <= jj && jj < dim2) ) {
    cerr << "Attempt to get value in grid outside its range\n";
    cerr << "Grid co-ords " << i << " " <<  j << " dimension " << dim1 << " " << dim2 << " extent " << left << " " << right << " " << bottom << " " << top << endl;
    exit(1);
  }

  return grid[ii][jj];
}

// Get weight using grid co-ordinates
int Grid::weight_at_grid(double i, double j) {               // grid co-ords
  int ii, jj;

  ii = (int)round(i); jj = (int)round(j);
  if ( ii == -1 ) ii = 0; if ( jj == -1 ) jj = 0;
  if ( ii == dim1 ) ii = dim1-1; if ( jj == dim2 ) jj = dim2-1;

  if ( ! (0 <= ii && ii < dim1 && 0 <= jj && jj < dim2) ) {
    cerr << "Attempt to get value in grid outside its range\n";
    cerr << "Grid co-ords " << i << " " <<  j << " dimension " << dim1 << " " << dim2 << " extent " << left << " " << right << " " << bottom << " " << top << endl;
    exit(1);
  }

  return weights[ii][jj];
}

// Set value using physical co-ordinates
void Grid::set_at(double x, double y, complex<double> val) {             // physical co-ords
  double i, j;

  if ( ! (left <= x && x < right && bottom <= y && y < top) ) {
    cerr << "Attempt to set value in grid outside its range\n";
    cerr << "Physical co-ords " << x << " " <<  y << endl;
    exit(1);
  }  
  physical2grid(x, y, i, j);
  set_grid_at(i, j, val);
}

// Set value using physical co-ordinates
void Grid::set_row_tag(double x, int which, double val) {             // physical co-ords
  double y=0, i, j;

  if ( ! (left <= x && x < right ) ) {
    cerr << "Attempt to set row tag in grid outside its range\n";
    cerr << "Physical co-ord " << x << " in " << left << " " << right << endl;
    exit(1);
  }  
  physical2grid(x, y, i, j);
  row_tags[which][(int)round(i)] = val;
}

// Get value using physical co-ordinates
double Grid::get_row_tag(double x, int which) {             // physical co-ords
  double y=0, i, j;

  if ( ! (left <= x && x < right ) ) {
    cerr << "Attempt to set row tag in grid outside its range\n";
    cerr << "Physical co-ord " << x << " in " << left << " " << right << endl;
    exit(1);
  }  
  physical2grid(x, y, i, j);
  return row_tags[which][(int)round(i)];
}

void Grid::raise_row_tags(double *rt, int which) {		// Increase the values to match the incoming tags
  bool all_zero = true;
  for (int i=0; i<dim1; ++i)
    if ( row_tags[which][i] > 0 ) all_zero = false;
  if ( all_zero ) cerr << "All row_tags are zero in original grid in raise_row_tags\n";
  all_zero = true;
  for (int i=0; i<dim1; ++i)
    if ( row_tags[which][i] > 0 ) all_zero = false;
  if ( all_zero ) cerr << "All row_tags are zero in incoming grid in raise_row_tags\n";
 
  for (int i=0; i<dim1; ++i) 
    if ( rt[i] > row_tags[which][i] ) row_tags[which][i] = rt[i];
}

// Set value using grid/array co-ordinates
void Grid::set_grid_at(double i, double j, complex<double> val) {		// grid co-ords
  int ii, jj;

  ii = (int)round(i); jj = (int)round(j);
  if ( ii == -1 ) ii = 0; if ( jj == -1 ) jj = 0;
  if ( ii == dim1 ) ii = dim1-1; if ( jj == dim2 ) jj = dim2-1;

  if ( ! (0 <= ii && ii < dim1 && 0 <= jj && jj < dim2) ) {
    cerr << "Attempt to set value in grid outside its range\n";
    cerr << "Grid co-ords " << i << " " <<  j << " dimension " << dim1 << " " << dim2 << " extent " << left << " " << right << " " << bottom << " " << top << endl;
    exit(1);
  } 
 
  grid[ii][jj] = val;
  weights[ii][jj] = 1;
}

// translation routines to go from physical to grid co-ordinates and vice-versa
void Grid::physical2grid(double x, double y, double& i, double& j) {
  i = dim1*(double)(x-left)/(double)(right-left);
  j = dim2*(double)(y-bottom)/(double)(top-bottom);
}

void Grid::x_physical2grid(double x, double& j) {
  j = dim1*(double)(x-left)/(double)(right-left);
}

void Grid::y_physical2grid(double y, double& j) {
  j = dim2*(double)(y-bottom)/(double)(top-bottom);
}

void Grid::physical2grid_window2(double x, double y, double& i, double& j) {
  i = dim1*(double)(x-left2)/(double)(right2-left2);
  j = dim2*(double)(y-bottom2)/(double)(top2-bottom2);
}

void Grid::y_physical2grid_window2(double y, double& j) {
  j = dim2*(double)(y-bottom2)/(double)(top2-bottom2);
}

void Grid::grid2physical(double i, double j, double& x, double& y) {   
  x = (right-left)*((double)i/(double)dim1)+left;
  y = (top-bottom)*((double)j/(double)dim2)+bottom;
}

void Grid::grid2physical_window2(double i, double j, double& x, double& y) {   
  x = (right2-left2)*((double)i/(double)dim1)+left2;
  y = (top2-bottom2)*((double)j/(double)dim2)+bottom2;
}

void Grid::x_grid2physical(double i, double& x) {
  x = (right-left)*((double)i/(double)dim1)+left;
}

void Grid::y_grid2physical(double j, double& y) {   
  y = (top-bottom)*((double)j/(double)dim2)+bottom;
}

void Grid::y_grid2physical_window2(double j, double& y) {
  y = (top2-bottom2)*((double)j/(double)dim2)+bottom2;
}

// ----------

// The grid doesn't change, but the physical co-oordinates represented by it can change
void Grid::set_new_extent(double l, double r, double b, double t) {
  left = l;
  right = r;
  bottom = b;
  top = t;
}

void Grid::set_window2_extent(double l, double r, double b, double t) {
  left2 = l;
  right2 = r;
  bottom2 = b;
  top2 = t;
}

// Empty
bool Grid::all_zero() {
  for (int i=0; i<dim1; ++i) 
    for (int j=0; j<dim2; ++j)
      if ( abs(grid[i][j]) != 0 ) return false;
  return true;
}

// Get maximum absolute value
double Grid::max() {
  double m = 0;
  for (int i=0; i<dim1; ++i)
    for (int j=0; j<dim2; ++j) {
      if ( abs(grid[i][j]) > m ) m = abs(grid[i][j]);
    }
  return m;
}

// Get minimum absolute value
double Grid::min() {
  double m = max();
  for (int i=0; i<dim1; ++i)
    for (int j=0; j<dim2; ++j) {
      if ( abs(grid[i][j]) < m ) m = abs(grid[i][j]);
    }
  return m;
}



// Apply weights from another grid that must have the say array dimensions
void Grid::apply_weights() {
  cout << "Applying weights to grid\n";
  for (int i=0; i<dim1; ++i)
    for (int j=0; j<dim2; ++j) 
        if ( abs(weights[i][j]) != 0 ) { 
        	grid[i][j] /= weights[i][j];
        	weights[i][j] = 1;
        }
}

// Clip a box out of the array. The range is specified in array co-ordinates not physical.
// The phsyical co-ordinates are adjusted based on the ranges of the new box.
void Grid::clip_grid(int il, int ir, int jb, int jt) {			// il ... is in grid co-ords
  double new_left, new_right, new_bottom, new_top;
  double new_left2, new_right2, new_bottom2, new_top2;
  complex<double> **new_grid;
  int **new_weights;
  double *tmp;

  if ( ir <= il || jt <= jb || il < 0 || il > dim1 || jb < 0 || jt > dim2 ) {
    cerr << "Invalid range for clipping " << il << " to " << ir << ", " << jb << " to " << jt << "\n";
    cerr << "Dimensions are " << left << " " << right << " " << bottom << " " << top << endl;
    exit(1);
  }   

  // First get new phys extent
  grid2physical(il, jb, new_left, new_bottom);
  grid2physical(ir, jt, new_right, new_top);
  grid2physical_window2(il, jb, new_left2, new_bottom2);
  grid2physical_window2(ir, jt, new_right2, new_top2);


  dim1 = ir-il;
  dim2 = jt-jb;
  
  // Copy to new grid
  new_grid = new complex<double>*[dim1]; check_not_null(new_grid, "in clip_grid");
  for (int i=0; i<dim1; ++i) { 
    new_grid[i] = new complex<double>[dim2]; check_not_null(new_grid[i], "in clip grid");
  }

  for (int i=il; i<ir; ++i)
    for (int j=jb; j<jt; ++j) new_grid[i-il][j-jb] = grid[i][j];

  for (int i=0; i<dim1; ++i) delete[] grid[i];
  delete[] grid;
  grid = new_grid;
  
  new_weights = new int*[dim1]; check_not_null(new_weights, "in clip grid");
  for (int i=0; i<dim1; ++i) { new_weights[i] = new int[dim2]; check_not_null(new_weights[i], "in clip grid"); }

  for (int i=il; i<ir; ++i)
    for (int j=jb; j<jt; ++j) new_weights[i-il][j-jb] = weights[i][j];

  for (int i=0; i<dim1; ++i) delete[] weights[i];
  delete[] weights;
  weights = new_weights;
  
  for (int i=0; i<num_row_tags; ++i) { 
	  tmp = new double[dim1]; check_not_null(tmp, "in clip_grid");
	  for (int j=il; j<ir; ++j) tmp[j-il] = row_tags[i][j];
	  delete[] row_tags[i];
	  row_tags[i] = tmp;
  }
  
  
  left = new_left;
  right = new_right;
  bottom = new_bottom;
  top = new_top;

  left2 = new_left2;
  right2 = new_right2;
  bottom2 = new_bottom2;
  top2 = new_top2;
}

// Clip based on physical co-ordinates
void Grid::clip(double l, double r, double b, double t) {
  double il, ir, jb, jt;
  int iil, iir, jjb, jjt;

  physical2grid(l, b, il, jb);
  physical2grid(r, t, ir, jt);

  iil = (int)round(il); if ( iil < 0 ) iil = 0; if ( iil >= dim1 ) iil = dim1-1;
  iir = (int)round(ir); if ( iir < 0 ) iir = 0; if ( iir >= dim1 ) iir = dim1-1;
  jjb = (int)round(jb); if ( jjb < 0 ) iil = 0; if ( jjb >= dim2 ) jjb = dim2-1;
  jjt = (int)round(jt); if ( jjt < 0 ) iir = 0; if ( jjt >= dim2 ) jjt = dim2-1;
  
  clip_grid(iil, iir, jjb, jjt);
}

// Bounding box
void Grid::bbox(int& il, int& ir, int& jb, int& jt) {
  il = dim1; jb = dim2;
  ir = 0; jt = 0;
  
  for (int i=0; i<dim1; ++i)
    for (int j=0; j<dim2; ++j)
      if ( abs(grid[i][j]) != 0 ) {
        if ( i < il ) il = i;
        if ( i > ir ) ir = i;
        if ( j < jb ) jb = j;
        if ( j > jt ) jt = j;
      }
  //cout << il << " " << ir << " " << jb << " " << jt << endl;
}

void Grid::gabs() {
  for (int i=0; i<dim1; ++i)
    for (int j=0; j<dim2; ++j) {
      grid[i][j] = complex<double>(abs(grid[i][j]), 0);
    }
}

// Clip out edges where there is no data, remove any border.
void Grid::auto_clip() {		
  int left, right, bottom, top;
  bbox(left, right, bottom, top);
   
  clip_grid(left, right+1, bottom, top+1);
}

void Grid::flip() {
  Grid new_grid(dim1, dim2, left, right, bottom, top, 0);

  for (int i=0; i<dim1; ++i)
    for (int j=0; j<dim2; ++j) 
      new_grid.set_grid_at(i, dim2-j-1, grid[i][j]); 

  for (int i=0; i<dim1; ++i)
    for (int j=0; j<dim2; ++j)
      set_grid_at(i, j, new_grid.grid[i][j]);       
}


void Grid::save_grid(const char *fname) {
  double right_coord, top_coord, junk;
  ofstream out;
  out.open(fname);
  // Here we dump the extent for matplotlib which is slightly different to the meaning of extent
  // here. Instead of "right" we want the physical co-ordinate of the last data element. Same for top.
  
  grid2physical(dim1-1, 0, right_coord, junk);
  grid2physical(0, dim2-1, junk, top_coord);
  out << dim1 << " "<< dim2 << " " << left << " " << right_coord << " " << bottom << " " << top_coord << endl;
  for (int i=0; i<dim1; ++i) {
    for (int j=0; j<dim2; ++j) {
      // Dump complex numbers in Python format
      if ( grid[i][j].imag() < 0 )   // negative sign printed automatically
        out << "(" << grid[i][j].real() << grid[i][j].imag() << "j) ";
      else
        out << "(" << grid[i][j].real() << "+" << grid[i][j].imag() << "j) ";
    }
    out << endl;
  }
  out.close();
}

void Grid::save_weights(const char *fname) {
  ofstream out;
  out.open(fname);

  for (int i=0; i<dim1; ++i) {
    for (int j=0; j<dim2; ++j) out << weights[i][j] << " ";
    out << endl;
  }
  out.close();
}

void Grid::cache(string fname) {

  if ( fname.length() == 0 and cache_file.length() == 0) {
    cerr << "cache() passed empty file name\n";
    exit(1);
  }

  if ( fname.length() == 0 ) fname = cache_file;

  ofstream out;
  out.open(fname.c_str(), ios::out | ios::binary);
 
  if ( !out ) {
    cerr << "Failed to open cache file: " << fname << endl;
    exit(1);
  }

  for (int i=0; i<dim1; ++i) {
    out.write((const char*)grid[i], dim2*sizeof(complex<double>));
    delete[] grid[i];
  }
  out.close();

  cache_file = fname;

  delete[] grid;
  grid = NULL;
}

void Grid::uncache() {
  ifstream in;

  if ( cache_file.length() == 0 ) {
    cerr << "uncache() has an empty file name\n";
    exit(1);
  }

  if ( grid != NULL ) {
    cerr << "Attempt to uncache a power spectrum not cached (grid!=NULL)\n";
    exit(1);
  }

  in.open(cache_file.c_str(), ios::in | ios::binary);

  if ( !in ) {
    cerr << "Failed to open cache file in uncache(): " << cache_file << endl;
    exit(1);
  }

  grid = new complex<double>*[dim1]; check_not_null(grid, "in uncache");
  for (int i=0; i<dim1; ++i) {
    grid[i] = new complex<double>[dim2]; check_not_null(grid[i], "in uncache");
    in.read((char*)grid[i], dim2*sizeof(complex<double>));
  }
  in.close();
  remove(cache_file.c_str());
}



void Grid::print() {
  for (int i=0; i<dim1; ++i) {
    for (int j=0; j<dim2; ++j) 
      cout << grid[i][j] << " ";
    cout << endl;
  }
}

void Grid::print_row_tags() {
	for (int i=0; i<num_row_tags; ++i) {
		for (int j=0; j<dim1; ++j) cout << row_tags[i][j] << " ";
		cout << endl;
	}
}

void Grid::print_sizes() {
  cout << "Grid dim" << " " << dim1 << " " << dim2 << endl;
  cout << "Extent+1" << " " << left << " " << right << " " << bottom << " " << top << " ("  << left2 << " " << right2 << " " << bottom2 << " " << top2 << ")" << endl;
  cout << "Max " << max() << endl;
}

/*
int main(int argc, char *argv[]) {
  Grid g1, g2;
  g1.setup(10, 10, 0, 9, 0, 9, 2);
  g2 = g1;
} */

