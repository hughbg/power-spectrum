/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <iostream>
#include <cstring>
#include "flags.h"
#include "uvfits.h"

using namespace std;

#define NUM_POLS 4


int main(int argc, char *argv[]) {
  int test_bl, test_ch_in;
  uvdata *data;
  Flags flags;

  if ( argc != 3 ) {
    cerr << "Invalid args\n";
    return 1;
  }

  flags.load_vis_flags();
  readUVFITS(argv[1], &data);

  cout << "Test in\n";
  test_bl = 2070; test_ch_in = 60;
  for (int j=0; j<5; ++j) {
    int index = (0+data->n_pol*(test_ch_in+data->n_freq*(test_bl+j)))*2;

    cout << j << endl;
    cout << data->u[data->n_vis-1][j+test_bl] << " " << data->v[data->n_vis-1][j+test_bl] << " " << data->w[data->n_vis-1][j+test_bl] << " " << data->n_baselines[data->n_vis-1] << " " << data->baseline[data->n_vis-1][j+test_bl] << endl;
    cout << data->visdata[data->n_vis-1][index] << " "  << data->visdata[data->n_vis-1][index+1] << " " << data->weightdata[data->n_vis-1][index] << endl;
  }

  
  data->source->ra /= 15;		// because of an issue with write
  data->cent_freq += 12000;		// another problem - half channel shift

  for (int ch=0; ch<data->n_freq; ++ch) {
    for (int vis=0; vis<data->n_vis; ++vis) 
      for (int bl=0; bl<data->n_baselines[vis]; ++bl) {
 	int st1, st2;
        bool flag_it;
        DecodeBaseline(data->baseline[vis][bl], &st1, &st2); --st1; --st2;

        flag_it = ( flags.in(flags.baseline_flags, st1, st2) ||
          flags.in(flags.channel_flags, ch) || flags.in(flags.rfi_channel_flags, ch, vis) ||
          flags.in(flags.stand_flags, st1) || flags.in(flags.stand_flags, st2) );

        for (int pol=0; pol<NUM_POLS; ++pol) {
  	    if ( flag_it ) data->weightdata[vis][bl*(data->n_pol*data->n_freq) + ch*data->n_pol + pol] = -9;
            else data->weightdata[vis][bl*(data->n_pol*data->n_freq) + ch*data->n_pol + pol] = 9;
	    // Make sure not 0 or Nan
	    data->visdata[vis][2*(pol+data->n_pol*(ch+data->n_freq*bl))] = 1;
  	    data->visdata[vis][2*(pol+data->n_pol*(ch+data->n_freq*bl))+1] = 1;
        }
      }
  }
 
  writeUVFITS(argv[2], data);
  freeUVFITSdata(data);

  readUVFITS(argv[2], &data);

  cout << "Test read in\n";
  cout << "Cent Freq " << data->cent_freq << " Num freq " << data->n_freq << endl;
  for (int j=0; j<5; ++j) {
    int index = (0+data->n_pol*(test_ch_in+data->n_freq*(test_bl+j)))*2;

    cout << j << endl;
    cout << data->u[data->n_vis-1][j+test_bl] << " " << data->v[data->n_vis-1][j+test_bl] << " " << data->w[data->n_vis-1][j+test_bl] << " " << data->n_baselines[data->n_vis-1] << " " << data->baseline[data->n_vis-1][j+test_bl] << endl;
    cout << data->visdata[data->n_vis-1][index] << " "  << data->visdata[data->n_vis-1][index+1] << " " << data->weightdata[data->n_vis-1][index] << endl;
  }
}


  

