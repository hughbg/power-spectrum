/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Convert TRIANGULAR DADA format to UVfits.

  triang2uvfits64 /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/header.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/antenna_locations.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/instr_config.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/2015-04-08-20_15_03_0001133593833216.dada x.uvfits
*/

#include <stdio.h>
#include <unistd.h>
#include <cstring>
#include <sstream>
#include <iostream>
#include "uvfits.h"

// get uvfits.h and uvfits.c from cuwarp. Convert SLA calls to PAL. Change "station" to stand. Make writeUVFITS extern "C"
      

extern "C" {
  int writeUVFITS(char *filename, uvdata *data);
  int readUVFITS(char *filename, uvdata **data);
  char *strip_file_name(char*);
}

const int NUM_BASELINES64 = 31125; //2080;	no autocorrelations
const int NUM64_STANDS = 250; //64;
const int NUM256_STANDS = 256;


static void usage() {
  fprintf(stderr, "Usage: split <input uvfits>\n");
}

int main(int argc, char *argv[]) {
  uvdata *data;
  int *n_baselines, n_vis;
  double **u, **v, **w, *date, ra;
  float **visdata, **weightdata;
  float **baseline;
  int i, bl_index256, bl_index64;

  if ( argc != 2 ) { usage(); return 1; }

  if ( access(argv[1],R_OK) == -1 ) {
    fprintf(stderr,"Failed to find %s\n",argv[i]);
    return 1;
  }
  
  
  readUVFITS(argv[1], &data);
  
  n_vis = data->n_vis; data->n_vis = 1;
  n_baselines = data->n_baselines; date = data->date;
  u = data->u; v = data->v; w = data->w;
  baseline = data->baseline;
  visdata = data->visdata; weightdata = data->weightdata;
  ra = data->source->ra;
  for (int i=0; i<n_vis; ++i) {
    std::stringstream out; out << strip_file_name(argv[1]) << "_" << i << ".uvfits";
    data->n_baselines = n_baselines+i; data->date = date+i;
    data->u = u+i; data->v = v+i; data->w = w+i;
    data->baseline = baseline+i;
    data->visdata = visdata+i; data->weightdata = weightdata+i;
    data->source->ra = ra+(9.0*i)/(24*60*60)*360;
    data->source->ra /= 15;
    char outs[1024];
    strcpy(outs, out.str().c_str());
    writeUVFITS(outs, data);
  }

}
