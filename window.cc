#include <cstring>
#include <cmath>
#include "window_vals.h"

#include <iostream>
#include <cstdlib>
void make_window(double* window_data, const char *window, int length) {
  /* For Dolph-chebyshev, values in window_vals.h
  if ( ! (length == 109 || length == 436 ) ) {
    std::cerr << "Invalid window length " << length << std::endl;
    exit(1);
  }
  if ( length == 109 ) 
    for (int i=0; i<109; ++i) window_data[i] = dc_109[i];
  if ( length == 436 )
    for (int i=0; i<436; ++i) window_data[i] = dc_436[i];
  return;
  */

  if ( strcmp(window, "hamming") == 0 )
    for (int i=0; i<length; ++i)
      window_data[i] = 0.54-0.46*cos((2*M_PI*i)/(length));
  else if ( strcmp(window, "blackmanharris") == 0 || strcmp(window, "blackmanharris-sqr") == 0 ) {
    for (int i=0; i<length; ++i)
      window_data[i] = 0.35875-0.48829*cos(2*M_PI*i/(length+1))+
                0.14128*cos(4*M_PI*i/(length+1))-0.01168*cos(6*M_PI*i/(length));
    if ( strcmp(window, "blackmanharris-sqr") == 0 )
      for (int i=0; i<length; ++i)
        window_data[i] *= window_data[i];
  } else if ( strcmp(window, "none") == 0 )
    for (int i=0; i<length; ++i)
      window_data[i] = 1;
}

#ifdef TEST
#include <iostream>
using namespace std;
int main() {
  double *window_data = new double[109];
  make_window(window_data, "blackmanharris", 109);
  double bw = 0;
  for (int i=0; i<109; ++i) bw += window_data[i]*window_data[i]*4*24e3;
  cout << bw << endl;
}
#endif
