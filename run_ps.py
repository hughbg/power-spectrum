import os, sys, subprocess, getopt

def stitch(files, output):
  print "stitch", files

  command = "python stitch_calibrated.py"
  for f in files:
    command += " "+f

  command += " "+output
  print "\n>>>>", command
  os.system(command)

num_files_in_time = 0
num_files_in_frequency = 0

def run_isolated_cuwarp(dname, dada_file):
  os.system("mkdir "+dname)
  os.system("cp -r run_one.py flagged_tiles.txt flagged_baselines.txt flagged_channels.txt bandpass_stats.py  antenna_locations.txt VLSScatalog07Jun26_10JyCutoff.txt combined.txt instr_config.txt rts_node_cpu dipole_files cuwarp.in lib*so* "+dname)
  os.chdir(dname)
  f = open("commands", "w")
  f.write(" ../hdf52dada "+dada_file+" x.dada\n")
  f.write("python run_one.py x.dada\n")
  f.close()
  p = subprocess.Popen([ "sh", "commands" ])  
  os.chdir("..")
  return p

def run_isolated_pspec_prep(dname, fits_file, pp_fits_file):
  os.system("mkdir "+dname)
  os.system("cp rts2aipy.py antenna_locations.txt "+dname)
  os.chdir(dname)
  f = open("commands", "w")
  command = "/home/leda/hgarsden/miriad/linux64/bin/fits op=uvin in="+"../"+fits_file+" out=leda.uv"
  f.write("echo "+command+"\n")
  f.write(command+"\n")
  command = "python ../pspec_prep.py leda.uv --horizon="+str(horizon)+" --nophs --nogain --window='blackman-harris' --clean="+str(threshold)+"  -C rts2aipy --model"
  f.write("echo "+command+"\n")
  f.write(command+"\n")
  command = "/home/leda/hgarsden//miriad/linux64/bin/fits op=uvout in=leda.uvF out="+pp_fits_file
  f.write("echo "+command+"\n")
  f.write(command+"\n")
  command = "python ../add_object.py "+pp_fits_file
  f.write("echo "+command+"\n")
  f.write(command+"\n")
  f.close()
  p = subprocess.Popen([ "sh", "commands" ])
  os.chdir("..")
  return p

try:
  opts, args = getopt.getopt(sys.argv[1:], "h:t:")
except getopt.GetoptError as err:
  print str(err)  

if len(args) > 0:
  print "Non-option arguments are ignored"

horizon = 50
threshold = 1e-6
for o in opts:
  if o[0] == "-h": horizon = int(o[1])
  if o[0] == "-t": threshold = int(o[1])
print "Horizon", horizon, "CLEAN threshold", threshold

time_files = []
with open("file_list.txt") as fl:
  line = fl.readline()
  l = line.split()
  num_files_in_time = int(l[0])
  num_files_in_frequency = int(l[1])
  print "Got", str(num_files_in_time)+"x"+str(num_files_in_frequency), "files"

  time_index = 0
  for nt in range(num_files_in_time):
    processes = [ None for i in range(num_files_in_frequency) ]
    for nf in range(num_files_in_frequency):
       file_name = fl.readline()[:-1]
       print file_name
       processes[nf] = run_isolated_cuwarp("frequency"+str(nf), file_name)
    
    for p in processes: p.wait()

    print "\n>>>>>>>>>>> Cal finished time", nt, "\n"

    files = [ [] for i in range(num_files_in_frequency) ]
    for nf in range(num_files_in_frequency):
       os.rename("frequency"+str(nf)+"/uvdump_01.uvfits", "uvdump_"+str(nt)+"_"+str(nf)+".uvfits")
       scan = 0
       ti = time_index
       while os.path.exists("frequency"+str(nf)+"/calibrated_"+str(scan)+".dat"):
         os.rename("frequency"+str(nf)+"/calibrated_"+str(scan)+".dat", "calibrated_"+str(ti)+"_"+str(nf+1)+".dat")
         print "frequency"+str(nf)+"/calibrated_"+str(scan)+".dat", "->", "calibrated_"+str(ti)+"_"+str(nf+1)+".dat"
         files[nf].append("calibrated_"+str(ti)+"_"+str(nf+1)+".dat")

         ti += 1

	 scan += 1
    
       print "rm -rf frequency"+str(nf)
       os.system("rm -rf frequency"+str(nf))

    # Given one time step but stitched over frequency
    for i in range(len(files[0])):
      flist = []
      for j in range(len(files)):
        flist.append(files[j][i])
      num = int(flist[0].split("_")[1])
      stitch(flist, "calibrated_t"+str(num)+".dat")
      time_files.append("calibrated_t"+str(num)+".dat")      

    time_index = ti
    
command = "power-spectrum-orig/power_spectrum"
for i in range(len(time_files)):
  command += " "+time_files[i]

print "\n>>>>", command
os.system(command)

os.system("mv power_spectrum.dat power_spectrum_orig.dat")

# UV Fits
print "Stitch UV"
for nt in range(num_files_in_time):
  command = "power-spectrum/stitch "
  for nf in range(num_files_in_frequency):
    command += " uvdump_"+str(nt)+"_"+str(nf)+".uvfits"
  command += " uvdump_t"+str(nt)+".uvfits"
  print "\n>>>>", command
  os.system(command)


command = "power-spectrum/power_spectrum "
for nt in range(num_files_in_time):
  command += " uvdump_t"+str(nt)+".uvfits"
print "\n>>>>", command
os.system(command)
os.system("mv power_spectrum.dat power_spectrum_uv.dat")

os.system("rm rfi_flagged_tiles.txt flagged_channels.txt")

# Do pspec_prep
print "PSPEC"
processes = [ None for i in range(num_files_in_time) ]
for nt in range(num_files_in_time):
  processes[nt] = run_isolated_pspec_prep("time"+str(nt), "uvdump_t"+str(nt)+".uvfits", "uvdump_t"+str(nt)+"_pp.uvfits")
  
for nt in range(num_files_in_time):
  processes[nt].wait()

command = "power-spectrum/power_spectrum "
for nt in range(num_files_in_time):
  command += " time"+str(nt)+"/uvdump_t"+str(nt)+"_pp.uvfits"
print "\n>>>>", command
os.system(command)


os.system("mv power_spectrum.dat power_spectrum_pp.dat")

for nt in range(num_files_in_time):
  os.system("mv "+"time"+str(nt)+"/uvdump_t"+str(nt)+"_pp.uvfits .")
  os.system("rm -rf time"+str(nt))
