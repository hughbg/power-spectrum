/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <complex>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include "vis_chain.h"

extern string strip_file_name(char *);

VisChain::VisChain(int num_ch, int num_bl) {
  if ( (vis = new VisLink**[num_ch]) == NULL ) {
    cerr << "Failed to get memory for VisChain\n";
    exit(1);
  }
  for (int ch=0; ch<num_ch; ++ch) 
    if ( (vis[ch] = new VisLink*[num_bl]) == NULL ) {
      cerr << "Failed to get memory for VisChain\n";
      exit(1);
    }
  
  for (int ch=0; ch<num_ch; ++ch) 
    for (int bl=0; bl<num_bl; ++bl) 
      vis[ch][bl] = NULL; 
  num_channels = num_ch; num_baselines = num_bl;
}

VisChain::~VisChain() {
  for (int ch=0; ch<num_channels; ++ch) 
    for (int bl=0; bl<num_baselines; ++bl) 
      if ( vis[ch][bl] != NULL ) {
        VisLink *p, *next;
        p = vis[ch][bl];

        do {
          next = p->next;
          delete p;
          p = next;
        } while ( p != NULL );
       
   
        vis[ch][bl] = NULL;
      }
}

void VisChain::report() {
  for (int ch=0; ch<num_channels; ++ch) 
    for (int bl=0; bl<num_baselines; ++bl) {
      VisLink *p = vis[ch][bl];
      int count = 0;
      while ( p != NULL ) {
        ++count;
        p = p->next;
      }
      cout << ch << " " << bl << " " << count << endl;
    }
} 

void VisChain::dump(char *name) {
  stringstream intermediate;
  intermediate << strip_file_name(name) << "_noise.dat";
  ofstream outfile(intermediate.str());
  for (int ch=0; ch<num_channels; ++ch) 
    for (int bl=0; bl<num_baselines; ++bl) {
      VisLink *p = vis[ch][bl];
      int count = 0;
      if ( p != NULL ) {
        outfile << ch << " " << bl << " " << p->length << " ";
        while ( p != NULL ) {
          outfile << p->c << " ";
          p = p->next;
        }
        outfile << endl;
      }
    }
  outfile.close();
} 


void VisChain::add(VisType c, float length, int ch, int bl) {
  if ( vis[ch][bl] == NULL ) {
    vis[ch][bl] = new VisLink;
    if ( vis[ch][bl] == NULL ) {
      cerr << "Failed to get memory for vis in VisChain::add\n";
      exit(1);
    }
    vis[ch][bl]->c = c; vis[ch][bl]->length = length; vis[ch][bl]->next = NULL;
  } else {
    VisLink *p = vis[ch][bl];
    while ( p->next != NULL ) p = p->next;
    p->next = new VisLink;
    if ( p->next == NULL ) {
      cerr << "Failed to get memory for vis link in VisChain::add\n";
      exit(1);
    }

    p->next->c = c; p->next->length = length;
    p->next->next = NULL;
  }
}

/*
int main() {
  VisChain vc(10, 10);
  vc.add(0, 5,2); vc.add(0, 5,2); 
  vc.add(1, 6, 5); vc.report();
}*/

