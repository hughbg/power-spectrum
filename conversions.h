/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <gsl/gsl_integration.h>
//Link with  -lm -lgsl -lblas

#define GHz(f) ((double)f/1e9)

class Conversions {
private:
	double D_H;
	gsl_integration_workspace *w;
	
public:
    Conversions();
    ~Conversions();
	double f2z(double fq /*GHz*/);
	double z2f(double z);
	double dL_dth(double z);
	double dL_df(double z, double omega_m=0.266);
	double dk_du(double z);
	double dk_deta(double z);
	double k_par(double f1 /*GHz*/, int num, double chan_width /*GHz*/);
	double F_X2Y(double z);
	double F_X(double z);
	double F_Y(double z);

	double X(double f /*GHz*/);
	double Y(double f /*GHz*/, double chan_width /*GHz*/);
	double X2Y(double f/*GHz*/, double chan_width/*GHz*/);
	double absolute_distance(double z);			// Hogg
	double uv2distance(double uv, double z);
	double channel2distance(double f /*GHz*/, double chan_width/*GHz*/);
	double k_perp(double uv_length, double z);
	double uv_length(double k_perp, double z);

};
