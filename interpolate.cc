/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <complex>
#include <vector>
#include <fftw3.h>
#include "spline.h"
#include <iostream>
using namespace std;

#define REAL 0
#define IMAG 1


void interpolate(vector<complex<double> >& array) {
   vector<double> X, Yr, Yi;

   for (int i=0; i<array.size(); ++i) {
     if ( abs(array[i]) != 0 ) {
       X.push_back(i);
       Yr.push_back(array[i].real());   // real
       Yi.push_back(array[i].imag());   // imag
     }
   }
 
   if ( X.size() == array.size() ) return;     // no zeros
    
   tk::spline sr;
   sr.set_points(X,Yr);    // currently it is required that X is already sorted

   tk::spline si;
   si.set_points(X,Yi);    

   for (int i=0; i<array.size(); ++i) 
     if ( abs(array[i]) == 0 ) {
       array[i] = complex<double>(sr(i), si(i));
     }

   //for (int i=0; i<len; ++i) cout << array[i]<<endl;
}
