/*

    Copyright (C) 2019  Hugh Garsden

    This file is part of Power Spectrum, a program for generating
    cosmological 21cm power spectra of the wedge kind.

    Power Spectrum is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Power Spectrum is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Power Spectrum.  If not, see <https://www.gnu.org/licenses/>.

*/
#include <unistd.h>
#include <cstring>
#include <fenv.h>
#include <iostream>
#include <vector>
#include <thread>
#include "power_spectrum.h"
#include "flags.h"

extern string strip_file_name(char*);
extern void check_not_null(void *, const char *);
#define MAX_DELAY 1500
#define MAX_DELAY_BIG 5100
#define TIME_SMEARING_LIMIT 36

#ifdef NOISE
#ifdef COHERENT
#define INTERMEDIATE "coherent_noise_power_spectrum_"
#define OUT "coherent_noise_power_spectrum.dat"
#define OUT_BIG "coherent_noise_power_spectrum_big.dat"
#else
#define INTERMEDIATE "noise_power_spectrum_"
#define OUT "noise_power_spectrum.dat"
#define OUT_BIG "noise_power_spectrum_big.dat"
#endif
#else
#define INTERMEDIATE "power_spectrum_"
#define OUT "power_spectrum.dat"
#define OUT_BIG "power_spectrum_big.dat"
#endif

void usage() {
  cerr << "power_spectrum [ options ] uvfits_file ...\n";
  cerr << "Options:\n";
  cerr << "	-s:		separate positive and negative sides of the FFT (default: false)\n";
  cerr << "	-i:		interpolate flagged channels using a spline function (default: false)\n";
  cerr << "     -d:             save power spectra for all the correlator dumps (default: false)\n";
  cerr << "	-f size:	FFT size (default: 2x number of channels)\n";
  cerr << "	-1:		use only the first time step in the UV fits file (default: false)\n";
  cerr << "	-p pol:		which polarization to use (default: XX). XX,YY, XY, YX\n";
  cerr << "	-w window	what window to put over the FFT (default: none)\n";
  cerr << "	-r num		scrunch num channels into 1 (default: no scrunch)\n";
  //cerr << "	-k		plot delay peaks only (default: false)\n";
  cerr << "     -c 		ignore channel flags (default: false)\n";
  cerr << "     -W              apply weight to k_perp bins (default: false)\n";
  cerr << "     -t num          number of threads to use (default: 1)\n";

}

void save_power(PowerSpectrum *ps, string fname, int max_delay, bool do_weight) {
 

  ps->clip(max_delay);

  if ( do_weight ) ps->reduced_kk_space.apply_weights();

  ps->calc_power();

  //if ( positive_negative_separate ) all_spectra[0]->kk_space.flip();
  cout << "Saving to " << fname << endl;
  // Result is now in all_spectra[0]
  ps->save(fname.c_str());
}

void sanity_check(vector<PowerSpectrum*>& all) {
  // Sanity check
#ifdef COHERENT
  if ( all.size() != 1 ) {
    cerr << "Should only be 1 spectrum for COHERENT averaging. Got " << all.size() << endl;
    exit(1);
  }
#endif

  for (int i=1; i<all.size(); ++i) {
   if ( !(all[i]->frequencies[0] == all[0]->frequencies[0] && all[i]->kk_space.dim2 == all[0]->kk_space.dim2) ) {
      cerr << "WARNING: Appear to be merging files with different bandwidths " << all[i]->frequencies[0] << " " 
	<< all[0]->frequencies[0] << " " << all[i]->kk_space.dim2 << " " << all[0]->kk_space.dim2 << endl;
      break;
    }
   if ( all[i]->kk_space.dim1 != all[0]->kk_space.dim1 ) {
      cerr << "WARNING: Appear to be merging files with different number of baselines " << all[i]->kk_space.dim1 << " " << all[0]->kk_space.dim1 << endl;
      break;
    }
   if ( all[i]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 0) !=  all[0]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 0) ) {
	// the uvw values shift over time and uv_length should be constant but there can be slight differences hence k_perp can vary slighly
      cerr << "WARNING: Appear to be merging files with different k_perp " << all[i]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 0) << " "
		<< all[0]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 0) << endl;
      break;
    }
   if ( all[i]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 1) !=  all[0]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 1) ) {
      cerr << "WARNING: Appear to be merging files with different horizons " <<  all[i]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 1) << " "
		<< all[0]->kk_space.get_row_tag(all[i]->kk_space.dim1-1, 1) << endl;
      break;
    }
  }
}

PowerSpectrum *reduce(vector<PowerSpectrum*>& all) {
  vector<PowerSpectrum*> multiplied;
  PowerSpectrum *result_ps;
  int num_pair_multiplied = 0;

  sanity_check(all);

  // Do a reduction step by multiplying.

  if ( all.size() > 1 && all.size()%2 == 1 ) {
    cerr << "WARNING: have an odd number of spectra for pairwise reduction\n";
  }

  if ( all.size() > 1 ) {
    cout << "Reduce: pairwise multiplication\n";
    // Do pairwise multiplication if possible
    int i = 0;
    while ( i < all.size() ) {
      if ( i+1 < all.size() && abs(all[i+1]->lst_seconds-all[i]->lst_seconds) < TIME_SMEARING_LIMIT ) {
        all[i]->kk_space.uncache(); all[i+1]->kk_space.uncache();
        PowerSpectrum *ps = new PowerSpectrum(*all[i]); check_not_null(ps, "in reduce");
        *ps *= *all[i+1];  
        all[i]->kk_space.cache(); all[i+1]->kk_space.cache();
        multiplied.push_back(ps);
        num_pair_multiplied += 2;
        i += 2;  // skip
      } else {
        // Multiply by itself
        all[i]->kk_space.uncache();
        PowerSpectrum *ps = new PowerSpectrum(*all[i]); check_not_null(ps, "in reduce");
        *ps *= *all[i]; 
        all[i]->kk_space.cache();
        multiplied.push_back(ps);
        i += 1;
      }
    }  
  } else { 
    all[0]->kk_space.uncache();
    result_ps = new PowerSpectrum(*all[0]); check_not_null(result_ps, "in reduce");
#ifndef COHERENT
    // There is only 1 PS which must be converted to power V^2 because there
    // has been no pair multiplication.
    *result_ps *= *all[0];		// convert to V squared but still a complex number
    // Why is this not done when we are simulating COHERENT integration? Because the 
    // pair multiplication is done in the GPU. However, if it transpires, due to the
    // time allocations, that there is not enough data for the pair multiplication,
    // then that is a problem. We catch it in the GPU program.
#endif
    all[0]->kk_space.cache();
    result_ps->reduce_by_k_perp(result_ps);
  }

  // Do a reduction by averaging, if we have enough
  if( multiplied.size() > 0 ) {
    cout << "Reduce: binning\n";
    
    for (int i=0; i<multiplied.size(); ++i) { 
      multiplied[0]->reduce_by_k_perp(multiplied[i], i==0);	
      if ( i > 0 ) delete multiplied[i];
    }
    result_ps = multiplied[0];
  }
  cout << "Num pair multiplied " << num_pair_multiplied << endl;
  return result_ps;
}

void save_one(PowerSpectrum *ps, char *fname, int j, bool apply_weights) {
  stringstream one;
  one << strip_file_name(fname) << "_" << j << "_ps.dat";

  PowerSpectrum tmp_ps(*ps);	
  tmp_ps *= tmp_ps;		// convert to V squared

  tmp_ps.reduce_by_k_perp(&tmp_ps);
  save_power(&tmp_ps, one.str(), MAX_DELAY_BIG, apply_weights);
}

class ThreadData {
public:
	char *fname;
	Flags* flags; 
	bool save_every; 
	int fft_size;
	const char *window;
	bool positive_negative_separate; 
	bool interpolate;
	bool apply_weights; 
	int which_pol;
	int file_index;
	int scrunch_num; 
	bool one_only;
	bool ignore_channel_flags;
        vector<PowerSpectrum*> spectra;
	thread *pthread;
	
	ThreadData(char *_fname, Flags* _flags, bool _save_every, int _fft_size, const char *_window, bool _positive_negative_separate, bool _interpolate,
		bool _apply_weights, int _which_pol, int _file_index, int _scrunch_num, bool _one_only, bool _ignore_channel_flags) {
	  fname = _fname;
          flags = _flags;
          save_every = _save_every;
          fft_size = _fft_size;
          window = _window;
          positive_negative_separate = _positive_negative_separate;
          interpolate = _interpolate;
          apply_weights = _apply_weights;
          which_pol = _which_pol;
          file_index = _file_index;
          scrunch_num = _scrunch_num;
          one_only = _one_only;
          ignore_channel_flags = _ignore_channel_flags;
        }

};

vector<PowerSpectrum*> get_spectra_from_file(char *fname, Flags* flags, bool save_every, int fft_size, const char *window, bool positive_negative_separate, bool interpolate,
		bool apply_weights, int which_pol, int file_index, int scrunch_num, bool one_only, bool ignore_channel_flags) {
  Visibilities **all;
  vector<PowerSpectrum*> spectra;
  int num_times, num_channels;

  load_uvdata(fname, all, which_pol,file_index, scrunch_num, num_times, num_channels, window, false, one_only, ignore_channel_flags, interpolate);
  if ( scrunch_num == 0 || scrunch_num >= num_channels ) {
    cerr << "Invalid scrunch\n";
    exit(1);
  }
  if ( fft_size < num_channels ) {
    cerr << "FFT size requested is smaller than the number of channels. FFT size: " << fft_size << ", num channels: " << num_channels << endl;
    exit(1);
  }

  for (int j=0; j<num_times; ++j) {
    cout << "Flag status " << strip_file_name(fname) << " step " << j << " " << (flags->in(flags->time_step_flags, strip_file_name(fname), j)==0?"not flagged":"flagged") << endl;
    if ( !flags->in(flags->time_step_flags, strip_file_name(fname), j) ) {		// otherwise we don't include this dump/time step in file

      all[j]->uncache();
      PowerSpectrum *ps = new PowerSpectrum(fname, j, *all[j], fft_size, window, positive_negative_separate, which_pol); check_not_null(ps, "in get_spectra_from_file");
      
      if ( save_every ) { cout << ps->which_pol << endl;save_one(ps, fname, j, apply_weights);}

      stringstream fcache; fcache << "grid_" << file_index << "_" << j << ".dat";
      ps->kk_space.cache(fcache.str());

      spectra.push_back(ps);
    }
    delete all[j];
 	
    if ( one_only ) break;
  }
  delete[] all;
  return spectra;
} 

void thread_run(ThreadData *data) {
  data->spectra = get_spectra_from_file(data->fname, data->flags, data->save_every, data->fft_size, data->window,
                data->positive_negative_separate, data->interpolate,
                data->apply_weights, data->which_pol, data->file_index, data->scrunch_num, data->one_only, data->ignore_channel_flags);
}

int main(int argc, char *argv[]) {
  vector<PowerSpectrum*> all_spectra;
  PowerSpectrum *result_ps;
  string fname;
  vector<ThreadData*> threads;
  Flags flags;
  int c, fft_size, which_pol, num_channels, scrunch_num, num_threads;
  const char *window="none", *fft_size_s="8192", *which_pol_s="XX", *scrunch_s="1", *num_threads_s="1";
  bool one_only=false, positive_negative_separate=false, interpolate =false, save_every=false;
  bool fft_specified=false, ignore_channel_flags=false, apply_weights=false;

  if ( argc == 1 ) {
    usage();
    return 0;
  }
  feenableexcept(FE_ALL_EXCEPT & ~FE_INEXACT);  // Enable all floating point exceptions but FE_INEXACT


  while ((c = getopt (argc, argv, "t:r:p:f:w:sid1kcW")) != -1)
    switch (c) {
      case 'd':
        save_every = true;
        break;
      case 'w':
        window = optarg;
        break;
      case 's':
        positive_negative_separate = true;
        break;
      case 'c':
        ignore_channel_flags = true;
        break;
      case 'i':
        interpolate = true;
        break;
      case 'W':
        apply_weights = true;
        break;
      case '1':
        one_only = true;
        break;
      case 'f':
        fft_size_s = optarg;
	fft_specified = true;
        break;
      case 'p':
        which_pol_s = optarg;
        break;
      case 't':
        num_threads_s = optarg;
        break;
      case 'r':
        scrunch_s = optarg;
        break;
      case '?':
        if (optopt == 'w')
          fprintf (stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint (optopt))
          fprintf (stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
        return 1;
      default:
        return 1;
      }

  if ( optind == argc ) {
    cerr << "No input files\n";
    return 1;
  }

  if ( !( strcmp(window, "hamming") == 0 || strcmp(window, "blackmanharris-sqr") == 0 || strcmp(window, "blackmanharris") == 0 || strcmp(window, "none") == 0 ) ) {
    cerr << "Invalid window\n";
    return 1;
  }

  if ( strcmp(which_pol_s, "XX") == 0 ) which_pol = 0;
  else if ( strcmp(which_pol_s, "YY") == 0 ) which_pol = 1;
  else if ( strcmp(which_pol_s, "XY") == 0 ) which_pol = 0;
  else if ( strcmp(which_pol_s, "YX") == 0 ) which_pol = 0;
  else {
    cerr << "Invalid pol. Expect XX, YY, XY, YX\n";
    return 1;
  }

  for (int i=0; i<strlen(fft_size_s); ++i)
    if ( !isdigit(fft_size_s[i]) ) {
      cerr << "Invalid fft_size\n";
      return 1;
    }


  for (int i=0; i<strlen(scrunch_s); ++i)
    if ( !isdigit(scrunch_s[i]) ) {
      cerr << "Invalid scrunch " << scrunch_s << endl;
      return 1;
    }
  scrunch_num = atoi(scrunch_s);

  for (int i=0; i<strlen(num_threads_s); ++i)
    if ( !isdigit(num_threads_s[i]) ) {
      cerr << "Invalid number of threads " << num_threads_s << endl;
      return 1;
    }
  num_threads = atoi(num_threads_s);


  if ( optind < argc ) num_channels = get_channels(argv[optind]);
  else num_channels = 0;

  if ( fft_specified ) {
    fft_size = atoi(fft_size_s);
  } else
    fft_size = num_channels;

  cout << "FFT_SIZE " << fft_size << " WINDOW " << window << " POL " << which_pol_s << " ONE_ONLY " << (one_only?"true":"false") << endl;

  flags.load_time_flags();

  // Read in all the calibrated data files and convert each to power spectra. The files
  // should have the same bandwidth and number of channels and the same uvlengths.
  // However they represent different times. 
 
  for (int i=optind; i<argc; ++i) {
    ThreadData *tdata = new ThreadData(argv[i], &flags, save_every, fft_size, window, positive_negative_separate, interpolate,
		apply_weights, which_pol, i-optind, scrunch_num, one_only, ignore_channel_flags); check_not_null(tdata, "for thread in main");
    if ( tdata == NULL ) {
      cerr << "Failed to allocate tdata\n";
      return 1;
    }

    cout << "Starting thread " << argv[i] << endl;
    tdata->pthread = new thread(thread_run, tdata); check_not_null(tdata->pthread, "for thread in main");
    if ( tdata->pthread == NULL ) {
      cerr << "Failed to allocate pthread\n";
      return 1;
    }	

    threads.push_back(tdata);

    if ( threads.size() == num_threads || i == argc-1 || one_only ) {		// Wait for existing threads and process results
      cout << threads.size() << " " << i << " " << argc << " waiting\n";
      for (int it=0; it<threads.size(); ++it) {
        threads[it]->pthread->join();
        cout << "Received " << threads[it]->spectra.size() << " spectra from " << threads[it]->fname << endl;
        for (int j=0; j<threads[it]->spectra.size(); ++j) { 
          all_spectra.push_back(threads[it]->spectra[j]);
        }
        delete threads[it]->pthread;
        delete threads[it];
      }
      threads.clear();

      if ( all_spectra.size() > 1 && argc-optind > 1 ) {
            stringstream intermediate;
            intermediate << INTERMEDIATE << all_spectra.size() << ".dat";
            PowerSpectrum *ps = reduce(all_spectra);
            save_power(ps, intermediate.str(), MAX_DELAY_BIG, apply_weights); 
            delete ps;  
      }
    }

    if ( one_only ) break;
  }

  if ( all_spectra.size() == 0 ) {
    cout << "No spectra\n";
    return 0;
  }


  sanity_check(all_spectra);

  result_ps = reduce(all_spectra);

  save_power(result_ps, OUT, MAX_DELAY, apply_weights); 
  delete result_ps;

  result_ps = reduce(all_spectra);
  save_power(result_ps, OUT_BIG, MAX_DELAY_BIG, apply_weights); 
  delete result_ps;

  ofstream used_file("used.txt");
  for (int i=0; i<all_spectra.size(); ++i) used_file << all_spectra[i]->fname << " " << all_spectra[i]->dump << endl;
  used_file.close();

  for (int i=0; i<all_spectra.size(); ++i) delete all_spectra[i];
    
  cout << all_spectra.size() << " time steps used in total\n";
  return 0;

}
